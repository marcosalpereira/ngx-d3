import { Component, OnInit } from '@angular/core';
import { Graph } from 'ngx-d3';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  data: Graph;

  constructor(private dataService: DataService) { }
  ngOnInit(): void {
    this.loadGraph('brasil');
  }

  public loadGraph(g) {
    this.dataService.getGraph(g)
      .subscribe(data => this.data = data);
  }

  onNodeClick(node) {
    this.dataService.getGraph(node.id)
      .subscribe(data => {
        this.data = data;
      }
      );
  }


}
