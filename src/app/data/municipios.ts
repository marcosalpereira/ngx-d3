﻿export const MUNICIPIOS = [
    {
        id: 5200050,
        nome: 'Abadia de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3100104,
        nome: 'Abadia dos Dourados',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5200100,
        nome: 'Abadiânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3100203,
        nome: 'Abaeté',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1500107,
        nome: 'Abaetetuba',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2300101,
        nome: 'Abaiara',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2900108,
        nome: 'Abaíra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2900207,
        nome: 'Abaré',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4100103,
        nome: 'Abatiá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4200051,
        nome: 'Abdon Batista',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1500131,
        nome: 'Abel Figueiredo',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4200101,
        nome: 'Abelardo Luz',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3100302,
        nome: 'Abre Campo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2600054,
        nome: 'Abreu e Lima',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1700251,
        nome: 'Abreulândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3100401,
        nome: 'Acaiaca',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2100055,
        nome: 'Açailândia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2900306,
        nome: 'Acajutiba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1500206,
        nome: 'Acará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2300150,
        nome: 'Acarape',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2300200,
        nome: 'Acaraú',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2400109,
        nome: 'Acari',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2200053,
        nome: 'Acauã',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4300034,
        nome: 'Aceguá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2300309,
        nome: 'Acopiara',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5100102,
        nome: 'Acorizal',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1200013,
        nome: 'Acrelândia',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 5200134,
        nome: 'Acreúna',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2400208,
        nome: 'Açu',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3100500,
        nome: 'Açucena',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3500105,
        nome: 'Adamantina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5200159,
        nome: 'Adelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3500204,
        nome: 'Adolfo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4100202,
        nome: 'Adrianópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2900355,
        nome: 'Adustina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2600104,
        nome: 'Afogados da Ingazeira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2400307,
        nome: 'Afonso Bezerra',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3200102,
        nome: 'Afonso Cláudio',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2100105,
        nome: 'Afonso Cunha',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2600203,
        nome: 'Afrânio',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1500305,
        nome: 'Afuá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2600302,
        nome: 'Agrestina',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2200103,
        nome: 'Agricolândia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4200200,
        nome: 'Agrolândia',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4200309,
        nome: 'Agronômica',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1500347,
        nome: 'Água Azul do Norte',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3100609,
        nome: 'Água Boa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5100201,
        nome: 'Água Boa',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2200202,
        nome: 'Água Branca',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2500106,
        nome: 'Água Branca',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2700102,
        nome: 'Água Branca',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5000203,
        nome: 'Água Clara',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3100708,
        nome: 'Água Comprida',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4200408,
        nome: 'Água Doce',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2100154,
        nome: 'Água Doce do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3200169,
        nome: 'Água Doce do Norte',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2900405,
        nome: 'Água Fria',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5200175,
        nome: 'Água Fria de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5200209,
        nome: 'Água Limpa',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2400406,
        nome: 'Água Nova',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2600401,
        nome: 'Água Preta',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4300059,
        nome: 'Água Santa',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3500303,
        nome: 'Aguaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3100807,
        nome: 'Aguanil',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2600500,
        nome: 'Águas Belas',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3500402,
        nome: 'Águas da Prata',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4200507,
        nome: 'Águas de Chapecó',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3500501,
        nome: 'Águas de Lindóia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3500550,
        nome: 'Águas de Santa Bárbara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3500600,
        nome: 'Águas de São Pedro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3100906,
        nome: 'Águas Formosas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4200556,
        nome: 'Águas Frias',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5200258,
        nome: 'Águas Lindas de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4200606,
        nome: 'Águas Mornas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3101003,
        nome: 'Águas Vermelhas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4300109,
        nome: 'Agudo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3500709,
        nome: 'Agudos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4100301,
        nome: 'Agudos do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3200136,
        nome: 'Águia Branca',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2500205,
        nome: 'Aguiar',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1700301,
        nome: 'Aguiarnópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3101102,
        nome: 'Aimorés',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2900603,
        nome: 'Aiquara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2300408,
        nome: 'Aiuaba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3101201,
        nome: 'Aiuruoca',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4300208,
        nome: 'Ajuricaba',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3101300,
        nome: 'Alagoa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2500304,
        nome: 'Alagoa Grande',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2500403,
        nome: 'Alagoa Nova',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2500502,
        nome: 'Alagoinha',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2600609,
        nome: 'Alagoinha',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2200251,
        nome: 'Alagoinha do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2900702,
        nome: 'Alagoinhas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3500758,
        nome: 'Alambari',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3101409,
        nome: 'Albertina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2100204,
        nome: 'Alcântara',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2300507,
        nome: 'Alcântaras',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2500536,
        nome: 'Alcantil',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5000252,
        nome: 'Alcinópolis',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2900801,
        nome: 'Alcobaça',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2100303,
        nome: 'Aldeias Altas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4300307,
        nome: 'Alecrim',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3200201,
        nome: 'Alegre',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4300406,
        nome: 'Alegrete',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2200277,
        nome: 'Alegrete do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4300455,
        nome: 'Alegria',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3101508,
        nome: 'Além Paraíba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1500404,
        nome: 'Alenquer',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2400505,
        nome: 'Alexandria',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5200308,
        nome: 'Alexânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3101607,
        nome: 'Alfenas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3200300,
        nome: 'Alfredo Chaves',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3500808,
        nome: 'Alfredo Marcondes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3101631,
        nome: 'Alfredo Vasconcelos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4200705,
        nome: 'Alfredo Wagner',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2500577,
        nome: 'Algodão de Jandaíra',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2500601,
        nome: 'Alhandra',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2600708,
        nome: 'Aliança',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1700350,
        nome: 'Aliança do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2900900,
        nome: 'Almadina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1700400,
        nome: 'Almas',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1500503,
        nome: 'Almeirim',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3101706,
        nome: 'Almenara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2400604,
        nome: 'Almino Afonso',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4100400,
        nome: 'Almirante Tamandaré',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4300471,
        nome: 'Almirante Tamandaré do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5200506,
        nome: 'Aloândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3101805,
        nome: 'Alpercata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4300505,
        nome: 'Alpestre',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3101904,
        nome: 'Alpinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5100250,
        nome: 'Alta Floresta',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1100015,
        nome: 'Alta Floresta D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3500907,
        nome: 'Altair',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1500602,
        nome: 'Altamira',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2100402,
        nome: 'Altamira do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4100459,
        nome: 'Altamira do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2300606,
        nome: 'Altaneira',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3102001,
        nome: 'Alterosa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2600807,
        nome: 'Altinho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3501004,
        nome: 'Altinópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3501103,
        nome: 'Alto Alegre',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1400050,
        nome: 'Alto Alegre',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 4300554,
        nome: 'Alto Alegre',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2100436,
        nome: 'Alto Alegre do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2100477,
        nome: 'Alto Alegre do Pindaré',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1100379,
        nome: 'Alto Alegre dos Parecis',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5100300,
        nome: 'Alto Araguaia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4200754,
        nome: 'Alto Bela Vista',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5100359,
        nome: 'Alto Boa Vista',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3102050,
        nome: 'Alto Caparaó',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2400703,
        nome: 'Alto do Rodrigues',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4300570,
        nome: 'Alto Feliz',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5100409,
        nome: 'Alto Garças',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5200555,
        nome: 'Alto Horizonte',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3153509,
        nome: 'Alto Jequitibá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2200301,
        nome: 'Alto Longá',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5100508,
        nome: 'Alto Paraguai',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4128625,
        nome: 'Alto Paraíso',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1100403,
        nome: 'Alto Paraíso',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5200605,
        nome: 'Alto Paraíso de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4100608,
        nome: 'Alto Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2100501,
        nome: 'Alto Parnaíba',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4100707,
        nome: 'Alto Piquiri',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3102100,
        nome: 'Alto Rio Doce',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3200359,
        nome: 'Alto Rio Novo',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2300705,
        nome: 'Alto Santo',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5100607,
        nome: 'Alto Taquari',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4100509,
        nome: 'Altônia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2200400,
        nome: 'Altos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3501152,
        nome: 'Alumínio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1300029,
        nome: 'Alvarães',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3102209,
        nome: 'Alvarenga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3501202,
        nome: 'Álvares Florence',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3501301,
        nome: 'Álvares Machado',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3501400,
        nome: 'Álvaro de Carvalho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3501509,
        nome: 'Alvinlândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3102308,
        nome: 'Alvinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1700707,
        nome: 'Alvorada',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4300604,
        nome: 'Alvorada',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100346,
        nome: 'Alvorada D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3102407,
        nome: 'Alvorada de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2200459,
        nome: 'Alvorada do Gurguéia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5200803,
        nome: 'Alvorada do Norte',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4100806,
        nome: 'Alvorada do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1400027,
        nome: 'Amajari',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 5000609,
        nome: 'Amambai',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1600105,
        nome: 'Amapá',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 2100550,
        nome: 'Amapá do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4100905,
        nome: 'Amaporã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2600906,
        nome: 'Amaraji',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4300638,
        nome: 'Amaral Ferrador',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5200829,
        nome: 'Amaralina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2200509,
        nome: 'Amarante',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2100600,
        nome: 'Amarante do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2901007,
        nome: 'Amargosa',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1300060,
        nome: 'Amaturá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2901106,
        nome: 'Amélia Rodrigues',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2901155,
        nome: 'América Dourada',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3501608,
        nome: 'Americana',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5200852,
        nome: 'Americano do Brasil',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3501707,
        nome: 'Américo Brasiliense',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3501806,
        nome: 'Américo de Campos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4300646,
        nome: 'Ametista do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2300754,
        nome: 'Amontada',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5200902,
        nome: 'Amorinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2500734,
        nome: 'Amparo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3501905,
        nome: 'Amparo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2800100,
        nome: 'Amparo de São Francisco',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3102506,
        nome: 'Amparo do Serra',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4101002,
        nome: 'Ampére',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2700201,
        nome: 'Anadia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2901205,
        nome: 'Anagé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4101051,
        nome: 'Anahy',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1500701,
        nome: 'Anajás',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2100709,
        nome: 'Anajatuba',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3502002,
        nome: 'Analândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1300086,
        nome: 'Anamã',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1701002,
        nome: 'Ananás',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1500800,
        nome: 'Ananindeua',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5201108,
        nome: 'Anápolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1500859,
        nome: 'Anapu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2100808,
        nome: 'Anapurus',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5000708,
        nome: 'Anastácio',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5000807,
        nome: 'Anaurilândia',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4200804,
        nome: 'Anchieta',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3200409,
        nome: 'Anchieta',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2901304,
        nome: 'Andaraí',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4101101,
        nome: 'Andirá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2901353,
        nome: 'Andorinha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3102605,
        nome: 'Andradas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3502101,
        nome: 'Andradina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4300661,
        nome: 'André da Rocha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3102803,
        nome: 'Andrelândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3502200,
        nome: 'Angatuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3102852,
        nome: 'Angelândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5000856,
        nome: 'Angélica',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2601003,
        nome: 'Angelim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4200903,
        nome: 'Angelina',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2901403,
        nome: 'Angical',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2200608,
        nome: 'Angical do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1701051,
        nome: 'Angico',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2400802,
        nome: 'Angicos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3300100,
        nome: 'Angra dos Reis',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2901502,
        nome: 'Anguera',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4101150,
        nome: 'Ângulo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5201207,
        nome: 'Anhanguera',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3502309,
        nome: 'Anhembi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3502408,
        nome: 'Anhumas',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5201306,
        nome: 'Anicuns',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2200707,
        nome: 'Anísio de Abreu',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4201000,
        nome: 'Anita Garibaldi',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4201109,
        nome: 'Anitápolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1300102,
        nome: 'Anori',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4300703,
        nome: 'Anta Gorda',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2901601,
        nome: 'Antas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4101200,
        nome: 'Antonina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2300804,
        nome: 'Antonina do Norte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2200806,
        nome: 'Antônio Almeida',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2901700,
        nome: 'Antônio Cardoso',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4201208,
        nome: 'Antônio Carlos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3102902,
        nome: 'Antônio Carlos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3103009,
        nome: 'Antônio Dias',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2901809,
        nome: 'Antônio Gonçalves',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5000906,
        nome: 'Antônio João',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2400901,
        nome: 'Antônio Martins',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4101309,
        nome: 'Antônio Olinto',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4300802,
        nome: 'Antônio Prado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3103108,
        nome: 'Antônio Prado de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2500775,
        nome: 'Aparecida',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3502507,
        nome: 'Aparecida',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3502606,
        nome: 'Aparecida d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5201405,
        nome: 'Aparecida de Goiânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5201454,
        nome: 'Aparecida do Rio Doce',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1701101,
        nome: 'Aparecida do Rio Negro',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5001003,
        nome: 'Aparecida do Taboado',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3300159,
        nome: 'Aperibé',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3200508,
        nome: 'Apiacá',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5100805,
        nome: 'Apiacás',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3502705,
        nome: 'Apiaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2100832,
        nome: 'Apicum-Açu',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4201257,
        nome: 'Apiúna',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2401008,
        nome: 'Apodi',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2901908,
        nome: 'Aporá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5201504,
        nome: 'Aporé',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2901957,
        nome: 'Apuarema',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4101408,
        nome: 'Apucarana',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1300144,
        nome: 'Apuí',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2300903,
        nome: 'Apuiarés',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2800209,
        nome: 'Aquidabã',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 5001102,
        nome: 'Aquidauana',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2301000,
        nome: 'Aquiraz',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4201273,
        nome: 'Arabutã',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2500809,
        nome: 'Araçagi',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3103207,
        nome: 'Araçaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2800308,
        nome: 'Aracaju',
        capital: 's',
        sigla_uf: 'SE'
    },
    {
        id: 3502754,
        nome: 'Araçariguama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2902054,
        nome: 'Araças',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2301109,
        nome: 'Aracati',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2902005,
        nome: 'Aracatu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3502804,
        nome: 'Araçatuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2902104,
        nome: 'Araci',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3103306,
        nome: 'Aracitaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2601052,
        nome: 'Araçoiaba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2301208,
        nome: 'Aracoiaba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3502903,
        nome: 'Araçoiaba da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3200607,
        nome: 'Aracruz',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5201603,
        nome: 'Araçu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3103405,
        nome: 'Araçuaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5201702,
        nome: 'Aragarças',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5201801,
        nome: 'Aragoiânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1701309,
        nome: 'Aragominas',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1701903,
        nome: 'Araguacema',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1702000,
        nome: 'Araguaçu',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5101001,
        nome: 'Araguaiana',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1702109,
        nome: 'Araguaína',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5101209,
        nome: 'Araguainha',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1702158,
        nome: 'Araguanã',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2100873,
        nome: 'Araguanã',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5202155,
        nome: 'Araguapaz',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3103504,
        nome: 'Araguari',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1702208,
        nome: 'Araguatins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2100907,
        nome: 'Araioses',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5001243,
        nome: 'Aral Moreira',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2902203,
        nome: 'Aramari',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4300851,
        nome: 'Arambaré',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2100956,
        nome: 'Arame',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3503000,
        nome: 'Aramina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3503109,
        nome: 'Arandu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3103603,
        nome: 'Arantina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3503158,
        nome: 'Arapeí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2700300,
        nome: 'Arapiraca',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1702307,
        nome: 'Arapoema',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3103702,
        nome: 'Araponga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4101507,
        nome: 'Arapongas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3103751,
        nome: 'Araporã',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4101606,
        nome: 'Arapoti',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4101655,
        nome: 'Arapuã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3103801,
        nome: 'Arapuá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5101258,
        nome: 'Araputanga',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4201307,
        nome: 'Araquari',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2500908,
        nome: 'Arara',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4201406,
        nome: 'Araranguá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3503208,
        nome: 'Araraquara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3503307,
        nome: 'Araras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2301257,
        nome: 'Ararendá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2101004,
        nome: 'Arari',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4300877,
        nome: 'Araricá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2301307,
        nome: 'Araripe',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2601102,
        nome: 'Araripina',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3300209,
        nome: 'Araruama',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4101705,
        nome: 'Araruna',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2501005,
        nome: 'Araruna',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2902252,
        nome: 'Arataca',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4300901,
        nome: 'Aratiba',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2301406,
        nome: 'Aratuba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2902302,
        nome: 'Aratuípe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2800407,
        nome: 'Arauá',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4101804,
        nome: 'Araucária',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3103900,
        nome: 'Araújos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3104007,
        nome: 'Araxá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3104106,
        nome: 'Arceburgo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3503356,
        nome: 'Arco-Íris',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3104205,
        nome: 'Arcos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2601201,
        nome: 'Arcoverde',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3104304,
        nome: 'Areado',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3300225,
        nome: 'Areal',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3503406,
        nome: 'Arealva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2501104,
        nome: 'Areia',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2401107,
        nome: 'Areia Branca',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2800506,
        nome: 'Areia Branca',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2501153,
        nome: 'Areia de Baraúnas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2501203,
        nome: 'Areial',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3503505,
        nome: 'Areias',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3503604,
        nome: 'Areiópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5101308,
        nome: 'Arenápolis',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5202353,
        nome: 'Arenópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2401206,
        nome: 'Arês',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3104403,
        nome: 'Argirita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3104452,
        nome: 'Aricanduva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3104502,
        nome: 'Arinos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5101407,
        nome: 'Aripuanã',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1100023,
        nome: 'Ariquemes',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3503703,
        nome: 'Ariranha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4101853,
        nome: 'Ariranha do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3300233,
        nome: 'Armação dos Búzios',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4201505,
        nome: 'Armazém',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2301505,
        nome: 'Arneiroz',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2200905,
        nome: 'Aroazes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2501302,
        nome: 'Aroeiras',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2200954,
        nome: 'Aroeiras do Itaim',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2201002,
        nome: 'Arraial',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3300258,
        nome: 'Arraial do Cabo',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 1702406,
        nome: 'Arraias',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4301008,
        nome: 'Arroio do Meio',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301073,
        nome: 'Arroio do Padre',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301057,
        nome: 'Arroio do Sal',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301206,
        nome: 'Arroio do Tigre',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301107,
        nome: 'Arroio dos Ratos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301305,
        nome: 'Arroio Grande',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4201604,
        nome: 'Arroio Trinta',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3503802,
        nome: 'Artur Nogueira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5202502,
        nome: 'Aruanã',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3503901,
        nome: 'Arujá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4201653,
        nome: 'Arvoredo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4301404,
        nome: 'Arvorezinha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4201703,
        nome: 'Ascurra',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3503950,
        nome: 'Aspásia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4101903,
        nome: 'Assaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2301604,
        nome: 'Assaré',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3504008,
        nome: 'Assis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1200054,
        nome: 'Assis Brasil',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4102000,
        nome: 'Assis Chateaubriand',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2501351,
        nome: 'Assunção',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2201051,
        nome: 'Assunção do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3104601,
        nome: 'Astolfo Dutra',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4102109,
        nome: 'Astorga',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4102208,
        nome: 'Atalaia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2700409,
        nome: 'Atalaia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1300201,
        nome: 'Atalaia do Norte',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4201802,
        nome: 'Atalanta',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3104700,
        nome: 'Ataléia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3504107,
        nome: 'Atibaia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3200706,
        nome: 'Atilio Vivacqua',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 1702554,
        nome: 'Augustinópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1500909,
        nome: 'Augusto Corrêa',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3104809,
        nome: 'Augusto de Lima',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4301503,
        nome: 'Augusto Pestana',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2401305,
        nome: 'Augusto Severo (Campo Grande)',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4301552,
        nome: 'Áurea',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2902401,
        nome: 'Aurelino Leal',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3504206,
        nome: 'Auriflama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5202601,
        nome: 'Aurilândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2301703,
        nome: 'Aurora',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4201901,
        nome: 'Aurora',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1500958,
        nome: 'Aurora do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1702703,
        nome: 'Aurora do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1300300,
        nome: 'Autazes',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3504305,
        nome: 'Avaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3504404,
        nome: 'Avanhandava',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3504503,
        nome: 'Avaré',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1501006,
        nome: 'Aveiro',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2201101,
        nome: 'Avelino Lopes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5202809,
        nome: 'Avelinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2101103,
        nome: 'Axixá',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1702901,
        nome: 'Axixá do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1703008,
        nome: 'Babaçulândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2101202,
        nome: 'Bacabal',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2101251,
        nome: 'Bacabeira',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2101301,
        nome: 'Bacuri',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2101350,
        nome: 'Bacurituba',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3504602,
        nome: 'Bady Bassitt',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3104908,
        nome: 'Baependi',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4301602,
        nome: 'Bagé',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1501105,
        nome: 'Bagre',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2501401,
        nome: 'Baía da Traição',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2401404,
        nome: 'Baía Formosa',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2902500,
        nome: 'Baianópolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1501204,
        nome: 'Baião',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2902609,
        nome: 'Baixa Grande',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2201150,
        nome: 'Baixa Grande do Ribeiro',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2301802,
        nome: 'Baixio',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3200805,
        nome: 'Baixo Guandu',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3504701,
        nome: 'Balbinos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3105004,
        nome: 'Baldim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5203104,
        nome: 'Baliza',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4201950,
        nome: 'Balneário Arroio do Silva',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4202057,
        nome: 'Balneário Barra do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4202008,
        nome: 'Balneário Camboriú',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4202073,
        nome: 'Balneário Gaivota',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4212809,
        nome: 'Balneário Piçarras',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4301636,
        nome: 'Balneário Pinhal',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4220000,
        nome: 'Balneário Rincão',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4102307,
        nome: 'Balsa Nova',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3504800,
        nome: 'Bálsamo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2101400,
        nome: 'Balsas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3105103,
        nome: 'Bambuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2301851,
        nome: 'Banabuiú',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3504909,
        nome: 'Bananal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2501500,
        nome: 'Bananeiras',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3105202,
        nome: 'Bandeira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3105301,
        nome: 'Bandeira do Sul',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4202081,
        nome: 'Bandeirante',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5001508,
        nome: 'Bandeirantes',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4102406,
        nome: 'Bandeirantes',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1703057,
        nome: 'Bandeirantes do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1501253,
        nome: 'Bannach',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2902658,
        nome: 'Banzaê',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4301651,
        nome: 'Barão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3505005,
        nome: 'Barão de Antonina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3105400,
        nome: 'Barão de Cocais',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4301701,
        nome: 'Barão de Cotegipe',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2101509,
        nome: 'Barão de Grajaú',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5101605,
        nome: 'Barão de Melgaço',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3105509,
        nome: 'Barão de Monte Alto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4301750,
        nome: 'Barão do Triunfo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2401453,
        nome: 'Baraúna',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2501534,
        nome: 'Baraúna',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3105608,
        nome: 'Barbacena',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2301901,
        nome: 'Barbalha',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3505104,
        nome: 'Barbosa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4102505,
        nome: 'Barbosa Ferraz',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1501303,
        nome: 'Barcarena',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2401503,
        nome: 'Barcelona',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1300409,
        nome: 'Barcelos',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3505203,
        nome: 'Bariri',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2902708,
        nome: 'Barra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4202099,
        nome: 'Barra Bonita',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3505302,
        nome: 'Barra Bonita',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2201176,
        nome: 'Barra D\'Alcântara',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2902807,
        nome: 'Barra da Estiva',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2601300,
        nome: 'Barra de Guabiraba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2501609,
        nome: 'Barra de Santa Rosa',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2501575,
        nome: 'Barra de Santana',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2700508,
        nome: 'Barra de Santo Antônio',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3200904,
        nome: 'Barra de São Francisco',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2501708,
        nome: 'Barra de São Miguel',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2700607,
        nome: 'Barra de São Miguel',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5101704,
        nome: 'Barra do Bugres',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3505351,
        nome: 'Barra do Chapéu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2902906,
        nome: 'Barra do Choça',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2101608,
        nome: 'Barra do Corda',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5101803,
        nome: 'Barra do Garças',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4301859,
        nome: 'Barra do Guarita',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4102703,
        nome: 'Barra do Jacaré',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2903003,
        nome: 'Barra do Mendes',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1703073,
        nome: 'Barra do Ouro',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3300308,
        nome: 'Barra do Piraí',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4301875,
        nome: 'Barra do Quaraí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301909,
        nome: 'Barra do Ribeiro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4301925,
        nome: 'Barra do Rio Azul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2903102,
        nome: 'Barra do Rocha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3505401,
        nome: 'Barra do Turvo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2800605,
        nome: 'Barra dos Coqueiros',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4301958,
        nome: 'Barra Funda',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3105707,
        nome: 'Barra Longa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3300407,
        nome: 'Barra Mansa',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4202107,
        nome: 'Barra Velha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4301800,
        nome: 'Barracão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4102604,
        nome: 'Barracão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2201200,
        nome: 'Barras',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2301950,
        nome: 'Barreira',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2903201,
        nome: 'Barreiras',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2201309,
        nome: 'Barreiras do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1300508,
        nome: 'Barreirinha',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2101707,
        nome: 'Barreirinhas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2601409,
        nome: 'Barreiros',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3505500,
        nome: 'Barretos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3505609,
        nome: 'Barrinha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2302008,
        nome: 'Barro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2903235,
        nome: 'Barro Alto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5203203,
        nome: 'Barro Alto',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2201408,
        nome: 'Barro Duro',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2903300,
        nome: 'Barro Preto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2903276,
        nome: 'Barrocas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1703107,
        nome: 'Barrolândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2302057,
        nome: 'Barroquinha',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4302006,
        nome: 'Barros Cassal',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3105905,
        nome: 'Barroso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3505708,
        nome: 'Barueri',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3505807,
        nome: 'Bastos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5001904,
        nome: 'Bataguassu',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2201507,
        nome: 'Batalha',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2700706,
        nome: 'Batalha',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3505906,
        nome: 'Batatais',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5002001,
        nome: 'Batayporã',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2302107,
        nome: 'Baturité',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3506003,
        nome: 'Bauru',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2501807,
        nome: 'Bayeux',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3506102,
        nome: 'Bebedouro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2302206,
        nome: 'Beberibe',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2302305,
        nome: 'Bela Cruz',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5002100,
        nome: 'Bela Vista',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4102752,
        nome: 'Bela Vista da Caroba',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5203302,
        nome: 'Bela Vista de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3106002,
        nome: 'Bela Vista de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2101772,
        nome: 'Bela Vista do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4102802,
        nome: 'Bela Vista do Paraíso',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2201556,
        nome: 'Bela Vista do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4202131,
        nome: 'Bela Vista do Toldo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2101731,
        nome: 'Belágua',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1501402,
        nome: 'Belém',
        capital: 's',
        sigla_uf: 'PA'
    },
    {
        id: 2501906,
        nome: 'Belém',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2700805,
        nome: 'Belém',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2601508,
        nome: 'Belém de Maria',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2502003,
        nome: 'Belém do Brejo do Cruz',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2201572,
        nome: 'Belém do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2601607,
        nome: 'Belém do São Francisco',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3300456,
        nome: 'Belford Roxo',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3106101,
        nome: 'Belmiro Braga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4202156,
        nome: 'Belmonte',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2903409,
        nome: 'Belmonte',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2903508,
        nome: 'Belo Campo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3106200,
        nome: 'Belo Horizonte',
        capital: 's',
        sigla_uf: 'MG'
    },
    {
        id: 2601706,
        nome: 'Belo Jardim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2700904,
        nome: 'Belo Monte',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3106309,
        nome: 'Belo Oriente',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3106408,
        nome: 'Belo Vale',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1501451,
        nome: 'Belterra',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2201606,
        nome: 'Beneditinos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2101806,
        nome: 'Benedito Leite',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4202206,
        nome: 'Benedito Novo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1501501,
        nome: 'Benevides',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1300607,
        nome: 'Benjamin Constant',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4302055,
        nome: 'Benjamin Constant do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3506201,
        nome: 'Bento de Abreu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2401602,
        nome: 'Bento Fernandes',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4302105,
        nome: 'Bento Gonçalves',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2101905,
        nome: 'Bequimão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3106507,
        nome: 'Berilo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3106655,
        nome: 'Berizal',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2502052,
        nome: 'Bernardino Batista',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3506300,
        nome: 'Bernardino de Campos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2101939,
        nome: 'Bernardo do Mearim',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1703206,
        nome: 'Bernardo Sayão',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3506359,
        nome: 'Bertioga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2201705,
        nome: 'Bertolínia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3106606,
        nome: 'Bertópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1300631,
        nome: 'Beruri',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2601805,
        nome: 'Betânia',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2201739,
        nome: 'Betânia do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3106705,
        nome: 'Betim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2601904,
        nome: 'Bezerros',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3106804,
        nome: 'Bias Fortes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3106903,
        nome: 'Bicas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4202305,
        nome: 'Biguaçu',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3506409,
        nome: 'Bilac',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3107000,
        nome: 'Biquinhas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3506508,
        nome: 'Birigui',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3506607,
        nome: 'Biritiba-Mirim',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2903607,
        nome: 'Biritinga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4102901,
        nome: 'Bituruna',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4202404,
        nome: 'Blumenau',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4103008,
        nome: 'Boa Esperança',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3107109,
        nome: 'Boa Esperança',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3201001,
        nome: 'Boa Esperança',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4103024,
        nome: 'Boa Esperança do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3506706,
        nome: 'Boa Esperança do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2201770,
        nome: 'Boa Hora',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2903706,
        nome: 'Boa Nova',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2502102,
        nome: 'Boa Ventura',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4103040,
        nome: 'Boa Ventura de São Roque',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2302404,
        nome: 'Boa Viagem',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1400100,
        nome: 'Boa Vista',
        capital: 's',
        sigla_uf: 'RR'
    },
    {
        id: 2502151,
        nome: 'Boa Vista',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4103057,
        nome: 'Boa Vista da Aparecida',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4302154,
        nome: 'Boa Vista das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4302204,
        nome: 'Boa Vista do Buricá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4302220,
        nome: 'Boa Vista do Cadeado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2101970,
        nome: 'Boa Vista do Gurupi',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4302238,
        nome: 'Boa Vista do Incra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1300680,
        nome: 'Boa Vista do Ramos',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4302253,
        nome: 'Boa Vista do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2903805,
        nome: 'Boa Vista do Tupim',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2701001,
        nome: 'Boca da Mata',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1300706,
        nome: 'Boca do Acre',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2201804,
        nome: 'Bocaina',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3506805,
        nome: 'Bocaina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3107208,
        nome: 'Bocaina de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4202438,
        nome: 'Bocaina do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3107307,
        nome: 'Bocaiúva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4103107,
        nome: 'Bocaiúva do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2401651,
        nome: 'Bodó',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2602001,
        nome: 'Bodocó',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5002159,
        nome: 'Bodoquena',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3506904,
        nome: 'Bofete',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3507001,
        nome: 'Boituva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2602100,
        nome: 'Bom Conselho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3107406,
        nome: 'Bom Despacho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3300506,
        nome: 'Bom Jardim',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2602209,
        nome: 'Bom Jardim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2102002,
        nome: 'Bom Jardim',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4202503,
        nome: 'Bom Jardim da Serra',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5203401,
        nome: 'Bom Jardim de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3107505,
        nome: 'Bom Jardim de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4202537,
        nome: 'Bom Jesus',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4302303,
        nome: 'Bom Jesus',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2201903,
        nome: 'Bom Jesus',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2401701,
        nome: 'Bom Jesus',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2502201,
        nome: 'Bom Jesus',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2903904,
        nome: 'Bom Jesus da Lapa',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3107604,
        nome: 'Bom Jesus da Penha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2903953,
        nome: 'Bom Jesus da Serra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2102036,
        nome: 'Bom Jesus das Selvas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5203500,
        nome: 'Bom Jesus de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3107703,
        nome: 'Bom Jesus do Amparo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5101852,
        nome: 'Bom Jesus do Araguaia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3107802,
        nome: 'Bom Jesus do Galho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3300605,
        nome: 'Bom Jesus do Itabapoana',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3201100,
        nome: 'Bom Jesus do Norte',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4202578,
        nome: 'Bom Jesus do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4103156,
        nome: 'Bom Jesus do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1501576,
        nome: 'Bom Jesus do Tocantins',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1703305,
        nome: 'Bom Jesus do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3507100,
        nome: 'Bom Jesus dos Perdões',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2102077,
        nome: 'Bom Lugar',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4302352,
        nome: 'Bom Princípio',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2201919,
        nome: 'Bom Princípio do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4302378,
        nome: 'Bom Progresso',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3107901,
        nome: 'Bom Repouso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4202602,
        nome: 'Bom Retiro',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4302402,
        nome: 'Bom Retiro do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3108008,
        nome: 'Bom Sucesso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4103206,
        nome: 'Bom Sucesso',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2502300,
        nome: 'Bom Sucesso',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3507159,
        nome: 'Bom Sucesso de Itararé',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4103222,
        nome: 'Bom Sucesso do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4202453,
        nome: 'Bombinhas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1400159,
        nome: 'Bonfim',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 3108107,
        nome: 'Bonfim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2201929,
        nome: 'Bonfim do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5203559,
        nome: 'Bonfinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3108206,
        nome: 'Bonfinópolis de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2904001,
        nome: 'Boninal',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2602308,
        nome: 'Bonito',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2904050,
        nome: 'Bonito',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1501600,
        nome: 'Bonito',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5002209,
        nome: 'Bonito',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3108255,
        nome: 'Bonito de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2502409,
        nome: 'Bonito de Santa Fé',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5203575,
        nome: 'Bonópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2502508,
        nome: 'Boqueirão',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4302451,
        nome: 'Boqueirão do Leão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2201945,
        nome: 'Boqueirão do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2800670,
        nome: 'Boquim',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2904100,
        nome: 'Boquira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3507209,
        nome: 'Borá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3507308,
        nome: 'Boracéia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1300805,
        nome: 'Borba',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2502706,
        nome: 'Borborema',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3507407,
        nome: 'Borborema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3108305,
        nome: 'Borda da Mata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3507456,
        nome: 'Borebi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4103305,
        nome: 'Borrazópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4302501,
        nome: 'Bossoroca',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3108404,
        nome: 'Botelhos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3507506,
        nome: 'Botucatu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3108503,
        nome: 'Botumirim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2904209,
        nome: 'Botuporã',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4202701,
        nome: 'Botuverá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4302584,
        nome: 'Bozano',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4202800,
        nome: 'Braço do Norte',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4202859,
        nome: 'Braço do Trombudo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4302600,
        nome: 'Braga',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1501709,
        nome: 'Bragança',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3507605,
        nome: 'Bragança Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4103354,
        nome: 'Braganey',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2701100,
        nome: 'Branquinha',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3108701,
        nome: 'Brás Pires',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1501725,
        nome: 'Brasil Novo',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5002308,
        nome: 'Brasilândia',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3108552,
        nome: 'Brasilândia de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4103370,
        nome: 'Brasilândia do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1703602,
        nome: 'Brasilândia do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1200104,
        nome: 'Brasiléia',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2201960,
        nome: 'Brasileira',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5300108,
        nome: 'Brasília',
        capital: 's',
        sigla_uf: 'DF'
    },
    {
        id: 3108602,
        nome: 'Brasília de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5101902,
        nome: 'Brasnorte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3507704,
        nome: 'Braúna',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3108800,
        nome: 'Braúnas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5203609,
        nome: 'Brazabrantes',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3108909,
        nome: 'Brazópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2602407,
        nome: 'Brejão',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3201159,
        nome: 'Brejetuba',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2401800,
        nome: 'Brejinho',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2602506,
        nome: 'Brejinho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1703701,
        nome: 'Brejinho de Nazaré',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2102101,
        nome: 'Brejo',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3507753,
        nome: 'Brejo Alegre',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2602605,
        nome: 'Brejo da Madre de Deus',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2102150,
        nome: 'Brejo de Areia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2502805,
        nome: 'Brejo do Cruz',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2201988,
        nome: 'Brejo do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2502904,
        nome: 'Brejo dos Santos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2800704,
        nome: 'Brejo Grande',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 1501758,
        nome: 'Brejo Grande do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2302503,
        nome: 'Brejo Santo',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2904308,
        nome: 'Brejões',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2904407,
        nome: 'Brejolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1501782,
        nome: 'Breu Branco',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1501808,
        nome: 'Breves',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5203807,
        nome: 'Britânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4302659,
        nome: 'Brochier',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3507803,
        nome: 'Brodowski',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3507902,
        nome: 'Brotas',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2904506,
        nome: 'Brotas de Macaúbas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3109006,
        nome: 'Brumadinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2904605,
        nome: 'Brumado',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4202875,
        nome: 'Brunópolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4202909,
        nome: 'Brusque',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3109105,
        nome: 'Bueno Brandão',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3109204,
        nome: 'Buenópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2602704,
        nome: 'Buenos Aires',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2904704,
        nome: 'Buerarema',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3109253,
        nome: 'Bugre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2602803,
        nome: 'Buíque',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1200138,
        nome: 'Bujari',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 1501907,
        nome: 'Bujaru',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3508009,
        nome: 'Buri',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3508108,
        nome: 'Buritama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2102200,
        nome: 'Buriti',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5203906,
        nome: 'Buriti Alegre',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2102309,
        nome: 'Buriti Bravo',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5203939,
        nome: 'Buriti de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1703800,
        nome: 'Buriti do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2202000,
        nome: 'Buriti dos Lopes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2202026,
        nome: 'Buriti dos Montes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2102325,
        nome: 'Buriticupu',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5203962,
        nome: 'Buritinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2904753,
        nome: 'Buritirama',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2102358,
        nome: 'Buritirana',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1100452,
        nome: 'Buritis',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3109303,
        nome: 'Buritis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3508207,
        nome: 'Buritizal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3109402,
        nome: 'Buritizeiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4302709,
        nome: 'Butiá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1300839,
        nome: 'Caapiranga',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2503001,
        nome: 'Caaporã',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5002407,
        nome: 'Caarapó',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2904803,
        nome: 'Caatiba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2503100,
        nome: 'Cabaceiras',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2904852,
        nome: 'Cabaceiras do Paraguaçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3109451,
        nome: 'Cabeceira Grande',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5204003,
        nome: 'Cabeceiras',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2202059,
        nome: 'Cabeceiras do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2503209,
        nome: 'Cabedelo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1100031,
        nome: 'Cabixi',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2602902,
        nome: 'Cabo de Santo Agostinho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3300704,
        nome: 'Cabo Frio',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3109501,
        nome: 'Cabo Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3508306,
        nome: 'Cabrália Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3508405,
        nome: 'Cabreúva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2603009,
        nome: 'Cabrobó',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4203006,
        nome: 'Caçador',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3508504,
        nome: 'Caçapava',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4302808,
        nome: 'Caçapava do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100601,
        nome: 'Cacaulândia',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4302907,
        nome: 'Cacequi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5102504,
        nome: 'Cáceres',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2904902,
        nome: 'Cachoeira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5204102,
        nome: 'Cachoeira Alta',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3109600,
        nome: 'Cachoeira da Prata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5204201,
        nome: 'Cachoeira de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3109709,
        nome: 'Cachoeira de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3102704,
        nome: 'Cachoeira de Pajeú',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1502004,
        nome: 'Cachoeira do Arari',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1501956,
        nome: 'Cachoeira do Piriá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4303004,
        nome: 'Cachoeira do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2503308,
        nome: 'Cachoeira dos Índios',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5204250,
        nome: 'Cachoeira Dourada',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3109808,
        nome: 'Cachoeira Dourada',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2102374,
        nome: 'Cachoeira Grande',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3508603,
        nome: 'Cachoeira Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3300803,
        nome: 'Cachoeiras de Macacu',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 1703826,
        nome: 'Cachoeirinha',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2603108,
        nome: 'Cachoeirinha',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4303103,
        nome: 'Cachoeirinha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3201209,
        nome: 'Cachoeiro de Itapemirim',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2503407,
        nome: 'Cacimba de Areia',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2503506,
        nome: 'Cacimba de Dentro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2503555,
        nome: 'Cacimbas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2701209,
        nome: 'Cacimbinhas',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4303202,
        nome: 'Cacique Doble',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100049,
        nome: 'Cacoal',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3508702,
        nome: 'Caconde',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5204300,
        nome: 'Caçu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2905008,
        nome: 'Caculé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2905107,
        nome: 'Caém',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3109907,
        nome: 'Caetanópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2905156,
        nome: 'Caetanos',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3110004,
        nome: 'Caeté',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2603207,
        nome: 'Caetés',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2905206,
        nome: 'Caetité',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2905305,
        nome: 'Cafarnaum',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4103404,
        nome: 'Cafeara',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3508801,
        nome: 'Cafelândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4103453,
        nome: 'Cafelândia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4103479,
        nome: 'Cafezal do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3508900,
        nome: 'Caiabu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3110103,
        nome: 'Caiana',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5204409,
        nome: 'Caiapônia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4303301,
        nome: 'Caibaté',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4203105,
        nome: 'Caibi',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4303400,
        nome: 'Caiçara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2503605,
        nome: 'Caiçara',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2401859,
        nome: 'Caiçara do Norte',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2401909,
        nome: 'Caiçara do Rio do Vento',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2402006,
        nome: 'Caicó',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3509007,
        nome: 'Caieiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2905404,
        nome: 'Cairu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3509106,
        nome: 'Caiuá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3509205,
        nome: 'Cajamar',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2102408,
        nome: 'Cajapió',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2102507,
        nome: 'Cajari',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3509254,
        nome: 'Cajati',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2503704,
        nome: 'Cajazeiras',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2202075,
        nome: 'Cajazeiras do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2503753,
        nome: 'Cajazeirinhas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3509304,
        nome: 'Cajobi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2701308,
        nome: 'Cajueiro',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2202083,
        nome: 'Cajueiro da Praia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3110202,
        nome: 'Cajuri',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3509403,
        nome: 'Cajuru',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2603306,
        nome: 'Calçado',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1600204,
        nome: 'Calçoene',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 3110301,
        nome: 'Caldas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2503803,
        nome: 'Caldas Brandão',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5204508,
        nome: 'Caldas Novas',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5204557,
        nome: 'Caldazinha',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2905503,
        nome: 'Caldeirão Grande',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2202091,
        nome: 'Caldeirão Grande do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4103503,
        nome: 'Califórnia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4203154,
        nome: 'Calmon',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2603405,
        nome: 'Calumbi',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2905602,
        nome: 'Camacan',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2905701,
        nome: 'Camaçari',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3110400,
        nome: 'Camacho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2503902,
        nome: 'Camalaú',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2905800,
        nome: 'Camamu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3110509,
        nome: 'Camanducaia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5002605,
        nome: 'Camapuã',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4303509,
        nome: 'Camaquã',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2603454,
        nome: 'Camaragibe',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4303558,
        nome: 'Camargo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4103602,
        nome: 'Cambará',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4303608,
        nome: 'Cambará do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4103701,
        nome: 'Cambé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4103800,
        nome: 'Cambira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4203204,
        nome: 'Camboriú',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3300902,
        nome: 'Cambuci',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3110608,
        nome: 'Cambuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3110707,
        nome: 'Cambuquira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1502103,
        nome: 'Cametá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2302602,
        nome: 'Camocim',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2603504,
        nome: 'Camocim de São Félix',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3110806,
        nome: 'Campanário',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3110905,
        nome: 'Campanha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3111002,
        nome: 'Campestre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2701357,
        nome: 'Campestre',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4303673,
        nome: 'Campestre da Serra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5204607,
        nome: 'Campestre de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2102556,
        nome: 'Campestre do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4103909,
        nome: 'Campina da Lagoa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4303707,
        nome: 'Campina das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3509452,
        nome: 'Campina do Monte Alegre',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4103958,
        nome: 'Campina do Simão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2504009,
        nome: 'Campina Grande',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4104006,
        nome: 'Campina Grande do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3111101,
        nome: 'Campina Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5204656,
        nome: 'Campinaçu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5102603,
        nome: 'Campinápolis',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3509502,
        nome: 'Campinas',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2202109,
        nome: 'Campinas do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4303806,
        nome: 'Campinas do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5204706,
        nome: 'Campinorte',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4203303,
        nome: 'Campo Alegre',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2701407,
        nome: 'Campo Alegre',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5204805,
        nome: 'Campo Alegre de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2905909,
        nome: 'Campo Alegre de Lourdes',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2202117,
        nome: 'Campo Alegre do Fidalgo',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3111150,
        nome: 'Campo Azul',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3111200,
        nome: 'Campo Belo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4203402,
        nome: 'Campo Belo do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4303905,
        nome: 'Campo Bom',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4104055,
        nome: 'Campo Bonito',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2801009,
        nome: 'Campo do Brito',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3111309,
        nome: 'Campo do Meio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4104105,
        nome: 'Campo do Tenente',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4203501,
        nome: 'Campo Erê',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3111408,
        nome: 'Campo Florido',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2906006,
        nome: 'Campo Formoso',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2701506,
        nome: 'Campo Grande',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5002704,
        nome: 'Campo Grande',
        capital: 's',
        sigla_uf: 'MS'
    },
    {
        id: 2202133,
        nome: 'Campo Grande do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4104204,
        nome: 'Campo Largo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2202174,
        nome: 'Campo Largo do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5204854,
        nome: 'Campo Limpo de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3509601,
        nome: 'Campo Limpo Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4104253,
        nome: 'Campo Magro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2202208,
        nome: 'Campo Maior',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4104303,
        nome: 'Campo Mourão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4304002,
        nome: 'Campo Novo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100700,
        nome: 'Campo Novo de Rondônia',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5102637,
        nome: 'Campo Novo do Parecis',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2402105,
        nome: 'Campo Redondo',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5102678,
        nome: 'Campo Verde',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3111507,
        nome: 'Campos Altos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5204904,
        nome: 'Campos Belos',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4304101,
        nome: 'Campos Borges',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5102686,
        nome: 'Campos de Júlio',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3509700,
        nome: 'Campos do Jordão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3301009,
        nome: 'Campos dos Goytacazes',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3111606,
        nome: 'Campos Gerais',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1703842,
        nome: 'Campos Lindos',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4203600,
        nome: 'Campos Novos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3509809,
        nome: 'Campos Novos Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2302701,
        nome: 'Campos Sales',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5204953,
        nome: 'Campos Verdes',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2603603,
        nome: 'Camutanga',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3111903,
        nome: 'Cana Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3111705,
        nome: 'Canaã',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1502152,
        nome: 'Canaã dos Carajás',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5102694,
        nome: 'Canabrava do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3509908,
        nome: 'Cananéia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2701605,
        nome: 'Canapi',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2906105,
        nome: 'Canápolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3111804,
        nome: 'Canápolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2906204,
        nome: 'Canarana',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5102702,
        nome: 'Canarana',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3509957,
        nome: 'Canas',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2202251,
        nome: 'Canavieira',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2906303,
        nome: 'Canavieiras',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2906402,
        nome: 'Candeal',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2906501,
        nome: 'Candeias',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3112000,
        nome: 'Candeias',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1100809,
        nome: 'Candeias do Jamari',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4304200,
        nome: 'Candelária',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2906600,
        nome: 'Candiba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4104402,
        nome: 'Cândido de Abreu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4304309,
        nome: 'Cândido Godói',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2102606,
        nome: 'Cândido Mendes',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3510005,
        nome: 'Cândido Mota',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3510104,
        nome: 'Cândido Rodrigues',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2906709,
        nome: 'Cândido Sales',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4304358,
        nome: 'Candiota',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4104428,
        nome: 'Candói',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4304408,
        nome: 'Canela',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4203709,
        nome: 'Canelinha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2402204,
        nome: 'Canguaretama',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4304507,
        nome: 'Canguçu',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2801108,
        nome: 'Canhoba',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2603702,
        nome: 'Canhotinho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2302800,
        nome: 'Canindé',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2801207,
        nome: 'Canindé de São Francisco',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3510153,
        nome: 'Canitar',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4304606,
        nome: 'Canoas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4203808,
        nome: 'Canoinhas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2906808,
        nome: 'Cansanção',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1400175,
        nome: 'Cantá',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 3301108,
        nome: 'Cantagalo',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4104451,
        nome: 'Cantagalo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3112059,
        nome: 'Cantagalo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2102705,
        nome: 'Cantanhede',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2202307,
        nome: 'Canto do Buriti',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2906824,
        nome: 'Canudos',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4304614,
        nome: 'Canudos do Vale',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1300904,
        nome: 'Canutama',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1502202,
        nome: 'Capanema',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4104501,
        nome: 'Capanema',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4203253,
        nome: 'Capão Alto',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3510203,
        nome: 'Capão Bonito',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4304622,
        nome: 'Capão Bonito do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4304630,
        nome: 'Capão da Canoa',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4304655,
        nome: 'Capão do Cipó',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4304663,
        nome: 'Capão do Leão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3112109,
        nome: 'Caparaó',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2701704,
        nome: 'Capela',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2801306,
        nome: 'Capela',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4304689,
        nome: 'Capela de Santana',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3510302,
        nome: 'Capela do Alto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2906857,
        nome: 'Capela do Alto Alegre',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3112208,
        nome: 'Capela Nova',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3112307,
        nome: 'Capelinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3112406,
        nome: 'Capetinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2504033,
        nome: 'Capim',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3112505,
        nome: 'Capim Branco',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2906873,
        nome: 'Capim Grosso',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3112604,
        nome: 'Capinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4203907,
        nome: 'Capinzal',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2102754,
        nome: 'Capinzal do Norte',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2302909,
        nome: 'Capistrano',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4304697,
        nome: 'Capitão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3112653,
        nome: 'Capitão Andrade',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2202406,
        nome: 'Capitão de Campos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3112703,
        nome: 'Capitão Enéas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2202455,
        nome: 'Capitão Gervásio Oliveira',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4104600,
        nome: 'Capitão Leônidas Marques',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1502301,
        nome: 'Capitão Poço',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3112802,
        nome: 'Capitólio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3510401,
        nome: 'Capivari',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4203956,
        nome: 'Capivari de Baixo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4304671,
        nome: 'Capivari do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1200179,
        nome: 'Capixaba',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2603801,
        nome: 'Capoeiras',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3112901,
        nome: 'Caputira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4304713,
        nome: 'Caraá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1400209,
        nome: 'Caracaraí',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 2202505,
        nome: 'Caracol',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5002803,
        nome: 'Caracol',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3510500,
        nome: 'Caraguatatuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3113008,
        nome: 'Caraí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2906899,
        nome: 'Caraíbas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4104659,
        nome: 'Carambeí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3113107,
        nome: 'Caranaíba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3113206,
        nome: 'Carandaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3113305,
        nome: 'Carangola',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3300936,
        nome: 'Carapebus',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3510609,
        nome: 'Carapicuíba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3113404,
        nome: 'Caratinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1301001,
        nome: 'Carauari',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2402303,
        nome: 'Caraúbas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2504074,
        nome: 'Caraúbas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2202539,
        nome: 'Caraúbas do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2906907,
        nome: 'Caravelas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4304705,
        nome: 'Carazinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3113503,
        nome: 'Carbonita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2907004,
        nome: 'Cardeal da Silva',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3510708,
        nome: 'Cardoso',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3301157,
        nome: 'Cardoso Moreira',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3113602,
        nome: 'Careaçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1301100,
        nome: 'Careiro',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1301159,
        nome: 'Careiro da Várzea',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3201308,
        nome: 'Cariacica',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2303006,
        nome: 'Caridade',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2202554,
        nome: 'Caridade do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2907103,
        nome: 'Carinhanha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2801405,
        nome: 'Carira',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2303105,
        nome: 'Cariré',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1703867,
        nome: 'Cariri do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2303204,
        nome: 'Caririaçu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2303303,
        nome: 'Cariús',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5102793,
        nome: 'Carlinda',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4104709,
        nome: 'Carlópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4304804,
        nome: 'Carlos Barbosa',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3113701,
        nome: 'Carlos Chagas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4304853,
        nome: 'Carlos Gomes',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3113800,
        nome: 'Carmésia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3301207,
        nome: 'Carmo',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3113909,
        nome: 'Carmo da Cachoeira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3114006,
        nome: 'Carmo da Mata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3114105,
        nome: 'Carmo de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3114204,
        nome: 'Carmo do Cajuru',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3114303,
        nome: 'Carmo do Paranaíba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3114402,
        nome: 'Carmo do Rio Claro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5205000,
        nome: 'Carmo do Rio Verde',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1703883,
        nome: 'Carmolândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2801504,
        nome: 'Carmópolis',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3114501,
        nome: 'Carmópolis de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2603900,
        nome: 'Carnaíba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2402402,
        nome: 'Carnaúba dos Dantas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2402501,
        nome: 'Carnaubais',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2303402,
        nome: 'Carnaubal',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2603926,
        nome: 'Carnaubeira da Penha',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3114550,
        nome: 'Carneirinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2701803,
        nome: 'Carneiros',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1400233,
        nome: 'Caroebe',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 2102804,
        nome: 'Carolina',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2604007,
        nome: 'Carpina',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3114600,
        nome: 'Carrancas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2504108,
        nome: 'Carrapateira',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1703891,
        nome: 'Carrasco Bonito',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2604106,
        nome: 'Caruaru',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2102903,
        nome: 'Carutapera',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3114709,
        nome: 'Carvalhópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3114808,
        nome: 'Carvalhos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3510807,
        nome: 'Casa Branca',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3114907,
        nome: 'Casa Grande',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2907202,
        nome: 'Casa Nova',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4304903,
        nome: 'Casca',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3115003,
        nome: 'Cascalho Rico',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4104808,
        nome: 'Cascavel',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2303501,
        nome: 'Cascavel',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1703909,
        nome: 'Caseara',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4304952,
        nome: 'Caseiros',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3301306,
        nome: 'Casimiro de Abreu',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2604155,
        nome: 'Casinhas',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2504157,
        nome: 'Casserengue',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3115102,
        nome: 'Cássia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3510906,
        nome: 'Cássia dos Coqueiros',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5002902,
        nome: 'Cassilândia',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1502400,
        nome: 'Castanhal',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5102850,
        nome: 'Castanheira',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1100908,
        nome: 'Castanheiras',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5205059,
        nome: 'Castelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3201407,
        nome: 'Castelo',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2202604,
        nome: 'Castelo do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3511003,
        nome: 'Castilho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4104907,
        nome: 'Castro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2907301,
        nome: 'Castro Alves',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3115300,
        nome: 'Cataguases',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5205109,
        nome: 'Catalão',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3511102,
        nome: 'Catanduva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4105003,
        nome: 'Catanduvas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4204004,
        nome: 'Catanduvas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2303600,
        nome: 'Catarina',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3115359,
        nome: 'Catas Altas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3115409,
        nome: 'Catas Altas da Noruega',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2604205,
        nome: 'Catende',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3511201,
        nome: 'Catiguá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2504207,
        nome: 'Catingueira',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2907400,
        nome: 'Catolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2504306,
        nome: 'Catolé do Rocha',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2907509,
        nome: 'Catu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4305009,
        nome: 'Catuípe',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3115458,
        nome: 'Catuji',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2303659,
        nome: 'Catunda',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5205208,
        nome: 'Caturaí',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2907558,
        nome: 'Caturama',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2504355,
        nome: 'Caturité',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3115474,
        nome: 'Catuti',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2303709,
        nome: 'Caucaia',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5205307,
        nome: 'Cavalcante',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3115508,
        nome: 'Caxambu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4204103,
        nome: 'Caxambu do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2103000,
        nome: 'Caxias',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4305108,
        nome: 'Caxias do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2202653,
        nome: 'Caxingó',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2402600,
        nome: 'Ceará-Mirim',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2103109,
        nome: 'Cedral',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3511300,
        nome: 'Cedral',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2303808,
        nome: 'Cedro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2604304,
        nome: 'Cedro',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2801603,
        nome: 'Cedro de São João',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3115607,
        nome: 'Cedro do Abaeté',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4204152,
        nome: 'Celso Ramos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4305116,
        nome: 'Centenário',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1704105,
        nome: 'Centenário',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4105102,
        nome: 'Centenário do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2907608,
        nome: 'Central',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3115706,
        nome: 'Central de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2103125,
        nome: 'Central do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3115805,
        nome: 'Centralina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2103158,
        nome: 'Centro do Guilherme',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2103174,
        nome: 'Centro Novo do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1100056,
        nome: 'Cerejeiras',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5205406,
        nome: 'Ceres',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3511409,
        nome: 'Cerqueira César',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3511508,
        nome: 'Cerquilho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4305124,
        nome: 'Cerrito',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4105201,
        nome: 'Cerro Azul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4305132,
        nome: 'Cerro Branco',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2402709,
        nome: 'Cerro Corá',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4305157,
        nome: 'Cerro Grande',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4305173,
        nome: 'Cerro Grande do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4305207,
        nome: 'Cerro Largo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4204178,
        nome: 'Cerro Negro',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3511607,
        nome: 'Cesário Lange',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4105300,
        nome: 'Céu Azul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5205455,
        nome: 'Cezarina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2604403,
        nome: 'Chã de Alegria',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2604502,
        nome: 'Chã Grande',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2701902,
        nome: 'Chã Preta',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3115904,
        nome: 'Chácara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3116001,
        nome: 'Chalé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305306,
        nome: 'Chapada',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1705102,
        nome: 'Chapada da Natividade',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1704600,
        nome: 'Chapada de Areia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3116100,
        nome: 'Chapada do Norte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5103007,
        nome: 'Chapada dos Guimarães',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3116159,
        nome: 'Chapada Gaúcha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5205471,
        nome: 'Chapadão do Céu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4204194,
        nome: 'Chapadão do Lageado',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5002951,
        nome: 'Chapadão do Sul',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2103208,
        nome: 'Chapadinha',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4204202,
        nome: 'Chapecó',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3511706,
        nome: 'Charqueada',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4305355,
        nome: 'Charqueadas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4305371,
        nome: 'Charrua',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2303907,
        nome: 'Chaval',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3557204,
        nome: 'Chavantes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1502509,
        nome: 'Chaves',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3116209,
        nome: 'Chiador',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305405,
        nome: 'Chiapetta',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4105409,
        nome: 'Chopinzinho',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2303931,
        nome: 'Choró',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2303956,
        nome: 'Chorozinho',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2907707,
        nome: 'Chorrochó',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4305439,
        nome: 'Chuí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100924,
        nome: 'Chupinguaia',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4305447,
        nome: 'Chuvisca',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4105508,
        nome: 'Cianorte',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2907806,
        nome: 'Cícero Dantas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4105607,
        nome: 'Cidade Gaúcha',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5205497,
        nome: 'Cidade Ocidental',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2103257,
        nome: 'Cidelândia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4305454,
        nome: 'Cidreira',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2907905,
        nome: 'Cipó',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3116308,
        nome: 'Cipotânea',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305504,
        nome: 'Ciríaco',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3116407,
        nome: 'Claraval',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3116506,
        nome: 'Claro dos Poções',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5103056,
        nome: 'Cláudia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3116605,
        nome: 'Cláudio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3511904,
        nome: 'Clementina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4105706,
        nome: 'Clevelândia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2908002,
        nome: 'Coaraci',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1301209,
        nome: 'Coari',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2202703,
        nome: 'Cocal',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2202711,
        nome: 'Cocal de Telha',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4204251,
        nome: 'Cocal do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2202729,
        nome: 'Cocal dos Alves',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5103106,
        nome: 'Cocalinho',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5205513,
        nome: 'Cocalzinho de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2908101,
        nome: 'Cocos',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1301308,
        nome: 'Codajás',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2103307,
        nome: 'Codó',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2103406,
        nome: 'Coelho Neto',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3116704,
        nome: 'Coimbra',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2702009,
        nome: 'Coité do Nóia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2202737,
        nome: 'Coivaras',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1502608,
        nome: 'Colares',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3201506,
        nome: 'Colatina',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5103205,
        nome: 'Colíder',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3512001,
        nome: 'Colina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4305587,
        nome: 'Colinas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2103505,
        nome: 'Colinas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5205521,
        nome: 'Colinas do Sul',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1705508,
        nome: 'Colinas do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1716703,
        nome: 'Colméia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5103254,
        nome: 'Colniza',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3512100,
        nome: 'Colômbia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4105805,
        nome: 'Colombo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2202752,
        nome: 'Colônia do Gurguéia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2202778,
        nome: 'Colônia do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2702108,
        nome: 'Colônia Leopoldina',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4305603,
        nome: 'Colorado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4105904,
        nome: 'Colorado',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1100064,
        nome: 'Colorado do Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3116803,
        nome: 'Coluna',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1705557,
        nome: 'Combinado',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3116902,
        nome: 'Comendador Gomes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3300951,
        nome: 'Comendador Levy Gasparian',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3117009,
        nome: 'Comercinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5103304,
        nome: 'Comodoro',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2504405,
        nome: 'Conceição',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3117108,
        nome: 'Conceição da Aparecida',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3201605,
        nome: 'Conceição da Barra',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3115201,
        nome: 'Conceição da Barra de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2908200,
        nome: 'Conceição da Feira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3117306,
        nome: 'Conceição das Alagoas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3117207,
        nome: 'Conceição das Pedras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3117405,
        nome: 'Conceição de Ipanema',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3301405,
        nome: 'Conceição de Macabu',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2908309,
        nome: 'Conceição do Almeida',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1502707,
        nome: 'Conceição do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2202802,
        nome: 'Conceição do Canindé',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3201704,
        nome: 'Conceição do Castelo',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2908408,
        nome: 'Conceição do Coité',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2908507,
        nome: 'Conceição do Jacuípe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2103554,
        nome: 'Conceição do Lago-Açu',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3117504,
        nome: 'Conceição do Mato Dentro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3117603,
        nome: 'Conceição do Pará',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3117702,
        nome: 'Conceição do Rio Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1705607,
        nome: 'Conceição do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3117801,
        nome: 'Conceição dos Ouros',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3512209,
        nome: 'Conchal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3512308,
        nome: 'Conchas',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4204301,
        nome: 'Concórdia',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1502756,
        nome: 'Concórdia do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2504504,
        nome: 'Condado',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2604601,
        nome: 'Condado',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2504603,
        nome: 'Conde',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2908606,
        nome: 'Conde',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2908705,
        nome: 'Condeúba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4305702,
        nome: 'Condor',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3117836,
        nome: 'Cônego Marinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3117876,
        nome: 'Confins',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5103353,
        nome: 'Confresa',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2504702,
        nome: 'Congo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3117900,
        nome: 'Congonhal',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3118007,
        nome: 'Congonhas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3118106,
        nome: 'Congonhas do Norte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4106001,
        nome: 'Congonhinhas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3118205,
        nome: 'Conquista',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5103361,
        nome: 'Conquista D\'Oeste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3118304,
        nome: 'Conselheiro Lafaiete',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4106100,
        nome: 'Conselheiro Mairinck',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3118403,
        nome: 'Conselheiro Pena',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3118502,
        nome: 'Consolação',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305801,
        nome: 'Constantina',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3118601,
        nome: 'Contagem',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4106209,
        nome: 'Contenda',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2908804,
        nome: 'Contendas do Sincorá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3118700,
        nome: 'Coqueiral',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305835,
        nome: 'Coqueiro Baixo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2702207,
        nome: 'Coqueiro Seco',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4305850,
        nome: 'Coqueiros do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3118809,
        nome: 'Coração de Jesus',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2908903,
        nome: 'Coração de Maria',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4106308,
        nome: 'Corbélia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3301504,
        nome: 'Cordeiro',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3512407,
        nome: 'Cordeirópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2909000,
        nome: 'Cordeiros',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4204350,
        nome: 'Cordilheira Alta',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3118908,
        nome: 'Cordisburgo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3119005,
        nome: 'Cordislândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2304004,
        nome: 'Coreaú',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2504801,
        nome: 'Coremas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5003108,
        nome: 'Corguinho',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2909109,
        nome: 'Coribe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3119104,
        nome: 'Corinto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4106407,
        nome: 'Cornélio Procópio',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3119203,
        nome: 'Coroaci',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3512506,
        nome: 'Coroados',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2103604,
        nome: 'Coroatá',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3119302,
        nome: 'Coromandel',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305871,
        nome: 'Coronel Barros',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4305900,
        nome: 'Coronel Bicaco',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4106456,
        nome: 'Coronel Domingos Soares',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2402808,
        nome: 'Coronel Ezequiel',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3119401,
        nome: 'Coronel Fabriciano',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4204400,
        nome: 'Coronel Freitas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2402907,
        nome: 'Coronel João Pessoa',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2909208,
        nome: 'Coronel João Sá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2202851,
        nome: 'Coronel José Dias',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3512605,
        nome: 'Coronel Macedo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4204459,
        nome: 'Coronel Martins',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3119500,
        nome: 'Coronel Murta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3119609,
        nome: 'Coronel Pacheco',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4305934,
        nome: 'Coronel Pilar',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5003157,
        nome: 'Coronel Sapucaia',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4106506,
        nome: 'Coronel Vivida',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3119708,
        nome: 'Coronel Xavier Chaves',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3119807,
        nome: 'Córrego Danta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3119906,
        nome: 'Córrego do Bom Jesus',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5205703,
        nome: 'Córrego do Ouro',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3119955,
        nome: 'Córrego Fundo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3120003,
        nome: 'Córrego Novo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4204558,
        nome: 'Correia Pinto',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2202901,
        nome: 'Corrente',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2604700,
        nome: 'Correntes',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2909307,
        nome: 'Correntina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2604809,
        nome: 'Cortês',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5003207,
        nome: 'Corumbá',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5205802,
        nome: 'Corumbá de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5205901,
        nome: 'Corumbaíba',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3512704,
        nome: 'Corumbataí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4106555,
        nome: 'Corumbataí do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1100072,
        nome: 'Corumbiara',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4204509,
        nome: 'Corupá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2702306,
        nome: 'Coruripe',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3512803,
        nome: 'Cosmópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3512902,
        nome: 'Cosmorama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1100080,
        nome: 'Costa Marques',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5003256,
        nome: 'Costa Rica',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2909406,
        nome: 'Cotegipe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3513009,
        nome: 'Cotia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4305959,
        nome: 'Cotiporã',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5103379,
        nome: 'Cotriguaçu',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3120102,
        nome: 'Couto de Magalhães de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1706001,
        nome: 'Couto Magalhães',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4305975,
        nome: 'Coxilha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5003306,
        nome: 'Coxim',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2504850,
        nome: 'Coxixola',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2702355,
        nome: 'Craíbas',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2304103,
        nome: 'Crateús',
        capital: 'n',
        sigla_uf: 'CE',
        imageUrl: 'assets/fa/solid/home.svg'
    },
    {
        id: 2304202,
        nome: 'Crato',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3513108,
        nome: 'Cravinhos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2909505,
        nome: 'Cravolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4204608,
        nome: 'Criciúma',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3120151,
        nome: 'Crisólita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2909604,
        nome: 'Crisópolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4306007,
        nome: 'Crissiumal',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3120201,
        nome: 'Cristais',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3513207,
        nome: 'Cristais Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4306056,
        nome: 'Cristal',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4306072,
        nome: 'Cristal do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1706100,
        nome: 'Cristalândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2203008,
        nome: 'Cristalândia do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3120300,
        nome: 'Cristália',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5206206,
        nome: 'Cristalina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3120409,
        nome: 'Cristiano Otoni',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5206305,
        nome: 'Cristianópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3120508,
        nome: 'Cristina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2801702,
        nome: 'Cristinápolis',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2203107,
        nome: 'Cristino Castro',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2909703,
        nome: 'Cristópolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5206404,
        nome: 'Crixás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1706258,
        nome: 'Crixás do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2304236,
        nome: 'Croatá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5206503,
        nome: 'Cromínia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3120607,
        nome: 'Crucilândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2304251,
        nome: 'Cruz',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4306106,
        nome: 'Cruz Alta',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2909802,
        nome: 'Cruz das Almas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2504900,
        nome: 'Cruz do Espírito Santo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4106803,
        nome: 'Cruz Machado',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3513306,
        nome: 'Cruzália',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4306130,
        nome: 'Cruzaltense',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3513405,
        nome: 'Cruzeiro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3120706,
        nome: 'Cruzeiro da Fortaleza',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4106571,
        nome: 'Cruzeiro do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4106605,
        nome: 'Cruzeiro do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4106704,
        nome: 'Cruzeiro do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4306205,
        nome: 'Cruzeiro do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1200203,
        nome: 'Cruzeiro do Sul',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2403004,
        nome: 'Cruzeta',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3120805,
        nome: 'Cruzília',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4106852,
        nome: 'Cruzmaltina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3513504,
        nome: 'Cubatão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2505006,
        nome: 'Cubati',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5103403,
        nome: 'Cuiabá',
        capital: 's',
        sigla_uf: 'MT'
    },
    {
        id: 2505105,
        nome: 'Cuité',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2505238,
        nome: 'Cuité de Mamanguape',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2505204,
        nome: 'Cuitegi',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1100940,
        nome: 'Cujubim',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5206602,
        nome: 'Cumari',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2604908,
        nome: 'Cumaru',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1502764,
        nome: 'Cumaru do Norte',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2801900,
        nome: 'Cumbe',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3513603,
        nome: 'Cunha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4204707,
        nome: 'Cunha Porã',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4204756,
        nome: 'Cunhataí',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3120839,
        nome: 'Cuparaque',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2605004,
        nome: 'Cupira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2909901,
        nome: 'Curaçá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2203206,
        nome: 'Curimatá',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1502772,
        nome: 'Curionópolis',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4106902,
        nome: 'Curitiba',
        capital: 's',
        sigla_uf: 'PR'
    },
    {
        id: 4204806,
        nome: 'Curitibanos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4107009,
        nome: 'Curiúva',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2203230,
        nome: 'Currais',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2403103,
        nome: 'Currais Novos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2505279,
        nome: 'Curral de Cima',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3120870,
        nome: 'Curral de Dentro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2203271,
        nome: 'Curral Novo do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2505303,
        nome: 'Curral Velho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1502806,
        nome: 'Curralinho',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2203255,
        nome: 'Curralinhos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1502855,
        nome: 'Curuá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1502905,
        nome: 'Curuçá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2103703,
        nome: 'Cururupu',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5103437,
        nome: 'Curvelândia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3120904,
        nome: 'Curvelo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2605103,
        nome: 'Custódia',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1600212,
        nome: 'Cutias',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 5206701,
        nome: 'Damianópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2505352,
        nome: 'Damião',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5206800,
        nome: 'Damolândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1706506,
        nome: 'Darcinópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2910008,
        nome: 'Dário Meira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3121001,
        nome: 'Datas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4306304,
        nome: 'David Canabarro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2103752,
        nome: 'Davinópolis',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5206909,
        nome: 'Davinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3121100,
        nome: 'Delfim Moreira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3121209,
        nome: 'Delfinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2702405,
        nome: 'Delmiro Gouveia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3121258,
        nome: 'Delta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2203305,
        nome: 'Demerval Lobão',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5103452,
        nome: 'Denise',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5003454,
        nome: 'Deodápolis',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2304269,
        nome: 'Deputado Irapuan Pinheiro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4306320,
        nome: 'Derrubadas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3513702,
        nome: 'Descalvado',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4204905,
        nome: 'Descanso',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3121308,
        nome: 'Descoberto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2505402,
        nome: 'Desterro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3121407,
        nome: 'Desterro de Entre Rios',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3121506,
        nome: 'Desterro do Melo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4306353,
        nome: 'Dezesseis de Novembro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3513801,
        nome: 'Diadema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2505600,
        nome: 'Diamante',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4107157,
        nome: 'Diamante D\'Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4107108,
        nome: 'Diamante do Norte',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4107124,
        nome: 'Diamante do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3121605,
        nome: 'Diamantina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5103502,
        nome: 'Diamantino',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1707009,
        nome: 'Dianópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2910057,
        nome: 'Dias d\'Ávila',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4306379,
        nome: 'Dilermando de Aguiar',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3121704,
        nome: 'Diogo de Vasconcelos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3121803,
        nome: 'Dionísio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4205001,
        nome: 'Dionísio Cerqueira',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5207105,
        nome: 'Diorama',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3513850,
        nome: 'Dirce Reis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2203354,
        nome: 'Dirceu Arcoverde',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2802007,
        nome: 'Divina Pastora',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3121902,
        nome: 'Divinésia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122009,
        nome: 'Divino',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122108,
        nome: 'Divino das Laranjeiras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3201803,
        nome: 'Divino de São Lourenço',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3513900,
        nome: 'Divinolândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3122207,
        nome: 'Divinolândia de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122306,
        nome: 'Divinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5208301,
        nome: 'Divinópolis de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1707108,
        nome: 'Divinópolis do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3122355,
        nome: 'Divisa Alegre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122405,
        nome: 'Divisa Nova',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122454,
        nome: 'Divisópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3514007,
        nome: 'Dobrada',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3514106,
        nome: 'Dois Córregos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4306403,
        nome: 'Dois Irmãos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4306429,
        nome: 'Dois Irmãos das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5003488,
        nome: 'Dois Irmãos do Buriti',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1707207,
        nome: 'Dois Irmãos do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4306452,
        nome: 'Dois Lajeados',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2702504,
        nome: 'Dois Riachos',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4107207,
        nome: 'Dois Vizinhos',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3514205,
        nome: 'Dolcinópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5103601,
        nome: 'Dom Aquino',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2910107,
        nome: 'Dom Basílio',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3122470,
        nome: 'Dom Bosco',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122504,
        nome: 'Dom Cavati',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1502939,
        nome: 'Dom Eliseu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2203404,
        nome: 'Dom Expedito Lopes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4306502,
        nome: 'Dom Feliciano',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2203453,
        nome: 'Dom Inocêncio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3122603,
        nome: 'Dom Joaquim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2910206,
        nome: 'Dom Macedo Costa',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4306601,
        nome: 'Dom Pedrito',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2103802,
        nome: 'Dom Pedro',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4306551,
        nome: 'Dom Pedro de Alcântara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3122702,
        nome: 'Dom Silvério',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3122801,
        nome: 'Dom Viçoso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3201902,
        nome: 'Domingos Martins',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2203420,
        nome: 'Domingos Mourão',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4205100,
        nome: 'Dona Emma',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3122900,
        nome: 'Dona Eusébia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4306700,
        nome: 'Dona Francisca',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2505709,
        nome: 'Dona Inês',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3123007,
        nome: 'Dores de Campos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3123106,
        nome: 'Dores de Guanhães',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3123205,
        nome: 'Dores do Indaiá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3202009,
        nome: 'Dores do Rio Preto',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3123304,
        nome: 'Dores do Turvo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3123403,
        nome: 'Doresópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2605152,
        nome: 'Dormentes',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5003504,
        nome: 'Douradina',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4107256,
        nome: 'Douradina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3514304,
        nome: 'Dourado',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3123502,
        nome: 'Douradoquara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5003702,
        nome: 'Dourados',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4107306,
        nome: 'Doutor Camargo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4306734,
        nome: 'Doutor Maurício Cardoso',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4205159,
        nome: 'Doutor Pedrinho',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4306759,
        nome: 'Doutor Ricardo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2403202,
        nome: 'Doutor Severiano',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4128633,
        nome: 'Doutor Ulysses',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5207253,
        nome: 'Doverlândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3514403,
        nome: 'Dracena',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3514502,
        nome: 'Duartina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3301603,
        nome: 'Duas Barras',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2505808,
        nome: 'Duas Estradas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1707306,
        nome: 'Dueré',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3514601,
        nome: 'Dumont',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2103901,
        nome: 'Duque Bacelar',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3301702,
        nome: 'Duque de Caxias',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3123528,
        nome: 'Durandé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3514700,
        nome: 'Echaporã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3202108,
        nome: 'Ecoporanga',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5207352,
        nome: 'Edealina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5207402,
        nome: 'Edéia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1301407,
        nome: 'Eirunepé',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 5003751,
        nome: 'Eldorado',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3514809,
        nome: 'Eldorado',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1502954,
        nome: 'Eldorado do Carajás',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4306767,
        nome: 'Eldorado do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2203503,
        nome: 'Elesbão Veloso',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3514908,
        nome: 'Elias Fausto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2203602,
        nome: 'Eliseu Martins',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3514924,
        nome: 'Elisiário',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2910305,
        nome: 'Elísio Medrado',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3123601,
        nome: 'Elói Mendes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2505907,
        nome: 'Emas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3514957,
        nome: 'Embaúba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515004,
        nome: 'Embu das Artes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515103,
        nome: 'Embu-Guaçu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515129,
        nome: 'Emilianópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4306809,
        nome: 'Encantado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2403301,
        nome: 'Encanto',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2910404,
        nome: 'Encruzilhada',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4306908,
        nome: 'Encruzilhada do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4107405,
        nome: 'Enéas Marques',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4107504,
        nome: 'Engenheiro Beltrão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3123700,
        nome: 'Engenheiro Caldas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3515152,
        nome: 'Engenheiro Coelho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3123809,
        nome: 'Engenheiro Navarro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3301801,
        nome: 'Engenheiro Paulo de Frontin',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4306924,
        nome: 'Engenho Velho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3123858,
        nome: 'Entre Folhas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2910503,
        nome: 'Entre Rios',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4205175,
        nome: 'Entre Rios',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3123908,
        nome: 'Entre Rios de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4107538,
        nome: 'Entre Rios do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4306957,
        nome: 'Entre Rios do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4306932,
        nome: 'Entre-Ijuís',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1301506,
        nome: 'Envira',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1200252,
        nome: 'Epitaciolândia',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2403400,
        nome: 'Equador',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4306973,
        nome: 'Erebango',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4307005,
        nome: 'Erechim',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2304277,
        nome: 'Ererê',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2900504,
        nome: 'Érico Cardoso',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4205191,
        nome: 'Ermo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4307054,
        nome: 'Ernestina',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4307203,
        nome: 'Erval Grande',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4307302,
        nome: 'Erval Seco',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4205209,
        nome: 'Erval Velho',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3124005,
        nome: 'Ervália',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2605202,
        nome: 'Escada',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4307401,
        nome: 'Esmeralda',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3124104,
        nome: 'Esmeraldas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3124203,
        nome: 'Espera Feliz',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2506004,
        nome: 'Esperança',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4307450,
        nome: 'Esperança do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4107520,
        nome: 'Esperança Nova',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1707405,
        nome: 'Esperantina',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2203701,
        nome: 'Esperantina',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2104008,
        nome: 'Esperantinópolis',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4107546,
        nome: 'Espigão Alto do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1100098,
        nome: 'Espigão D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3124302,
        nome: 'Espinosa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2403509,
        nome: 'Espírito Santo',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3124401,
        nome: 'Espírito Santo do Dourado',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3515186,
        nome: 'Espírito Santo do Pinhal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515194,
        nome: 'Espírito Santo do Turvo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2910602,
        nome: 'Esplanada',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4307500,
        nome: 'Espumoso',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4307559,
        nome: 'Estação',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2802106,
        nome: 'Estância',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4307609,
        nome: 'Estância Velha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4307708,
        nome: 'Esteio',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3124500,
        nome: 'Estiva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3557303,
        nome: 'Estiva Gerbi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2104057,
        nome: 'Estreito',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4307807,
        nome: 'Estrela',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3515202,
        nome: 'Estrela d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3124609,
        nome: 'Estrela Dalva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2702553,
        nome: 'Estrela de Alagoas',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3124708,
        nome: 'Estrela do Indaiá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5207501,
        nome: 'Estrela do Norte',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3515301,
        nome: 'Estrela do Norte',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3124807,
        nome: 'Estrela do Sul',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4307815,
        nome: 'Estrela Velha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2910701,
        nome: 'Euclides da Cunha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3515350,
        nome: 'Euclides da Cunha Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4307831,
        nome: 'Eugênio de Castro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3124906,
        nome: 'Eugenópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2910727,
        nome: 'Eunápolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2304285,
        nome: 'Eusébio',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3125002,
        nome: 'Ewbank da Câmara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3125101,
        nome: 'Extrema',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2403608,
        nome: 'Extremoz',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2605301,
        nome: 'Exu',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2506103,
        nome: 'Fagundes',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4307864,
        nome: 'Fagundes Varela',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5207535,
        nome: 'Faina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3125200,
        nome: 'Fama',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3125309,
        nome: 'Faria Lemos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2304301,
        nome: 'Farias Brito',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1503002,
        nome: 'Faro',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4107553,
        nome: 'Farol',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4307906,
        nome: 'Farroupilha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3515400,
        nome: 'Fartura',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2203750,
        nome: 'Fartura do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1707553,
        nome: 'Fátima',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2910750,
        nome: 'Fátima',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5003801,
        nome: 'Fátima do Sul',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4107603,
        nome: 'Faxinal',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4308003,
        nome: 'Faxinal do Soturno',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4205308,
        nome: 'Faxinal dos Guedes',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4308052,
        nome: 'Faxinalzinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5207600,
        nome: 'Fazenda Nova',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4107652,
        nome: 'Fazenda Rio Grande',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4308078,
        nome: 'Fazenda Vilanova',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1200302,
        nome: 'Feijó',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2910776,
        nome: 'Feira da Mata',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2910800,
        nome: 'Feira de Santana',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2702603,
        nome: 'Feira Grande',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2605400,
        nome: 'Feira Nova',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2802205,
        nome: 'Feira Nova',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2104073,
        nome: 'Feira Nova do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3125408,
        nome: 'Felício dos Santos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2403707,
        nome: 'Felipe Guerra',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3125606,
        nome: 'Felisburgo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3125705,
        nome: 'Felixlândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4308102,
        nome: 'Feliz',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2702702,
        nome: 'Feliz Deserto',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5103700,
        nome: 'Feliz Natal',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4107702,
        nome: 'Fênix',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4107736,
        nome: 'Fernandes Pinheiro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3125804,
        nome: 'Fernandes Tourinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2605459,
        nome: 'Fernando de Noronha',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2104081,
        nome: 'Fernando Falcão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2403756,
        nome: 'Fernando Pedroza',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3515608,
        nome: 'Fernando Prestes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515509,
        nome: 'Fernandópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515657,
        nome: 'Fernão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3515707,
        nome: 'Ferraz de Vasconcelos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1600238,
        nome: 'Ferreira Gomes',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 2605509,
        nome: 'Ferreiros',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3125903,
        nome: 'Ferros',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3125952,
        nome: 'Fervedouro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4107751,
        nome: 'Figueira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5003900,
        nome: 'Figueirão',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1707652,
        nome: 'Figueirópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5103809,
        nome: 'Figueirópolis D\'Oeste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1707702,
        nome: 'Filadélfia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2910859,
        nome: 'Filadélfia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2910909,
        nome: 'Firmino Alves',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5207808,
        nome: 'Firminópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2702801,
        nome: 'Flexeiras',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4107850,
        nome: 'Flor da Serra do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4205357,
        nome: 'Flor do Sertão',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3515806,
        nome: 'Flora Rica',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4107801,
        nome: 'Floraí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2403806,
        nome: 'Florânia',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3515905,
        nome: 'Floreal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2605608,
        nome: 'Flores',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4308201,
        nome: 'Flores da Cunha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5207907,
        nome: 'Flores de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2203800,
        nome: 'Flores do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4107900,
        nome: 'Floresta',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2605707,
        nome: 'Floresta',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2911006,
        nome: 'Floresta Azul',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1503044,
        nome: 'Floresta do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2203859,
        nome: 'Floresta do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3126000,
        nome: 'Florestal',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4108007,
        nome: 'Florestópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2203909,
        nome: 'Floriano',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4308250,
        nome: 'Floriano Peixoto',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4205407,
        nome: 'Florianópolis',
        capital: 's',
        sigla_uf: 'SC'
    },
    {
        id: 4108106,
        nome: 'Flórida',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3516002,
        nome: 'Flórida Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3516101,
        nome: 'Florínia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1301605,
        nome: 'Fonte Boa',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4308300,
        nome: 'Fontoura Xavier',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3126109,
        nome: 'Formiga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4308409,
        nome: 'Formigueiro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5208004,
        nome: 'Formosa',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2104099,
        nome: 'Formosa da Serra Negra',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4108205,
        nome: 'Formosa do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2911105,
        nome: 'Formosa do Rio Preto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4205431,
        nome: 'Formosa do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5208103,
        nome: 'Formoso',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3126208,
        nome: 'Formoso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1708205,
        nome: 'Formoso do Araguaia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4308433,
        nome: 'Forquetinha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2304350,
        nome: 'Forquilha',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4205456,
        nome: 'Forquilhinha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2304400,
        nome: 'Fortaleza',
        capital: 's',
        sigla_uf: 'CE'
    },
    {
        id: 3126307,
        nome: 'Fortaleza de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1708254,
        nome: 'Fortaleza do Tabocão',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2104107,
        nome: 'Fortaleza dos Nogueiras',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4308458,
        nome: 'Fortaleza dos Valos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2304459,
        nome: 'Fortim',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2104206,
        nome: 'Fortuna',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3126406,
        nome: 'Fortuna de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4108304,
        nome: 'Foz do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4108452,
        nome: 'Foz do Jordão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4205506,
        nome: 'Fraiburgo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3516200,
        nome: 'Franca',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2204006,
        nome: 'Francinópolis',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4108320,
        nome: 'Francisco Alves',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2204105,
        nome: 'Francisco Ayres',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3126505,
        nome: 'Francisco Badaró',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4108403,
        nome: 'Francisco Beltrão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2403905,
        nome: 'Francisco Dantas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3126604,
        nome: 'Francisco Dumont',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2204154,
        nome: 'Francisco Macedo',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3516309,
        nome: 'Francisco Morato',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3126703,
        nome: 'Francisco Sá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2204204,
        nome: 'Francisco Santos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3126752,
        nome: 'Franciscópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3516408,
        nome: 'Franco da Rocha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2304509,
        nome: 'Frecheirinha',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4308508,
        nome: 'Frederico Westphalen',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3126802,
        nome: 'Frei Gaspar',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3126901,
        nome: 'Frei Inocêncio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3126950,
        nome: 'Frei Lagonegro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2506202,
        nome: 'Frei Martinho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2605806,
        nome: 'Frei Miguelinho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2802304,
        nome: 'Frei Paulo',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4205555,
        nome: 'Frei Rogério',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3127008,
        nome: 'Fronteira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3127057,
        nome: 'Fronteira dos Vales',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2204303,
        nome: 'Fronteiras',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3127073,
        nome: 'Fruta de Leite',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3127107,
        nome: 'Frutal',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2404002,
        nome: 'Frutuoso Gomes',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3202207,
        nome: 'Fundão',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3127206,
        nome: 'Funilândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3516507,
        nome: 'Gabriel Monteiro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2506251,
        nome: 'Gado Bravo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3516606,
        nome: 'Gália',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3127305,
        nome: 'Galiléia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2404101,
        nome: 'Galinhos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4205605,
        nome: 'Galvão',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2605905,
        nome: 'Gameleira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5208152,
        nome: 'Gameleira de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3127339,
        nome: 'Gameleiras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2911204,
        nome: 'Gandu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2606002,
        nome: 'Garanhuns',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2802403,
        nome: 'Gararu',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3516705,
        nome: 'Garça',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4308607,
        nome: 'Garibaldi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4205704,
        nome: 'Garopaba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1503077,
        nome: 'Garrafão do Norte',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4308656,
        nome: 'Garruchos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4205803,
        nome: 'Garuva',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4205902,
        nome: 'Gaspar',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3516804,
        nome: 'Gastão Vidigal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5103858,
        nome: 'Gaúcha do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4308706,
        nome: 'Gaurama',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2911253,
        nome: 'Gavião',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3516853,
        nome: 'Gavião Peixoto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2204352,
        nome: 'Geminiano',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4308805,
        nome: 'General Câmara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5103908,
        nome: 'General Carneiro',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4108502,
        nome: 'General Carneiro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2802502,
        nome: 'General Maynard',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3516903,
        nome: 'General Salgado',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2304608,
        nome: 'General Sampaio',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4308854,
        nome: 'Gentil',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2911303,
        nome: 'Gentio do Ouro',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3517000,
        nome: 'Getulina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4308904,
        nome: 'Getúlio Vargas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2204402,
        nome: 'Gilbués',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2702900,
        nome: 'Girau do Ponciano',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4309001,
        nome: 'Giruá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3127354,
        nome: 'Glaucilândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3517109,
        nome: 'Glicério',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2911402,
        nome: 'Glória',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5103957,
        nome: 'Glória D\'Oeste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5004007,
        nome: 'Glória de Dourados',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2606101,
        nome: 'Glória do Goitá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4309050,
        nome: 'Glorinha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2104305,
        nome: 'Godofredo Viana',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4108551,
        nome: 'Godoy Moreira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3127370,
        nome: 'Goiabeira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3127388,
        nome: 'Goianá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2606200,
        nome: 'Goiana',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5208400,
        nome: 'Goianápolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5208509,
        nome: 'Goiandira',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5208608,
        nome: 'Goianésia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1503093,
        nome: 'Goianésia do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5208707,
        nome: 'Goiânia',
        capital: 's',
        sigla_uf: 'GO'
    },
    {
        id: 2404200,
        nome: 'Goianinha',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5208806,
        nome: 'Goianira',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1708304,
        nome: 'Goianorte',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5208905,
        nome: 'Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1709005,
        nome: 'Goiatins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5209101,
        nome: 'Goiatuba',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4108601,
        nome: 'Goioerê',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4108650,
        nome: 'Goioxim',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3127404,
        nome: 'Gonçalves',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2104404,
        nome: 'Gonçalves Dias',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2911501,
        nome: 'Gongogi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3127503,
        nome: 'Gonzaga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3127602,
        nome: 'Gouveia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5209150,
        nome: 'Gouvelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2104503,
        nome: 'Governador Archer',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4206009,
        nome: 'Governador Celso Ramos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2404309,
        nome: 'Governador Dix-Sept Rosado',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2104552,
        nome: 'Governador Edison Lobão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2104602,
        nome: 'Governador Eugênio Barros',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1101005,
        nome: 'Governador Jorge Teixeira',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3202256,
        nome: 'Governador Lindenberg',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2104628,
        nome: 'Governador Luiz Rocha',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2911600,
        nome: 'Governador Mangabeira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2104651,
        nome: 'Governador Newton Bello',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2104677,
        nome: 'Governador Nunes Freire',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3127701,
        nome: 'Governador Valadares',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2304657,
        nome: 'Graça',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2104701,
        nome: 'Graça Aranha',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2802601,
        nome: 'Gracho Cardoso',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2104800,
        nome: 'Grajaú',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4309100,
        nome: 'Gramado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4309126,
        nome: 'Gramado dos Loureiros',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4309159,
        nome: 'Gramado Xavier',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4108700,
        nome: 'Grandes Rios',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2606309,
        nome: 'Granito',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2304707,
        nome: 'Granja',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2304806,
        nome: 'Granjeiro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3127800,
        nome: 'Grão Mogol',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4206108,
        nome: 'Grão Pará',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2606408,
        nome: 'Gravatá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4309209,
        nome: 'Gravataí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4206207,
        nome: 'Gravatal',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2304905,
        nome: 'Groaíras',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2404408,
        nome: 'Grossos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3127909,
        nome: 'Grupiara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4309258,
        nome: 'Guabiju',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4206306,
        nome: 'Guabiruba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3202306,
        nome: 'Guaçuí',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2204501,
        nome: 'Guadalupe',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4309308,
        nome: 'Guaíba',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3517208,
        nome: 'Guaiçara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3517307,
        nome: 'Guaimbê',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3517406,
        nome: 'Guaíra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4108809,
        nome: 'Guaíra',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4108908,
        nome: 'Guairaçá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2304954,
        nome: 'Guaiúba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1301654,
        nome: 'Guajará',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1100106,
        nome: 'Guajará-Mirim',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2911659,
        nome: 'Guajeru',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2404507,
        nome: 'Guamaré',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4108957,
        nome: 'Guamiranga',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2911709,
        nome: 'Guanambi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3128006,
        nome: 'Guanhães',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3128105,
        nome: 'Guapé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3517505,
        nome: 'Guapiaçu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3517604,
        nome: 'Guapiara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3301850,
        nome: 'Guapimirim',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4109005,
        nome: 'Guapirama',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5209200,
        nome: 'Guapó',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4309407,
        nome: 'Guaporé',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4109104,
        nome: 'Guaporema',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3517703,
        nome: 'Guará',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2506301,
        nome: 'Guarabira',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3517802,
        nome: 'Guaraçaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3517901,
        nome: 'Guaraci',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4109203,
        nome: 'Guaraci',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3128204,
        nome: 'Guaraciaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4206405,
        nome: 'Guaraciaba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2305001,
        nome: 'Guaraciaba do Norte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3128253,
        nome: 'Guaraciama',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1709302,
        nome: 'Guaraí',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5209291,
        nome: 'Guaraíta',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2305100,
        nome: 'Guaramiranga',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4206504,
        nome: 'Guaramirim',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3128303,
        nome: 'Guaranésia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3128402,
        nome: 'Guarani',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3518008,
        nome: 'Guarani d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4309506,
        nome: 'Guarani das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5209408,
        nome: 'Guarani de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4109302,
        nome: 'Guaraniaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3518107,
        nome: 'Guarantã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5104104,
        nome: 'Guarantã do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3202405,
        nome: 'Guarapari',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4109401,
        nome: 'Guarapuava',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4109500,
        nome: 'Guaraqueçaba',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3128501,
        nome: 'Guarará',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3518206,
        nome: 'Guararapes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3518305,
        nome: 'Guararema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2911808,
        nome: 'Guaratinga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3518404,
        nome: 'Guaratinguetá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4109609,
        nome: 'Guaratuba',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3128600,
        nome: 'Guarda-Mor',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3518503,
        nome: 'Guareí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3518602,
        nome: 'Guariba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2204550,
        nome: 'Guaribas',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5209457,
        nome: 'Guarinos',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3518701,
        nome: 'Guarujá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4206603,
        nome: 'Guarujá do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3518800,
        nome: 'Guarulhos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4206652,
        nome: 'Guatambú',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3518859,
        nome: 'Guatapará',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3128709,
        nome: 'Guaxupé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5004106,
        nome: 'Guia Lopes da Laguna',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3128808,
        nome: 'Guidoval',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2104909,
        nome: 'Guimarães',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3128907,
        nome: 'Guimarânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5104203,
        nome: 'Guiratinga',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3129004,
        nome: 'Guiricema',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3129103,
        nome: 'Gurinhatã',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2506400,
        nome: 'Gurinhém',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2506509,
        nome: 'Gurjão',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1503101,
        nome: 'Gurupá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1709500,
        nome: 'Gurupi',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3518909,
        nome: 'Guzolândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4309555,
        nome: 'Harmonia',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5209606,
        nome: 'Heitoraí',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3129202,
        nome: 'Heliodora',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2911857,
        nome: 'Heliópolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3519006,
        nome: 'Herculândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4307104,
        nome: 'Herval',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4206702,
        nome: 'Herval d\'Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4309571,
        nome: 'Herveiras',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5209705,
        nome: 'Hidrolândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2305209,
        nome: 'Hidrolândia',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5209804,
        nome: 'Hidrolina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3519055,
        nome: 'Holambra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4109658,
        nome: 'Honório Serpa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2305233,
        nome: 'Horizonte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4309605,
        nome: 'Horizontina',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3519071,
        nome: 'Hortolândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2204600,
        nome: 'Hugo Napoleão',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4309654,
        nome: 'Hulha Negra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4309704,
        nome: 'Humaitá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1301704,
        nome: 'Humaitá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2105005,
        nome: 'Humberto de Campos',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3519105,
        nome: 'Iacanga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5209903,
        nome: 'Iaciara',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3519204,
        nome: 'Iacri',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2911907,
        nome: 'Iaçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3129301,
        nome: 'Iapu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3519253,
        nome: 'Iaras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2606507,
        nome: 'Iati',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4109708,
        nome: 'Ibaiti',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4309753,
        nome: 'Ibarama',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2305266,
        nome: 'Ibaretama',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3519303,
        nome: 'Ibaté',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2703007,
        nome: 'Ibateguara',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3202454,
        nome: 'Ibatiba',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4109757,
        nome: 'Ibema',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3129400,
        nome: 'Ibertioga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3129509,
        nome: 'Ibiá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4309803,
        nome: 'Ibiaçá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3129608,
        nome: 'Ibiaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4206751,
        nome: 'Ibiam',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2305308,
        nome: 'Ibiapina',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2506608,
        nome: 'Ibiara',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2912004,
        nome: 'Ibiassucê',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2912103,
        nome: 'Ibicaraí',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4206801,
        nome: 'Ibicaré',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2912202,
        nome: 'Ibicoara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2912301,
        nome: 'Ibicuí',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2305332,
        nome: 'Ibicuitinga',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2606606,
        nome: 'Ibimirim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2912400,
        nome: 'Ibipeba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2912509,
        nome: 'Ibipitanga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4109807,
        nome: 'Ibiporã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2912608,
        nome: 'Ibiquera',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3519402,
        nome: 'Ibirá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3129657,
        nome: 'Ibiracatu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3129707,
        nome: 'Ibiraci',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3202504,
        nome: 'Ibiraçu',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4309902,
        nome: 'Ibiraiaras',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2606705,
        nome: 'Ibirajuba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4206900,
        nome: 'Ibirama',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2912707,
        nome: 'Ibirapitanga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2912806,
        nome: 'Ibirapuã',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4309951,
        nome: 'Ibirapuitã',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3519501,
        nome: 'Ibirarema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2912905,
        nome: 'Ibirataia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3129806,
        nome: 'Ibirité',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4310009,
        nome: 'Ibirubá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2913002,
        nome: 'Ibitiara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3519600,
        nome: 'Ibitinga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3202553,
        nome: 'Ibitirama',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2913101,
        nome: 'Ibititá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3129905,
        nome: 'Ibitiúra de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3130002,
        nome: 'Ibituruna',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3519709,
        nome: 'Ibiúna',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2913200,
        nome: 'Ibotirama',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2305357,
        nome: 'Icapuí',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4207007,
        nome: 'Içara',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3130051,
        nome: 'Icaraí de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4109906,
        nome: 'Icaraíma',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2105104,
        nome: 'Icatu',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3519808,
        nome: 'Icém',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2913309,
        nome: 'Ichu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2305407,
        nome: 'Icó',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3202603,
        nome: 'Iconha',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2404606,
        nome: 'Ielmo Marinho',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3519907,
        nome: 'Iepê',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2703106,
        nome: 'Igaci',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2913408,
        nome: 'Igaporã',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3520004,
        nome: 'Igaraçu do Tietê',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2502607,
        nome: 'Igaracy',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3520103,
        nome: 'Igarapava',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3130101,
        nome: 'Igarapé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2105153,
        nome: 'Igarapé do Meio',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2105203,
        nome: 'Igarapé Grande',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1503200,
        nome: 'Igarapé-Açu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1503309,
        nome: 'Igarapé-Miri',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2606804,
        nome: 'Igarassu',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3520202,
        nome: 'Igaratá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3130200,
        nome: 'Igaratinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2913457,
        nome: 'Igrapiúna',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2703205,
        nome: 'Igreja Nova',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4310108,
        nome: 'Igrejinha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3301876,
        nome: 'Iguaba Grande',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2913507,
        nome: 'Iguaí',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3520301,
        nome: 'Iguape',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4110003,
        nome: 'Iguaraçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2606903,
        nome: 'Iguaracy',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3130309,
        nome: 'Iguatama',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5004304,
        nome: 'Iguatemi',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2305506,
        nome: 'Iguatu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4110052,
        nome: 'Iguatu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3130408,
        nome: 'Ijaci',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4310207,
        nome: 'Ijuí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3520426,
        nome: 'Ilha Comprida',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2802700,
        nome: 'Ilha das Flores',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2607604,
        nome: 'Ilha de Itamaracá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2204659,
        nome: 'Ilha Grande',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3520442,
        nome: 'Ilha Solteira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3520400,
        nome: 'Ilhabela',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2913606,
        nome: 'Ilhéus',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4207106,
        nome: 'Ilhota',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3130507,
        nome: 'Ilicínea',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4310306,
        nome: 'Ilópolis',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2506707,
        nome: 'Imaculada',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4207205,
        nome: 'Imaruí',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4110078,
        nome: 'Imbaú',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4310330,
        nome: 'Imbé',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3130556,
        nome: 'Imbé de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4207304,
        nome: 'Imbituba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4110102,
        nome: 'Imbituva',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4207403,
        nome: 'Imbuia',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4310363,
        nome: 'Imigrante',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2105302,
        nome: 'Imperatriz',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4110201,
        nome: 'Inácio Martins',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5209937,
        nome: 'Inaciolândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2607000,
        nome: 'Inajá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4110300,
        nome: 'Inajá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3130606,
        nome: 'Inconfidentes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3130655,
        nome: 'Indaiabira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4207502,
        nome: 'Indaial',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3520509,
        nome: 'Indaiatuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4310405,
        nome: 'Independência',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2305605,
        nome: 'Independência',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3520608,
        nome: 'Indiana',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4110409,
        nome: 'Indianópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3130705,
        nome: 'Indianópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3520707,
        nome: 'Indiaporã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5209952,
        nome: 'Indiara',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2802809,
        nome: 'Indiaroba',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 5104500,
        nome: 'Indiavaí',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2506806,
        nome: 'Ingá',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3130804,
        nome: 'Ingaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2607109,
        nome: 'Ingazeira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4310413,
        nome: 'Inhacorá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2913705,
        nome: 'Inhambupe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1503408,
        nome: 'Inhangapi',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2703304,
        nome: 'Inhapi',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3130903,
        nome: 'Inhapim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3131000,
        nome: 'Inhaúma',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2204709,
        nome: 'Inhuma',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5210000,
        nome: 'Inhumas',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3131109,
        nome: 'Inimutaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5004403,
        nome: 'Inocência',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3520806,
        nome: 'Inúbia Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4207577,
        nome: 'Iomerê',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3131158,
        nome: 'Ipaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5210109,
        nome: 'Ipameri',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3131208,
        nome: 'Ipanema',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2404705,
        nome: 'Ipanguaçu',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2305654,
        nome: 'Ipaporanga',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3131307,
        nome: 'Ipatinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2305704,
        nome: 'Ipaumirim',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3520905,
        nome: 'Ipaussu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4310439,
        nome: 'Ipê',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2913804,
        nome: 'Ipecaetá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3521002,
        nome: 'Iperó',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3521101,
        nome: 'Ipeúna',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3131406,
        nome: 'Ipiaçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2913903,
        nome: 'Ipiaú',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3521150,
        nome: 'Ipiguá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2914000,
        nome: 'Ipirá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4207601,
        nome: 'Ipira',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4110508,
        nome: 'Ipiranga',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5210158,
        nome: 'Ipiranga de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5104526,
        nome: 'Ipiranga do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2204808,
        nome: 'Ipiranga do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4310462,
        nome: 'Ipiranga do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1301803,
        nome: 'Ipixuna',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1503457,
        nome: 'Ipixuna do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2607208,
        nome: 'Ipojuca',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4110607,
        nome: 'Iporã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5210208,
        nome: 'Iporá',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4207650,
        nome: 'Iporã do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3521200,
        nome: 'Iporanga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2305803,
        nome: 'Ipu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3521309,
        nome: 'Ipuã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4207684,
        nome: 'Ipuaçu',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2607307,
        nome: 'Ipubi',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2404804,
        nome: 'Ipueira',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1709807,
        nome: 'Ipueiras',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2305902,
        nome: 'Ipueiras',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3131505,
        nome: 'Ipuiúna',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4207700,
        nome: 'Ipumirim',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2914109,
        nome: 'Ipupiara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1400282,
        nome: 'Iracema',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 2306009,
        nome: 'Iracema',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4110656,
        nome: 'Iracema do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3521408,
        nome: 'Iracemápolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4207759,
        nome: 'Iraceminha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4310504,
        nome: 'Iraí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3131604,
        nome: 'Iraí de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2914208,
        nome: 'Irajuba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2914307,
        nome: 'Iramaia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1301852,
        nome: 'Iranduba',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4207809,
        nome: 'Irani',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3521507,
        nome: 'Irapuã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3521606,
        nome: 'Irapuru',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2914406,
        nome: 'Iraquara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2914505,
        nome: 'Irará',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4110706,
        nome: 'Irati',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4207858,
        nome: 'Irati',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2306108,
        nome: 'Irauçuba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2914604,
        nome: 'Irecê',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4110805,
        nome: 'Iretama',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4207908,
        nome: 'Irineópolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1503507,
        nome: 'Irituia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3202652,
        nome: 'Irupi',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2204907,
        nome: 'Isaías Coelho',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5210307,
        nome: 'Israelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4208005,
        nome: 'Itá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4310538,
        nome: 'Itaara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2506905,
        nome: 'Itabaiana',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2802908,
        nome: 'Itabaiana',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2803005,
        nome: 'Itabaianinha',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2914653,
        nome: 'Itabela',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3521705,
        nome: 'Itaberá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2914703,
        nome: 'Itaberaba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5210406,
        nome: 'Itaberaí',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2803104,
        nome: 'Itabi',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3131703,
        nome: 'Itabira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3131802,
        nome: 'Itabirinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3131901,
        nome: 'Itabirito',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3301900,
        nome: 'Itaboraí',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2914802,
        nome: 'Itabuna',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1710508,
        nome: 'Itacajá',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3132008,
        nome: 'Itacambira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3132107,
        nome: 'Itacarambi',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2914901,
        nome: 'Itacaré',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1301902,
        nome: 'Itacoatiara',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2607406,
        nome: 'Itacuruba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4310553,
        nome: 'Itacurubi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2915007,
        nome: 'Itaeté',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2915106,
        nome: 'Itagi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2915205,
        nome: 'Itagibá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2915304,
        nome: 'Itagimirim',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3202702,
        nome: 'Itaguaçu',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2915353,
        nome: 'Itaguaçu da Bahia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3302007,
        nome: 'Itaguaí',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4110904,
        nome: 'Itaguajé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3132206,
        nome: 'Itaguara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5210562,
        nome: 'Itaguari',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5210604,
        nome: 'Itaguaru',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1710706,
        nome: 'Itaguatins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3521804,
        nome: 'Itaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2607505,
        nome: 'Itaíba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2306207,
        nome: 'Itaiçaba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2205003,
        nome: 'Itainópolis',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4208104,
        nome: 'Itaiópolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2105351,
        nome: 'Itaipava do Grajaú',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3132305,
        nome: 'Itaipé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4110953,
        nome: 'Itaipulândia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2306256,
        nome: 'Itaitinga',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1503606,
        nome: 'Itaituba',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2404853,
        nome: 'Itajá',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5210802,
        nome: 'Itajá',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4208203,
        nome: 'Itajaí',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3521903,
        nome: 'Itajobi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3522000,
        nome: 'Itaju',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2915403,
        nome: 'Itaju do Colônia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3132404,
        nome: 'Itajubá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2915502,
        nome: 'Itajuípe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3302056,
        nome: 'Italva',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2915601,
        nome: 'Itamaraju',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3132503,
        nome: 'Itamarandiba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1301951,
        nome: 'Itamarati',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3132602,
        nome: 'Itamarati de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2915700,
        nome: 'Itamari',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3132701,
        nome: 'Itambacuri',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4111001,
        nome: 'Itambaracá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4111100,
        nome: 'Itambé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2607653,
        nome: 'Itambé',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2915809,
        nome: 'Itambé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3132800,
        nome: 'Itambé do Mato Dentro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3132909,
        nome: 'Itamogi',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3133006,
        nome: 'Itamonte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2915908,
        nome: 'Itanagra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3522109,
        nome: 'Itanhaém',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3133105,
        nome: 'Itanhandu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5104542,
        nome: 'Itanhangá',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2916005,
        nome: 'Itanhém',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3133204,
        nome: 'Itanhomi',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3133303,
        nome: 'Itaobim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3522158,
        nome: 'Itaóca',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3302106,
        nome: 'Itaocara',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5210901,
        nome: 'Itapaci',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3133402,
        nome: 'Itapagipe',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2306306,
        nome: 'Itapajé',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2916104,
        nome: 'Itaparica',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2916203,
        nome: 'Itapé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2916302,
        nome: 'Itapebi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3133501,
        nome: 'Itapecerica',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3522208,
        nome: 'Itapecerica da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2105401,
        nome: 'Itapecuru Mirim',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4111209,
        nome: 'Itapejara d\'Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4208302,
        nome: 'Itapema',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3202801,
        nome: 'Itapemirim',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4111258,
        nome: 'Itaperuçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3302205,
        nome: 'Itaperuna',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2607703,
        nome: 'Itapetim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2916401,
        nome: 'Itapetinga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3522307,
        nome: 'Itapetininga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3522406,
        nome: 'Itapeva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3133600,
        nome: 'Itapeva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3522505,
        nome: 'Itapevi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2916500,
        nome: 'Itapicuru',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2306405,
        nome: 'Itapipoca',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3522604,
        nome: 'Itapira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1302009,
        nome: 'Itapiranga',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4208401,
        nome: 'Itapiranga',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5211008,
        nome: 'Itapirapuã',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3522653,
        nome: 'Itapirapuã Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1710904,
        nome: 'Itapiratins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2607752,
        nome: 'Itapissuma',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2916609,
        nome: 'Itapitanga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2306504,
        nome: 'Itapiúna',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4208450,
        nome: 'Itapoá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3522703,
        nome: 'Itápolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5004502,
        nome: 'Itaporã',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1711100,
        nome: 'Itaporã do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3522802,
        nome: 'Itaporanga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2507002,
        nome: 'Itaporanga',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2803203,
        nome: 'Itaporanga d\'Ajuda',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2507101,
        nome: 'Itapororoca',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1101104,
        nome: 'Itapuã do Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4310579,
        nome: 'Itapuca',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3522901,
        nome: 'Itapuí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3523008,
        nome: 'Itapura',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5211206,
        nome: 'Itapuranga',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3523107,
        nome: 'Itaquaquecetuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2916708,
        nome: 'Itaquara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4310603,
        nome: 'Itaqui',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5004601,
        nome: 'Itaquiraí',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2607802,
        nome: 'Itaquitinga',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3202900,
        nome: 'Itarana',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2916807,
        nome: 'Itarantim',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3523206,
        nome: 'Itararé',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2306553,
        nome: 'Itarema',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3523305,
        nome: 'Itariri',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5211305,
        nome: 'Itarumã',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4310652,
        nome: 'Itati',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3302254,
        nome: 'Itatiaia',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3133709,
        nome: 'Itatiaiuçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3523404,
        nome: 'Itatiba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4310702,
        nome: 'Itatiba do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2916856,
        nome: 'Itatim',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3523503,
        nome: 'Itatinga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2306603,
        nome: 'Itatira',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2507200,
        nome: 'Itatuba',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2404903,
        nome: 'Itaú',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3133758,
        nome: 'Itaú de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5104559,
        nome: 'Itaúba',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1600253,
        nome: 'Itaubal',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 5211404,
        nome: 'Itauçu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2205102,
        nome: 'Itaueira',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3133808,
        nome: 'Itaúna',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4111308,
        nome: 'Itaúna do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3133907,
        nome: 'Itaverava',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3134004,
        nome: 'Itinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2105427,
        nome: 'Itinga do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5104609,
        nome: 'Itiquira',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3523602,
        nome: 'Itirapina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3523701,
        nome: 'Itirapuã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2916906,
        nome: 'Itiruçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2917003,
        nome: 'Itiúba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3523800,
        nome: 'Itobi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2917102,
        nome: 'Itororó',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3523909,
        nome: 'Itu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2917201,
        nome: 'Ituaçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2917300,
        nome: 'Ituberá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3134103,
        nome: 'Itueta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3134202,
        nome: 'Ituiutaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5211503,
        nome: 'Itumbiara',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3134301,
        nome: 'Itumirim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3524006,
        nome: 'Itupeva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1503705,
        nome: 'Itupiranga',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4208500,
        nome: 'Ituporanga',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3134400,
        nome: 'Iturama',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3134509,
        nome: 'Itutinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3524105,
        nome: 'Ituverava',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2917334,
        nome: 'Iuiú',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3203007,
        nome: 'Iúna',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4111407,
        nome: 'Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4111506,
        nome: 'Ivaiporã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4111555,
        nome: 'Ivaté',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4111605,
        nome: 'Ivatuba',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5004700,
        nome: 'Ivinhema',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5211602,
        nome: 'Ivolândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4310751,
        nome: 'Ivorá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4310801,
        nome: 'Ivoti',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2607901,
        nome: 'Jaboatão dos Guararapes',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4208609,
        nome: 'Jaborá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2917359,
        nome: 'Jaborandi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3524204,
        nome: 'Jaborandi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4111704,
        nome: 'Jaboti',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4310850,
        nome: 'Jaboticaba',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3524303,
        nome: 'Jaboticabal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3134608,
        nome: 'Jaboticatubas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2405009,
        nome: 'Jaçanã',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2917409,
        nome: 'Jacaraci',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2507309,
        nome: 'Jacaraú',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2703403,
        nome: 'Jacaré dos Homens',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1503754,
        nome: 'Jacareacanga',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3524402,
        nome: 'Jacareí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4111803,
        nome: 'Jacarezinho',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3524501,
        nome: 'Jaci',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5104807,
        nome: 'Jaciara',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3134707,
        nome: 'Jacinto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4208708,
        nome: 'Jacinto Machado',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2917508,
        nome: 'Jacobina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2205151,
        nome: 'Jacobina do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3134806,
        nome: 'Jacuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2703502,
        nome: 'Jacuípe',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4310876,
        nome: 'Jacuizinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1503804,
        nome: 'Jacundá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3524600,
        nome: 'Jacupiranga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4310900,
        nome: 'Jacutinga',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3134905,
        nome: 'Jacutinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4111902,
        nome: 'Jaguapitã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2917607,
        nome: 'Jaguaquara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3135001,
        nome: 'Jaguaraçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4311007,
        nome: 'Jaguarão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2917706,
        nome: 'Jaguarari',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3203056,
        nome: 'Jaguaré',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2306702,
        nome: 'Jaguaretama',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4311106,
        nome: 'Jaguari',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4112009,
        nome: 'Jaguariaíva',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2306801,
        nome: 'Jaguaribara',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2306900,
        nome: 'Jaguaribe',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2917805,
        nome: 'Jaguaripe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3524709,
        nome: 'Jaguariúna',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2307007,
        nome: 'Jaguaruana',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4208807,
        nome: 'Jaguaruna',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3135050,
        nome: 'Jaíba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2205201,
        nome: 'Jaicós',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3524808,
        nome: 'Jales',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3524907,
        nome: 'Jambeiro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3135076,
        nome: 'Jampruca',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3135100,
        nome: 'Janaúba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5211701,
        nome: 'Jandaia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4112108,
        nome: 'Jandaia do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2405108,
        nome: 'Jandaíra',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2917904,
        nome: 'Jandaíra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3525003,
        nome: 'Jandira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2405207,
        nome: 'Janduís',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5104906,
        nome: 'Jangada',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4112207,
        nome: 'Janiópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3135209,
        nome: 'Januária',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2405306,
        nome: 'Januário Cicco (Boa Saúde)',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3135308,
        nome: 'Japaraíba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2703601,
        nome: 'Japaratinga',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2803302,
        nome: 'Japaratuba',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3302270,
        nome: 'Japeri',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2405405,
        nome: 'Japi',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4112306,
        nome: 'Japira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2803401,
        nome: 'Japoatã',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3135357,
        nome: 'Japonvar',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5004809,
        nome: 'Japorã',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4112405,
        nome: 'Japurá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1302108,
        nome: 'Japurá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2607950,
        nome: 'Jaqueira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4311122,
        nome: 'Jaquirana',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5211800,
        nome: 'Jaraguá',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4208906,
        nome: 'Jaraguá do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5004908,
        nome: 'Jaraguari',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2703700,
        nome: 'Jaramataia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2307106,
        nome: 'Jardim',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5005004,
        nome: 'Jardim',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4112504,
        nome: 'Jardim Alegre',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2405504,
        nome: 'Jardim de Angicos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2405603,
        nome: 'Jardim de Piranhas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2205250,
        nome: 'Jardim do Mulato',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2405702,
        nome: 'Jardim do Seridó',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4112603,
        nome: 'Jardim Olinda',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3525102,
        nome: 'Jardinópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4208955,
        nome: 'Jardinópolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4311130,
        nome: 'Jari',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3525201,
        nome: 'Jarinu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1100114,
        nome: 'Jaru',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5211909,
        nome: 'Jataí',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4112702,
        nome: 'Jataizinho',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2608008,
        nome: 'Jataúba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5005103,
        nome: 'Jateí',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2307205,
        nome: 'Jati',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2105450,
        nome: 'Jatobá',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2608057,
        nome: 'Jatobá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2205276,
        nome: 'Jatobá do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3525300,
        nome: 'Jaú',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1711506,
        nome: 'Jaú do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5212006,
        nome: 'Jaupaci',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5105002,
        nome: 'Jauru',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3135407,
        nome: 'Jeceaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3135456,
        nome: 'Jenipapo de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2105476,
        nome: 'Jenipapo dos Vieiras',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3135506,
        nome: 'Jequeri',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2703759,
        nome: 'Jequiá da Praia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2918001,
        nome: 'Jequié',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3135605,
        nome: 'Jequitaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3135704,
        nome: 'Jequitibá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3135803,
        nome: 'Jequitinhonha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2918100,
        nome: 'Jeremoabo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2507408,
        nome: 'Jericó',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3525409,
        nome: 'Jeriquara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3203106,
        nome: 'Jerônimo Monteiro',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2205300,
        nome: 'Jerumenha',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3135902,
        nome: 'Jesuânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4112751,
        nome: 'Jesuítas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5212055,
        nome: 'Jesúpolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1100122,
        nome: 'Ji-Paraná',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2307254,
        nome: 'Jijoca de Jericoacoara',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2918209,
        nome: 'Jiquiriçá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2918308,
        nome: 'Jitaúna',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4209003,
        nome: 'Joaçaba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3136009,
        nome: 'Joaíma',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3136108,
        nome: 'Joanésia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3525508,
        nome: 'Joanópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2608107,
        nome: 'João Alfredo',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2405801,
        nome: 'João Câmara',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2205359,
        nome: 'João Costa',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2405900,
        nome: 'João Dias',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2918357,
        nome: 'João Dourado',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2105500,
        nome: 'João Lisboa',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3136207,
        nome: 'João Monlevade',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3203130,
        nome: 'João Neiva',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2507507,
        nome: 'João Pessoa',
        capital: 's',
        sigla_uf: 'PB'
    },
    {
        id: 3136306,
        nome: 'João Pinheiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3525607,
        nome: 'João Ramalho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3136405,
        nome: 'Joaquim Felício',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2703809,
        nome: 'Joaquim Gomes',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2608206,
        nome: 'Joaquim Nabuco',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2205409,
        nome: 'Joaquim Pires',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4112801,
        nome: 'Joaquim Távora',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2513653,
        nome: 'Joca Claudino',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2205458,
        nome: 'Joca Marques',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4311155,
        nome: 'Jóia',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4209102,
        nome: 'Joinville',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3136504,
        nome: 'Jordânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1200328,
        nome: 'Jordão',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4209151,
        nome: 'José Boiteux',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3525706,
        nome: 'José Bonifácio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2406007,
        nome: 'José da Penha',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2205508,
        nome: 'José de Freitas',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3136520,
        nome: 'José Gonçalves de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3136553,
        nome: 'José Raydan',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2105609,
        nome: 'Joselândia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3136579,
        nome: 'Josenópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5212105,
        nome: 'Joviânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5105101,
        nome: 'Juara',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2507606,
        nome: 'Juarez Távora',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1711803,
        nome: 'Juarina',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3136652,
        nome: 'Juatuba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2507705,
        nome: 'Juazeirinho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2918407,
        nome: 'Juazeiro',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2307304,
        nome: 'Juazeiro do Norte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2205516,
        nome: 'Juazeiro do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2307403,
        nome: 'Jucás',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2608255,
        nome: 'Jucati',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2918456,
        nome: 'Jucuruçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2406106,
        nome: 'Jucurutu',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5105150,
        nome: 'Juína',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3136702,
        nome: 'Juiz de Fora',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2205524,
        nome: 'Júlio Borges',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4311205,
        nome: 'Júlio de Castilhos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3525805,
        nome: 'Júlio Mesquita',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3525854,
        nome: 'Jumirim',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2105658,
        nome: 'Junco do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2507804,
        nome: 'Junco do Seridó',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2406155,
        nome: 'Jundiá',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2703908,
        nome: 'Jundiá',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3525904,
        nome: 'Jundiaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4112900,
        nome: 'Jundiaí do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2704005,
        nome: 'Junqueiro',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3526001,
        nome: 'Junqueirópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2608305,
        nome: 'Jupi',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4209177,
        nome: 'Jupiá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3526100,
        nome: 'Juquiá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3526209,
        nome: 'Juquitiba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3136801,
        nome: 'Juramento',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4112959,
        nome: 'Juranda',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2608404,
        nome: 'Jurema',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2205532,
        nome: 'Jurema',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2507903,
        nome: 'Juripiranga',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2508000,
        nome: 'Juru',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1302207,
        nome: 'Juruá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3136900,
        nome: 'Juruaia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5105176,
        nome: 'Juruena',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1503903,
        nome: 'Juruti',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5105200,
        nome: 'Juscimeira',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2918506,
        nome: 'Jussara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5212204,
        nome: 'Jussara',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4113007,
        nome: 'Jussara',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2918555,
        nome: 'Jussari',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2918605,
        nome: 'Jussiape',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1302306,
        nome: 'Jutaí',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 5005152,
        nome: 'Juti',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3136959,
        nome: 'Juvenília',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4113106,
        nome: 'Kaloré',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1302405,
        nome: 'Lábrea',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4209201,
        nome: 'Lacerdópolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3137007,
        nome: 'Ladainha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5005202,
        nome: 'Ladário',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2918704,
        nome: 'Lafaiete Coutinho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3137106,
        nome: 'Lagamar',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2803500,
        nome: 'Lagarto',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4209300,
        nome: 'Lages',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2105708,
        nome: 'Lago da Pedra',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2105807,
        nome: 'Lago do Junco',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2105948,
        nome: 'Lago dos Rodrigues',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2105906,
        nome: 'Lago Verde',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2508109,
        nome: 'Lagoa',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2205557,
        nome: 'Lagoa Alegre',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4311239,
        nome: 'Lagoa Bonita do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2406205,
        nome: 'Lagoa d\'Anta',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2704104,
        nome: 'Lagoa da Canoa',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1711902,
        nome: 'Lagoa da Confusão',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3137205,
        nome: 'Lagoa da Prata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2508208,
        nome: 'Lagoa de Dentro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2608503,
        nome: 'Lagoa de Itaenga',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2406304,
        nome: 'Lagoa de Pedras',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2205573,
        nome: 'Lagoa de São Francisco',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2406403,
        nome: 'Lagoa de Velhos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2205565,
        nome: 'Lagoa do Barro do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2608453,
        nome: 'Lagoa do Carro',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2105922,
        nome: 'Lagoa do Mato',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2608602,
        nome: 'Lagoa do Ouro',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2205581,
        nome: 'Lagoa do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2205599,
        nome: 'Lagoa do Sítio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1711951,
        nome: 'Lagoa do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2608701,
        nome: 'Lagoa dos Gatos',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3137304,
        nome: 'Lagoa dos Patos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4311270,
        nome: 'Lagoa dos Três Cantos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3137403,
        nome: 'Lagoa Dourada',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3137502,
        nome: 'Lagoa Formosa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3137536,
        nome: 'Lagoa Grande',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2608750,
        nome: 'Lagoa Grande',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2105963,
        nome: 'Lagoa Grande do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2406502,
        nome: 'Lagoa Nova',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2918753,
        nome: 'Lagoa Real',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2406601,
        nome: 'Lagoa Salgada',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5212253,
        nome: 'Lagoa Santa',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3137601,
        nome: 'Lagoa Santa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2508307,
        nome: 'Lagoa Seca',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4311304,
        nome: 'Lagoa Vermelha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4311254,
        nome: 'Lagoão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3526308,
        nome: 'Lagoinha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2205540,
        nome: 'Lagoinha do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4209409,
        nome: 'Laguna',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5005251,
        nome: 'Laguna Carapã',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2918803,
        nome: 'Laje',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3302304,
        nome: 'Laje do Muriaé',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 1712009,
        nome: 'Lajeado',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4311403,
        nome: 'Lajeado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4311429,
        nome: 'Lajeado do Bugre',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4209458,
        nome: 'Lajeado Grande',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2105989,
        nome: 'Lajeado Novo',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2918902,
        nome: 'Lajedão',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2919009,
        nome: 'Lajedinho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2608800,
        nome: 'Lajedo',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2919058,
        nome: 'Lajedo do Tabocal',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2406700,
        nome: 'Lajes',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2406809,
        nome: 'Lajes Pintadas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3137700,
        nome: 'Lajinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2919108,
        nome: 'Lamarão',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3137809,
        nome: 'Lambari',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5105234,
        nome: 'Lambari D\'Oeste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3137908,
        nome: 'Lamim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2205607,
        nome: 'Landri Sales',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4113205,
        nome: 'Lapa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2919157,
        nome: 'Lapão',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3203163,
        nome: 'Laranja da Terra',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3138005,
        nome: 'Laranjal',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4113254,
        nome: 'Laranjal',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1600279,
        nome: 'Laranjal do Jari',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 3526407,
        nome: 'Laranjal Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2803609,
        nome: 'Laranjeiras',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4113304,
        nome: 'Laranjeiras do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3138104,
        nome: 'Lassance',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2508406,
        nome: 'Lastro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4209508,
        nome: 'Laurentino',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2919207,
        nome: 'Lauro de Freitas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4209607,
        nome: 'Lauro Muller',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1712157,
        nome: 'Lavandeira',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3526506,
        nome: 'Lavínia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3138203,
        nome: 'Lavras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2307502,
        nome: 'Lavras da Mangabeira',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4311502,
        nome: 'Lavras do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3526605,
        nome: 'Lavrinhas',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3138302,
        nome: 'Leandro Ferreira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4209706,
        nome: 'Lebon Régis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3526704,
        nome: 'Leme',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3138351,
        nome: 'Leme do Prado',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2919306,
        nome: 'Lençóis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3526803,
        nome: 'Lençóis Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4209805,
        nome: 'Leoberto Leal',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3138401,
        nome: 'Leopoldina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5212303,
        nome: 'Leopoldo de Bulhões',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4113403,
        nome: 'Leópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4311601,
        nome: 'Liberato Salzano',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3138500,
        nome: 'Liberdade',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2919405,
        nome: 'Licínio de Almeida',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4113429,
        nome: 'Lidianópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2106003,
        nome: 'Lima Campos',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3138609,
        nome: 'Lima Duarte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3526902,
        nome: 'Limeira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3138625,
        nome: 'Limeira do Oeste',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2608909,
        nome: 'Limoeiro',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2704203,
        nome: 'Limoeiro de Anadia',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1504000,
        nome: 'Limoeiro do Ajuru',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2307601,
        nome: 'Limoeiro do Norte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4113452,
        nome: 'Lindoeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3527009,
        nome: 'Lindóia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4209854,
        nome: 'Lindóia do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4311627,
        nome: 'Lindolfo Collor',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4311643,
        nome: 'Linha Nova',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3203205,
        nome: 'Linhares',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3527108,
        nome: 'Lins',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2508505,
        nome: 'Livramento',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2919504,
        nome: 'Livramento de Nossa Senhora',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1712405,
        nome: 'Lizarda',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4113502,
        nome: 'Loanda',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4113601,
        nome: 'Lobato',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2508554,
        nome: 'Logradouro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4113700,
        nome: 'Londrina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3138658,
        nome: 'Lontra',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4209904,
        nome: 'Lontras',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3527207,
        nome: 'Lorena',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2106102,
        nome: 'Loreto',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3527256,
        nome: 'Lourdes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3527306,
        nome: 'Louveira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5105259,
        nome: 'Lucas do Rio Verde',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3527405,
        nome: 'Lucélia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2508604,
        nome: 'Lucena',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3527504,
        nome: 'Lucianópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5105309,
        nome: 'Luciara',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2406908,
        nome: 'Lucrécia',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3527603,
        nome: 'Luís Antônio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2205706,
        nome: 'Luís Correia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2106201,
        nome: 'Luís Domingues',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2919553,
        nome: 'Luís Eduardo Magalhães',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2407005,
        nome: 'Luís Gomes',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3138674,
        nome: 'Luisburgo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3138682,
        nome: 'Luislândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4210001,
        nome: 'Luiz Alves',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4113734,
        nome: 'Luiziana',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3527702,
        nome: 'Luiziânia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3138708,
        nome: 'Luminárias',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4113759,
        nome: 'Lunardelli',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3527801,
        nome: 'Lupércio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4113809,
        nome: 'Lupionópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3527900,
        nome: 'Lutécia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3138807,
        nome: 'Luz',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4210035,
        nome: 'Luzerna',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5212501,
        nome: 'Luziânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2205805,
        nome: 'Luzilândia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1712454,
        nome: 'Luzinópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3302403,
        nome: 'Macaé',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2407104,
        nome: 'Macaíba',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2919603,
        nome: 'Macajuba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4311718,
        nome: 'Maçambará',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2803708,
        nome: 'Macambira',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 1600303,
        nome: 'Macapá',
        capital: 's',
        sigla_uf: 'AP'
    },
    {
        id: 2609006,
        nome: 'Macaparana',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2919702,
        nome: 'Macarani',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3528007,
        nome: 'Macatuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2407203,
        nome: 'Macau',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3528106,
        nome: 'Macaubal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2919801,
        nome: 'Macaúbas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3528205,
        nome: 'Macedônia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2704302,
        nome: 'Maceió',
        capital: 's',
        sigla_uf: 'AL'
    },
    {
        id: 3138906,
        nome: 'Machacalis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4311700,
        nome: 'Machadinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100130,
        nome: 'Machadinho D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3139003,
        nome: 'Machado',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2609105,
        nome: 'Machados',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4210050,
        nome: 'Macieira',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3302452,
        nome: 'Macuco',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2919900,
        nome: 'Macururé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2307635,
        nome: 'Madalena',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2205854,
        nome: 'Madeiro',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2919926,
        nome: 'Madre de Deus',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3139102,
        nome: 'Madre de Deus de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2508703,
        nome: 'Mãe d\'Água',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1504059,
        nome: 'Mãe do Rio',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2919959,
        nome: 'Maetinga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4210100,
        nome: 'Mafra',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1504109,
        nome: 'Magalhães Barata',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2106300,
        nome: 'Magalhães de Almeida',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3528304,
        nome: 'Magda',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3302502,
        nome: 'Magé',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2920007,
        nome: 'Maiquinique',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2920106,
        nome: 'Mairi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3528403,
        nome: 'Mairinque',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3528502,
        nome: 'Mairiporã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5212600,
        nome: 'Mairipotaba',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4210209,
        nome: 'Major Gercino',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2704401,
        nome: 'Major Isidoro',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2407252,
        nome: 'Major Sales',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4210308,
        nome: 'Major Vieira',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3139201,
        nome: 'Malacacheta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2920205,
        nome: 'Malhada',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2920304,
        nome: 'Malhada de Pedras',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2803807,
        nome: 'Malhada dos Bois',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2803906,
        nome: 'Malhador',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4113908,
        nome: 'Mallet',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2508802,
        nome: 'Malta',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2508901,
        nome: 'Mamanguape',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5212709,
        nome: 'Mambaí',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4114005,
        nome: 'Mamborê',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3139250,
        nome: 'Mamonas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4311734,
        nome: 'Mampituba',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1302504,
        nome: 'Manacapuru',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2509008,
        nome: 'Manaíra',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1302553,
        nome: 'Manaquiri',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2609154,
        nome: 'Manari',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1302603,
        nome: 'Manaus',
        capital: 's',
        sigla_uf: 'AM'
    },
    {
        id: 1200336,
        nome: 'Mâncio Lima',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4114104,
        nome: 'Mandaguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4114203,
        nome: 'Mandaguari',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4114302,
        nome: 'Mandirituba',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3528601,
        nome: 'Manduri',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4114351,
        nome: 'Manfrinópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3139300,
        nome: 'Manga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3302601,
        nome: 'Mangaratiba',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4114401,
        nome: 'Mangueirinha',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3139409,
        nome: 'Manhuaçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3139508,
        nome: 'Manhumirim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1302702,
        nome: 'Manicoré',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2205904,
        nome: 'Manoel Emídio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4114500,
        nome: 'Manoel Ribas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1200344,
        nome: 'Manoel Urbano',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4311759,
        nome: 'Manoel Viana',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2920403,
        nome: 'Manoel Vitorino',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2920452,
        nome: 'Mansidão',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3139607,
        nome: 'Mantena',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3203304,
        nome: 'Mantenópolis',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4311775,
        nome: 'Maquiné',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3139805,
        nome: 'Mar de Espanha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2704906,
        nome: 'Mar Vermelho',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5212808,
        nome: 'Mara Rosa',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1302801,
        nome: 'Maraã',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1504208,
        nome: 'Marabá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3528700,
        nome: 'Marabá Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2106326,
        nome: 'Maracaçumé',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3528809,
        nome: 'Maracaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4210407,
        nome: 'Maracajá',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5005400,
        nome: 'Maracaju',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1504307,
        nome: 'Maracanã',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2307650,
        nome: 'Maracanaú',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2920502,
        nome: 'Maracás',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2704500,
        nome: 'Maragogi',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2920601,
        nome: 'Maragogipe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2609204,
        nome: 'Maraial',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2106359,
        nome: 'Marajá do Sena',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2307700,
        nome: 'Maranguape',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2106375,
        nome: 'Maranhãozinho',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1504406,
        nome: 'Marapanim',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3528858,
        nome: 'Marapoama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4311791,
        nome: 'Maratá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3203320,
        nome: 'Marataízes',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4311809,
        nome: 'Marau',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2920700,
        nome: 'Maraú',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2704609,
        nome: 'Maravilha',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4210506,
        nome: 'Maravilha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3139706,
        nome: 'Maravilhas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2509057,
        nome: 'Marcação',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5105580,
        nome: 'Marcelândia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4311908,
        nome: 'Marcelino Ramos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2407302,
        nome: 'Marcelino Vieira',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2920809,
        nome: 'Marcionílio Souza',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2307809,
        nome: 'Marco',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2205953,
        nome: 'Marcolândia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2206001,
        nome: 'Marcos Parente',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4114609,
        nome: 'Marechal Cândido Rondon',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2704708,
        nome: 'Marechal Deodoro',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3203346,
        nome: 'Marechal Floriano',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 1200351,
        nome: 'Marechal Thaumaturgo',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4210555,
        nome: 'Marema',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2509107,
        nome: 'Mari',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3139904,
        nome: 'Maria da Fé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4114708,
        nome: 'Maria Helena',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4114807,
        nome: 'Marialva',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3140001,
        nome: 'Mariana',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4311981,
        nome: 'Mariana Pimentel',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4312005,
        nome: 'Mariano Moro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1712504,
        nome: 'Marianópolis do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3528908,
        nome: 'Mariápolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2704807,
        nome: 'Maribondo',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3302700,
        nome: 'Maricá',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3140100,
        nome: 'Marilac',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3203353,
        nome: 'Marilândia',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4114906,
        nome: 'Marilândia do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4115002,
        nome: 'Marilena',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3529005,
        nome: 'Marília',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4115101,
        nome: 'Mariluz',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4115200,
        nome: 'Maringá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3529104,
        nome: 'Marinópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3140159,
        nome: 'Mário Campos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4115309,
        nome: 'Mariópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4115358,
        nome: 'Maripá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3140209,
        nome: 'Maripá de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1504422,
        nome: 'Marituba',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2509156,
        nome: 'Marizópolis',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3140308,
        nome: 'Marliéria',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4115408,
        nome: 'Marmeleiro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3140407,
        nome: 'Marmelópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4312054,
        nome: 'Marques de Souza',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4115457,
        nome: 'Marquinho',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3140506,
        nome: 'Martinho Campos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2307908,
        nome: 'Martinópole',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3529203,
        nome: 'Martinópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2407401,
        nome: 'Martins',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3140530,
        nome: 'Martins Soares',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2804003,
        nome: 'Maruim',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4115507,
        nome: 'Marumbi',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5212907,
        nome: 'Marzagão',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2920908,
        nome: 'Mascote',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2308005,
        nome: 'Massapê',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2206050,
        nome: 'Massapê do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2509206,
        nome: 'Massaranduba',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4210605,
        nome: 'Massaranduba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4312104,
        nome: 'Mata',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2921005,
        nome: 'Mata de São João',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2705002,
        nome: 'Mata Grande',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2106409,
        nome: 'Mata Roma',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3140555,
        nome: 'Mata Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3529302,
        nome: 'Matão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2509305,
        nome: 'Mataraca',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1712702,
        nome: 'Mateiros',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4115606,
        nome: 'Matelândia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3140605,
        nome: 'Materlândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3140704,
        nome: 'Mateus Leme',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3171501,
        nome: 'Mathias Lobato',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3140803,
        nome: 'Matias Barbosa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3140852,
        nome: 'Matias Cardoso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2206100,
        nome: 'Matias Olímpio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2921054,
        nome: 'Matina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2106508,
        nome: 'Matinha',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2509339,
        nome: 'Matinhas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4115705,
        nome: 'Matinhos',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3140902,
        nome: 'Matipó',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4312138,
        nome: 'Mato Castelhano',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2509370,
        nome: 'Mato Grosso',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4312153,
        nome: 'Mato Leitão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4312179,
        nome: 'Mato Queimado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4115739,
        nome: 'Mato Rico',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3141009,
        nome: 'Mato Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2106607,
        nome: 'Matões',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2106631,
        nome: 'Matões do Norte',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4210704,
        nome: 'Matos Costa',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3141108,
        nome: 'Matozinhos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5212956,
        nome: 'Matrinchã',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2705101,
        nome: 'Matriz de Camaragibe',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5105606,
        nome: 'Matupá',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2509396,
        nome: 'Maturéia',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3141207,
        nome: 'Matutina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3529401,
        nome: 'Mauá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4115754,
        nome: 'Mauá da Serra',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1302900,
        nome: 'Maués',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 5213004,
        nome: 'Maurilândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1712801,
        nome: 'Maurilândia do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2308104,
        nome: 'Mauriti',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2407500,
        nome: 'Maxaranguape',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4312203,
        nome: 'Maximiliano de Almeida',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1600402,
        nome: 'Mazagão',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 3141306,
        nome: 'Medeiros',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2921104,
        nome: 'Medeiros Neto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4115804,
        nome: 'Medianeira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1504455,
        nome: 'Medicilândia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3141405,
        nome: 'Medina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4210803,
        nome: 'Meleiro',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1504505,
        nome: 'Melgaço',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3302809,
        nome: 'Mendes',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3141504,
        nome: 'Mendes Pimentel',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3529500,
        nome: 'Mendonça',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4115853,
        nome: 'Mercedes',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3141603,
        nome: 'Mercês',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3529609,
        nome: 'Meridiano',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2308203,
        nome: 'Meruoca',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3529658,
        nome: 'Mesópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3302858,
        nome: 'Mesquita',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3141702,
        nome: 'Mesquita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2705200,
        nome: 'Messias',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2407609,
        nome: 'Messias Targino',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2206209,
        nome: 'Miguel Alves',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2921203,
        nome: 'Miguel Calmon',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2206308,
        nome: 'Miguel Leão',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3302908,
        nome: 'Miguel Pereira',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3529708,
        nome: 'Miguelópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2308302,
        nome: 'Milagres',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2921302,
        nome: 'Milagres',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2106672,
        nome: 'Milagres do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2308351,
        nome: 'Milhã',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2206357,
        nome: 'Milton Brandão',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5213053,
        nome: 'Mimoso de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3203403,
        nome: 'Mimoso do Sul',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5213087,
        nome: 'Minaçu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2705309,
        nome: 'Minador do Negrão',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4312252,
        nome: 'Minas do Leão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3141801,
        nome: 'Minas Novas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3141900,
        nome: 'Minduri',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5213103,
        nome: 'Mineiros',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3529807,
        nome: 'Mineiros do Tietê',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1101203,
        nome: 'Ministro Andreazza',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3530003,
        nome: 'Mira Estrela',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3142007,
        nome: 'Mirabela',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3529906,
        nome: 'Miracatu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3303005,
        nome: 'Miracema',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 1713205,
        nome: 'Miracema do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2106706,
        nome: 'Mirador',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4115903,
        nome: 'Mirador',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3142106,
        nome: 'Miradouro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4312302,
        nome: 'Miraguaí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3142205,
        nome: 'Miraí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2308377,
        nome: 'Miraíma',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5005608,
        nome: 'Miranda',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2106755,
        nome: 'Miranda do Norte',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2609303,
        nome: 'Mirandiba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3530102,
        nome: 'Mirandópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2921401,
        nome: 'Mirangaba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1713304,
        nome: 'Miranorte',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2921450,
        nome: 'Mirante',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1101302,
        nome: 'Mirante da Serra',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3530201,
        nome: 'Mirante do Paranapanema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4116000,
        nome: 'Miraselva',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3530300,
        nome: 'Mirassol',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5105622,
        nome: 'Mirassol d\'Oeste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3530409,
        nome: 'Mirassolândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3142254,
        nome: 'Miravânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4210852,
        nome: 'Mirim Doce',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2106805,
        nome: 'Mirinzal',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4116059,
        nome: 'Missal',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2308401,
        nome: 'Missão Velha',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1504604,
        nome: 'Mocajuba',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3530508,
        nome: 'Mococa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4210902,
        nome: 'Modelo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3142304,
        nome: 'Moeda',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3142403,
        nome: 'Moema',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2509404,
        nome: 'Mogeiro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3530607,
        nome: 'Mogi das Cruzes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3530706,
        nome: 'Mogi Guaçu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3530805,
        nome: 'Mogi Mirim',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5213400,
        nome: 'Moiporá',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2804102,
        nome: 'Moita Bonita',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 1504703,
        nome: 'Moju',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1504752,
        nome: 'Mojuí dos Campos',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2308500,
        nome: 'Mombaça',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3530904,
        nome: 'Mombuca',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2106904,
        nome: 'Monção',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3531001,
        nome: 'Monções',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4211009,
        nome: 'Mondaí',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3531100,
        nome: 'Mongaguá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3142502,
        nome: 'Monjolos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2206407,
        nome: 'Monsenhor Gil',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2206506,
        nome: 'Monsenhor Hipólito',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3142601,
        nome: 'Monsenhor Paulo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2308609,
        nome: 'Monsenhor Tabosa',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2509503,
        nome: 'Montadas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3142700,
        nome: 'Montalvânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3203502,
        nome: 'Montanha',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2407708,
        nome: 'Montanhas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4312351,
        nome: 'Montauri',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1504802,
        nome: 'Monte Alegre',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2407807,
        nome: 'Monte Alegre',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5213509,
        nome: 'Monte Alegre de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3142809,
        nome: 'Monte Alegre de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2804201,
        nome: 'Monte Alegre de Sergipe',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2206605,
        nome: 'Monte Alegre do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3531209,
        nome: 'Monte Alegre do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4312377,
        nome: 'Monte Alegre dos Campos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3531308,
        nome: 'Monte Alto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3531407,
        nome: 'Monte Aprazível',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3142908,
        nome: 'Monte Azul',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3531506,
        nome: 'Monte Azul Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3143005,
        nome: 'Monte Belo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4312385,
        nome: 'Monte Belo do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4211058,
        nome: 'Monte Carlo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3143104,
        nome: 'Monte Carmelo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4211108,
        nome: 'Monte Castelo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3531605,
        nome: 'Monte Castelo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2407906,
        nome: 'Monte das Gameleiras',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1713601,
        nome: 'Monte do Carmo',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3143153,
        nome: 'Monte Formoso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2509602,
        nome: 'Monte Horebe',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3531803,
        nome: 'Monte Mor',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1101401,
        nome: 'Monte Negro',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2921500,
        nome: 'Monte Santo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3143203,
        nome: 'Monte Santo de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1713700,
        nome: 'Monte Santo do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3143401,
        nome: 'Monte Sião',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2509701,
        nome: 'Monteiro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3531704,
        nome: 'Monteiro Lobato',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2705408,
        nome: 'Monteirópolis',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4312401,
        nome: 'Montenegro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2107001,
        nome: 'Montes Altos',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3143302,
        nome: 'Montes Claros',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5213707,
        nome: 'Montes Claros de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3143450,
        nome: 'Montezuma',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5213756,
        nome: 'Montividiu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5213772,
        nome: 'Montividiu do Norte',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2308708,
        nome: 'Morada Nova',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3143500,
        nome: 'Morada Nova de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2308807,
        nome: 'Moraújo',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2614303,
        nome: 'Moreilândia',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4116109,
        nome: 'Moreira Sales',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2609402,
        nome: 'Moreno',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4312427,
        nome: 'Mormaço',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2921609,
        nome: 'Morpará',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4116208,
        nome: 'Morretes',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5213806,
        nome: 'Morrinhos',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2308906,
        nome: 'Morrinhos',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4312443,
        nome: 'Morrinhos do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3531902,
        nome: 'Morro Agudo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5213855,
        nome: 'Morro Agudo de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2206654,
        nome: 'Morro Cabeça no Tempo',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4211207,
        nome: 'Morro da Fumaça',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3143609,
        nome: 'Morro da Garça',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2921708,
        nome: 'Morro do Chapéu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2206670,
        nome: 'Morro do Chapéu do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3143708,
        nome: 'Morro do Pilar',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4211256,
        nome: 'Morro Grande',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4312450,
        nome: 'Morro Redondo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4312476,
        nome: 'Morro Reuter',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2107100,
        nome: 'Morros',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2921807,
        nome: 'Mortugaba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3532009,
        nome: 'Morungaba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5213905,
        nome: 'Mossâmedes',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2408003,
        nome: 'Mossoró',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4312500,
        nome: 'Mostardas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3532058,
        nome: 'Motuca',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5214002,
        nome: 'Mozarlândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1504901,
        nome: 'Muaná',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1400308,
        nome: 'Mucajaí',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 2309003,
        nome: 'Mucambo',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2921906,
        nome: 'Mucugê',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4312609,
        nome: 'Muçum',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2922003,
        nome: 'Mucuri',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3203601,
        nome: 'Mucurici',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4312617,
        nome: 'Muitos Capões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4312625,
        nome: 'Muliterno',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2509800,
        nome: 'Mulungu',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2309102,
        nome: 'Mulungu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2922052,
        nome: 'Mulungu do Morro',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2922102,
        nome: 'Mundo Novo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5005681,
        nome: 'Mundo Novo',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5214051,
        nome: 'Mundo Novo',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3143807,
        nome: 'Munhoz',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4116307,
        nome: 'Munhoz de Melo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2922201,
        nome: 'Muniz Ferreira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3203700,
        nome: 'Muniz Freire',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2922250,
        nome: 'Muquém de São Francisco',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3203809,
        nome: 'Muqui',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3143906,
        nome: 'Muriaé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2804300,
        nome: 'Muribeca',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2705507,
        nome: 'Murici',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2206696,
        nome: 'Murici dos Portelas',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1713957,
        nome: 'Muricilândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2922300,
        nome: 'Muritiba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3532108,
        nome: 'Murutinga do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2922409,
        nome: 'Mutuípe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3144003,
        nome: 'Mutum',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5214101,
        nome: 'Mutunópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3144102,
        nome: 'Muzambinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3144201,
        nome: 'Nacip Raydan',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3532157,
        nome: 'Nantes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3144300,
        nome: 'Nanuque',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4312658,
        nome: 'Não-Me-Toque',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3144359,
        nome: 'Naque',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3532207,
        nome: 'Narandiba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2408102,
        nome: 'Natal',
        capital: 's',
        sigla_uf: 'RN'
    },
    {
        id: 3144375,
        nome: 'Natalândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3144409,
        nome: 'Natércia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1714203,
        nome: 'Natividade',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3303104,
        nome: 'Natividade',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3532306,
        nome: 'Natividade da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2509909,
        nome: 'Natuba',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4211306,
        nome: 'Navegantes',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5005707,
        nome: 'Naviraí',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2922508,
        nome: 'Nazaré',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1714302,
        nome: 'Nazaré',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2609501,
        nome: 'Nazaré da Mata',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2206704,
        nome: 'Nazaré do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3532405,
        nome: 'Nazaré Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3144508,
        nome: 'Nazareno',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2510006,
        nome: 'Nazarezinho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2206720,
        nome: 'Nazária',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5214408,
        nome: 'Nazário',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2804409,
        nome: 'Neópolis',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3144607,
        nome: 'Nepomuceno',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5214507,
        nome: 'Nerópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3532504,
        nome: 'Neves Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1303007,
        nome: 'Nhamundá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3532603,
        nome: 'Nhandeara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4312674,
        nome: 'Nicolau Vergueiro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2922607,
        nome: 'Nilo Peçanha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3303203,
        nome: 'Nilópolis',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2107209,
        nome: 'Nina Rodrigues',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3144656,
        nome: 'Ninheira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5005806,
        nome: 'Nioaque',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3532702,
        nome: 'Nipoã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5214606,
        nome: 'Niquelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2408201,
        nome: 'Nísia Floresta',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3303302,
        nome: 'Niterói',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5105903,
        nome: 'Nobres',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4312708,
        nome: 'Nonoai',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2922656,
        nome: 'Nordestina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1400407,
        nome: 'Normandia',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 5106000,
        nome: 'Nortelândia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2804458,
        nome: 'Nossa Senhora Aparecida',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2804508,
        nome: 'Nossa Senhora da Glória',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2804607,
        nome: 'Nossa Senhora das Dores',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4116406,
        nome: 'Nossa Senhora das Graças',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2804706,
        nome: 'Nossa Senhora de Lourdes',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2206753,
        nome: 'Nossa Senhora de Nazaré',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5106109,
        nome: 'Nossa Senhora do Livramento',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2804805,
        nome: 'Nossa Senhora do Socorro',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2206803,
        nome: 'Nossa Senhora dos Remédios',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3532801,
        nome: 'Nova Aliança',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4116505,
        nome: 'Nova Aliança do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4312757,
        nome: 'Nova Alvorada',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5006002,
        nome: 'Nova Alvorada do Sul',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5214705,
        nome: 'Nova América',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4116604,
        nome: 'Nova América da Colina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5006200,
        nome: 'Nova Andradina',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4312807,
        nome: 'Nova Araçá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4116703,
        nome: 'Nova Aurora',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5214804,
        nome: 'Nova Aurora',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5106158,
        nome: 'Nova Bandeirantes',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4312906,
        nome: 'Nova Bassano',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3144672,
        nome: 'Nova Belém',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4312955,
        nome: 'Nova Boa Vista',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5106208,
        nome: 'Nova Brasilândia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1100148,
        nome: 'Nova Brasilândia D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4313003,
        nome: 'Nova Bréscia',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3532827,
        nome: 'Nova Campina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2922706,
        nome: 'Nova Canaã',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5106216,
        nome: 'Nova Canaã do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3532843,
        nome: 'Nova Canaã Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4313011,
        nome: 'Nova Candelária',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4116802,
        nome: 'Nova Cantu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3532868,
        nome: 'Nova Castilho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2107258,
        nome: 'Nova Colinas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5214838,
        nome: 'Nova Crixás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2408300,
        nome: 'Nova Cruz',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3144706,
        nome: 'Nova Era',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4211405,
        nome: 'Nova Erechim',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4116901,
        nome: 'Nova Esperança',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1504950,
        nome: 'Nova Esperança do Piriá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4116950,
        nome: 'Nova Esperança do Sudoeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4313037,
        nome: 'Nova Esperança do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3532900,
        nome: 'Nova Europa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4117008,
        nome: 'Nova Fátima',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2922730,
        nome: 'Nova Fátima',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2510105,
        nome: 'Nova Floresta',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3303401,
        nome: 'Nova Friburgo',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5214861,
        nome: 'Nova Glória',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3533007,
        nome: 'Nova Granada',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5108808,
        nome: 'Nova Guarita',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3533106,
        nome: 'Nova Guataporanga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4313060,
        nome: 'Nova Hartz',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2922755,
        nome: 'Nova Ibiá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3303500,
        nome: 'Nova Iguaçu',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5214879,
        nome: 'Nova Iguaçu de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3533205,
        nome: 'Nova Independência',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2107308,
        nome: 'Nova Iorque',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1504976,
        nome: 'Nova Ipixuna',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4211454,
        nome: 'Nova Itaberaba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2922805,
        nome: 'Nova Itarana',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5106182,
        nome: 'Nova Lacerda',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4117057,
        nome: 'Nova Laranjeiras',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3144805,
        nome: 'Nova Lima',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4117107,
        nome: 'Nova Londrina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3533304,
        nome: 'Nova Luzitânia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1100338,
        nome: 'Nova Mamoré',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5108857,
        nome: 'Nova Marilândia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5108907,
        nome: 'Nova Maringá',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3144904,
        nome: 'Nova Módica',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5108956,
        nome: 'Nova Monte Verde',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5106224,
        nome: 'Nova Mutum',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5106174,
        nome: 'Nova Nazaré',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3533403,
        nome: 'Nova Odessa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4117206,
        nome: 'Nova Olímpia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5106232,
        nome: 'Nova Olímpia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1714880,
        nome: 'Nova Olinda',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2309201,
        nome: 'Nova Olinda',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2510204,
        nome: 'Nova Olinda',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2107357,
        nome: 'Nova Olinda do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1303106,
        nome: 'Nova Olinda do Norte',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4313086,
        nome: 'Nova Pádua',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4313102,
        nome: 'Nova Palma',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2510303,
        nome: 'Nova Palmeira',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4313201,
        nome: 'Nova Petrópolis',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3145000,
        nome: 'Nova Ponte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3145059,
        nome: 'Nova Porteirinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4313300,
        nome: 'Nova Prata',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4117255,
        nome: 'Nova Prata do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4313334,
        nome: 'Nova Ramada',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2922854,
        nome: 'Nova Redenção',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3145109,
        nome: 'Nova Resende',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5214903,
        nome: 'Nova Roma',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4313359,
        nome: 'Nova Roma do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1715002,
        nome: 'Nova Rosalândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2309300,
        nome: 'Nova Russas',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4117214,
        nome: 'Nova Santa Bárbara',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5106190,
        nome: 'Nova Santa Helena',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4313375,
        nome: 'Nova Santa Rita',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2207959,
        nome: 'Nova Santa Rita',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4117222,
        nome: 'Nova Santa Rosa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3145208,
        nome: 'Nova Serrana',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2922904,
        nome: 'Nova Soure',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4117271,
        nome: 'Nova Tebas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1505007,
        nome: 'Nova Timboteua',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4211504,
        nome: 'Nova Trento',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5106240,
        nome: 'Nova Ubiratã',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3136603,
        nome: 'Nova União',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1101435,
        nome: 'Nova União',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3203908,
        nome: 'Nova Venécia',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4211603,
        nome: 'Nova Veneza',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5215009,
        nome: 'Nova Veneza',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2923001,
        nome: 'Nova Viçosa',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5106257,
        nome: 'Nova Xavantina',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3533254,
        nome: 'Novais',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1715101,
        nome: 'Novo Acordo',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1303205,
        nome: 'Novo Airão',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 1715150,
        nome: 'Novo Alegre',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1303304,
        nome: 'Novo Aripuanã',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4313490,
        nome: 'Novo Barreiro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5215207,
        nome: 'Novo Brasil',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4313391,
        nome: 'Novo Cabrais',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3145307,
        nome: 'Novo Cruzeiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5215231,
        nome: 'Novo Gama',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4313409,
        nome: 'Novo Hamburgo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4211652,
        nome: 'Novo Horizonte',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3533502,
        nome: 'Novo Horizonte',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2923035,
        nome: 'Novo Horizonte',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5106273,
        nome: 'Novo Horizonte do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1100502,
        nome: 'Novo Horizonte do Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5006259,
        nome: 'Novo Horizonte do Sul',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4117297,
        nome: 'Novo Itacolomi',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1715259,
        nome: 'Novo Jardim',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2705606,
        nome: 'Novo Lino',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4313425,
        nome: 'Novo Machado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5106265,
        nome: 'Novo Mundo',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2309409,
        nome: 'Novo Oriente',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3145356,
        nome: 'Novo Oriente de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2206902,
        nome: 'Novo Oriente do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5215256,
        nome: 'Novo Planalto',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1505031,
        nome: 'Novo Progresso',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1505064,
        nome: 'Novo Repartimento',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2206951,
        nome: 'Novo Santo Antônio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5106315,
        nome: 'Novo Santo Antônio',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5106281,
        nome: 'Novo São Joaquim',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4313441,
        nome: 'Novo Tiradentes',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2923050,
        nome: 'Novo Triunfo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4313466,
        nome: 'Novo Xingu',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3145372,
        nome: 'Novorizonte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3533601,
        nome: 'Nuporanga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1505106,
        nome: 'Óbidos',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2309458,
        nome: 'Ocara',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3533700,
        nome: 'Ocauçu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2207009,
        nome: 'Oeiras',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1505205,
        nome: 'Oeiras do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1600501,
        nome: 'Oiapoque',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 3145406,
        nome: 'Olaria',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3533809,
        nome: 'Óleo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2510402,
        nome: 'Olho d\'Água',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2107407,
        nome: 'Olho d\'Água das Cunhãs',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2705705,
        nome: 'Olho d\'Água das Flores',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2705804,
        nome: 'Olho d\'Água do Casado',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2207108,
        nome: 'Olho D\'Água do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2705903,
        nome: 'Olho d\'Água Grande',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2408409,
        nome: 'Olho-d\'Água do Borges',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3145455,
        nome: 'Olhos d\'Água',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3533908,
        nome: 'Olímpia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3145505,
        nome: 'Olímpio Noronha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2609600,
        nome: 'Olinda',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2107456,
        nome: 'Olinda Nova do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2923100,
        nome: 'Olindina',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2510501,
        nome: 'Olivedos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3145604,
        nome: 'Oliveira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1715507,
        nome: 'Oliveira de Fátima',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2923209,
        nome: 'Oliveira dos Brejinhos',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3145703,
        nome: 'Oliveira Fortes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2706000,
        nome: 'Olivença',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3145802,
        nome: 'Onça de Pitangui',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3534005,
        nome: 'Onda Verde',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3145851,
        nome: 'Oratórios',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3534104,
        nome: 'Oriente',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3534203,
        nome: 'Orindiúva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1505304,
        nome: 'Oriximiná',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3145877,
        nome: 'Orizânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5215306,
        nome: 'Orizona',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3534302,
        nome: 'Orlândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4211702,
        nome: 'Orleans',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2609709,
        nome: 'Orobó',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2609808,
        nome: 'Orocó',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2309508,
        nome: 'Orós',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4117305,
        nome: 'Ortigueira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3534401,
        nome: 'Osasco',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3534500,
        nome: 'Oscar Bressane',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4313508,
        nome: 'Osório',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3534609,
        nome: 'Osvaldo Cruz',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4211751,
        nome: 'Otacílio Costa',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1505403,
        nome: 'Ourém',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2923308,
        nome: 'Ouriçangas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2609907,
        nome: 'Ouricuri',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1505437,
        nome: 'Ourilândia do Norte',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3534708,
        nome: 'Ourinhos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4117404,
        nome: 'Ourizona',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4211801,
        nome: 'Ouro',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3145901,
        nome: 'Ouro Branco',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2408508,
        nome: 'Ouro Branco',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2706109,
        nome: 'Ouro Branco',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3146008,
        nome: 'Ouro Fino',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3146107,
        nome: 'Ouro Preto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1100155,
        nome: 'Ouro Preto do Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2510600,
        nome: 'Ouro Velho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4211850,
        nome: 'Ouro Verde',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3534807,
        nome: 'Ouro Verde',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5215405,
        nome: 'Ouro Verde de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3146206,
        nome: 'Ouro Verde de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4117453,
        nome: 'Ouro Verde do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3534757,
        nome: 'Ouroeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2923357,
        nome: 'Ourolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5215504,
        nome: 'Ouvidor',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3534906,
        nome: 'Pacaembu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1505486,
        nome: 'Pacajá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2309607,
        nome: 'Pacajus',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1400456,
        nome: 'Pacaraima',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 2309706,
        nome: 'Pacatuba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2804904,
        nome: 'Pacatuba',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2107506,
        nome: 'Paço do Lumiar',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2309805,
        nome: 'Pacoti',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2309904,
        nome: 'Pacujá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5215603,
        nome: 'Padre Bernardo',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3146255,
        nome: 'Padre Carvalho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2207207,
        nome: 'Padre Marcos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3146305,
        nome: 'Padre Paraíso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2207306,
        nome: 'Paes Landim',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3146552,
        nome: 'Pai Pedro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4211876,
        nome: 'Paial',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4117503,
        nome: 'Paiçandu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4313607,
        nome: 'Paim Filho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3146404,
        nome: 'Paineiras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4211892,
        nome: 'Painel',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3146503,
        nome: 'Pains',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3146602,
        nome: 'Paiva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2207355,
        nome: 'Pajeú do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2706208,
        nome: 'Palestina',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3535002,
        nome: 'Palestina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5215652,
        nome: 'Palestina de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1505494,
        nome: 'Palestina do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2310001,
        nome: 'Palhano',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4211900,
        nome: 'Palhoça',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3146701,
        nome: 'Palma',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4212007,
        nome: 'Palma Sola',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2310100,
        nome: 'Palmácia',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2610004,
        nome: 'Palmares',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4313656,
        nome: 'Palmares do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3535101,
        nome: 'Palmares Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4117602,
        nome: 'Palmas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1721000,
        nome: 'Palmas',
        capital: 's',
        sigla_uf: 'TO'
    },
    {
        id: 2923407,
        nome: 'Palmas de Monte Alto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4117701,
        nome: 'Palmeira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4212056,
        nome: 'Palmeira',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3535200,
        nome: 'Palmeira d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4313706,
        nome: 'Palmeira das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2207405,
        nome: 'Palmeira do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2706307,
        nome: 'Palmeira dos Índios',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2207504,
        nome: 'Palmeirais',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2107605,
        nome: 'Palmeirândia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1715705,
        nome: 'Palmeirante',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2923506,
        nome: 'Palmeiras',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5215702,
        nome: 'Palmeiras de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1713809,
        nome: 'Palmeiras do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2610103,
        nome: 'Palmeirina',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1715754,
        nome: 'Palmeirópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5215801,
        nome: 'Palmelo',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5215900,
        nome: 'Palminópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3535309,
        nome: 'Palmital',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4117800,
        nome: 'Palmital',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4313805,
        nome: 'Palmitinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4212106,
        nome: 'Palmitos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3146750,
        nome: 'Palmópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4117909,
        nome: 'Palotina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5216007,
        nome: 'Panamá',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4313904,
        nome: 'Panambi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3204005,
        nome: 'Pancas',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2610202,
        nome: 'Panelas',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3535408,
        nome: 'Panorama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4313953,
        nome: 'Pantano Grande',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2706406,
        nome: 'Pão de Açúcar',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3146909,
        nome: 'Papagaios',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4212205,
        nome: 'Papanduva',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2207553,
        nome: 'Paquetá',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3147105,
        nome: 'Pará de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3303609,
        nome: 'Paracambi',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3147006,
        nome: 'Paracatu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2310209,
        nome: 'Paracuru',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1505502,
        nome: 'Paragominas',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3147204,
        nome: 'Paraguaçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3535507,
        nome: 'Paraguaçu Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4314001,
        nome: 'Paraí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3303708,
        nome: 'Paraíba do Sul',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2107704,
        nome: 'Paraibano',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3535606,
        nome: 'Paraibuna',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2310258,
        nome: 'Paraipaba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3535705,
        nome: 'Paraíso',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4212239,
        nome: 'Paraíso',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5006275,
        nome: 'Paraíso das Águas',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4118006,
        nome: 'Paraíso do Norte',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4314027,
        nome: 'Paraíso do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1716109,
        nome: 'Paraíso do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3147303,
        nome: 'Paraisópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2310308,
        nome: 'Parambu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2923605,
        nome: 'Paramirim',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2310407,
        nome: 'Paramoti',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1716208,
        nome: 'Paranã',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2408607,
        nome: 'Paraná',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4118105,
        nome: 'Paranacity',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4118204,
        nome: 'Paranaguá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5006309,
        nome: 'Paranaíba',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5216304,
        nome: 'Paranaiguara',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5106299,
        nome: 'Paranaíta',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3535804,
        nome: 'Paranapanema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4118303,
        nome: 'Paranapoema',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3535903,
        nome: 'Paranapuã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2610301,
        nome: 'Paranatama',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5106307,
        nome: 'Paranatinga',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4118402,
        nome: 'Paranavaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5006358,
        nome: 'Paranhos',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3147402,
        nome: 'Paraopeba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3536000,
        nome: 'Parapuã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2510659,
        nome: 'Parari',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2923704,
        nome: 'Paratinga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3303807,
        nome: 'Paraty',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2408706,
        nome: 'Paraú',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1505536,
        nome: 'Parauapebas',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5216403,
        nome: 'Paraúna',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2408805,
        nome: 'Parazinho',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3536109,
        nome: 'Pardinho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4314035,
        nome: 'Pareci Novo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1101450,
        nome: 'Parecis',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2408904,
        nome: 'Parelhas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2706422,
        nome: 'Pariconha',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1303403,
        nome: 'Parintins',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2923803,
        nome: 'Paripiranga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2706448,
        nome: 'Paripueira',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3536208,
        nome: 'Pariquera-Açu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3536257,
        nome: 'Parisi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2207603,
        nome: 'Parnaguá',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2207702,
        nome: 'Parnaíba',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2403251,
        nome: 'Parnamirim',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2610400,
        nome: 'Parnamirim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2107803,
        nome: 'Parnarama',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4314050,
        nome: 'Parobé',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2409100,
        nome: 'Passa e Fica',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3147600,
        nome: 'Passa Quatro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4314068,
        nome: 'Passa Sete',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3147709,
        nome: 'Passa Tempo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3147808,
        nome: 'Passa-Vinte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3147501,
        nome: 'Passabém',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2409209,
        nome: 'Passagem',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2510709,
        nome: 'Passagem',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2107902,
        nome: 'Passagem Franca',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2207751,
        nome: 'Passagem Franca do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2610509,
        nome: 'Passira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2706505,
        nome: 'Passo de Camaragibe',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4212254,
        nome: 'Passo de Torres',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4314076,
        nome: 'Passo do Sobrado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4314100,
        nome: 'Passo Fundo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3147907,
        nome: 'Passos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4212270,
        nome: 'Passos Maia',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2108009,
        nome: 'Pastos Bons',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3147956,
        nome: 'Patis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4118451,
        nome: 'Pato Bragado',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4118501,
        nome: 'Pato Branco',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2510808,
        nome: 'Patos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3148004,
        nome: 'Patos de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2207777,
        nome: 'Patos do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3148103,
        nome: 'Patrocínio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3148202,
        nome: 'Patrocínio do Muriaé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3536307,
        nome: 'Patrocínio Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2409308,
        nome: 'Patu',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3303856,
        nome: 'Paty do Alferes',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2923902,
        nome: 'Pau Brasil',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1505551,
        nome: 'Pau d\'Arco',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1716307,
        nome: 'Pau D\'Arco',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2207793,
        nome: 'Pau D\'Arco do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2409407,
        nome: 'Pau dos Ferros',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2610608,
        nome: 'Paudalho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1303502,
        nome: 'Pauini',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3148301,
        nome: 'Paula Cândido',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4118600,
        nome: 'Paula Freitas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3536406,
        nome: 'Paulicéia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3536505,
        nome: 'Paulínia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2108058,
        nome: 'Paulino Neves',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2510907,
        nome: 'Paulista',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2610707,
        nome: 'Paulista',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2207801,
        nome: 'Paulistana',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3536570,
        nome: 'Paulistânia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3148400,
        nome: 'Paulistas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2924009,
        nome: 'Paulo Afonso',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4314134,
        nome: 'Paulo Bento',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3536604,
        nome: 'Paulo de Faria',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4118709,
        nome: 'Paulo Frontin',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2706604,
        nome: 'Paulo Jacinto',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4212304,
        nome: 'Paulo Lopes',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2108108,
        nome: 'Paulo Ramos',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3148509,
        nome: 'Pavão',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4314159,
        nome: 'Paverama',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2207850,
        nome: 'Pavussu',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2924058,
        nome: 'Pé de Serra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4118808,
        nome: 'Peabiru',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3148608,
        nome: 'Peçanha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3536703,
        nome: 'Pederneiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2610806,
        nome: 'Pedra',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3148707,
        nome: 'Pedra Azul',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3536802,
        nome: 'Pedra Bela',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3148756,
        nome: 'Pedra Bonita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2511004,
        nome: 'Pedra Branca',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2310506,
        nome: 'Pedra Branca',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1600154,
        nome: 'Pedra Branca do Amapari',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 3148806,
        nome: 'Pedra do Anta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3148905,
        nome: 'Pedra do Indaiá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3149002,
        nome: 'Pedra Dourada',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2409506,
        nome: 'Pedra Grande',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2511103,
        nome: 'Pedra Lavrada',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2805000,
        nome: 'Pedra Mole',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2409605,
        nome: 'Pedra Preta',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5106372,
        nome: 'Pedra Preta',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3149101,
        nome: 'Pedralva',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3536901,
        nome: 'Pedranópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2924108,
        nome: 'Pedrão',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4314175,
        nome: 'Pedras Altas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2511202,
        nome: 'Pedras de Fogo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3149150,
        nome: 'Pedras de Maria da Cruz',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4212403,
        nome: 'Pedras Grandes',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3537008,
        nome: 'Pedregulho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3537107,
        nome: 'Pedreira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2108207,
        nome: 'Pedreiras',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2805109,
        nome: 'Pedrinhas',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3537156,
        nome: 'Pedrinhas Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3149200,
        nome: 'Pedrinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1716505,
        nome: 'Pedro Afonso',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2924207,
        nome: 'Pedro Alexandre',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2409704,
        nome: 'Pedro Avelino',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3204054,
        nome: 'Pedro Canário',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3537206,
        nome: 'Pedro de Toledo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2108256,
        nome: 'Pedro do Rosário',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5006408,
        nome: 'Pedro Gomes',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2207900,
        nome: 'Pedro II',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2207934,
        nome: 'Pedro Laurentino',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3149309,
        nome: 'Pedro Leopoldo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4314209,
        nome: 'Pedro Osório',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2512721,
        nome: 'Pedro Régis',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3149408,
        nome: 'Pedro Teixeira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2409803,
        nome: 'Pedro Velho',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1716604,
        nome: 'Peixe',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1505601,
        nome: 'Peixe-Boi',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5106422,
        nome: 'Peixoto de Azevedo',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4314308,
        nome: 'Pejuçara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4314407,
        nome: 'Pelotas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2310605,
        nome: 'Penaforte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2108306,
        nome: 'Penalva',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3537305,
        nome: 'Penápolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2409902,
        nome: 'Pendências',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2706703,
        nome: 'Penedo',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4212502,
        nome: 'Penha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2310704,
        nome: 'Pentecoste',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3149507,
        nome: 'Pequeri',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3149606,
        nome: 'Pequi',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1716653,
        nome: 'Pequizeiro',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3149705,
        nome: 'Perdigão',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3149804,
        nome: 'Perdizes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3149903,
        nome: 'Perdões',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3537404,
        nome: 'Pereira Barreto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3537503,
        nome: 'Pereiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2310803,
        nome: 'Pereiro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2108405,
        nome: 'Peri Mirim',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3149952,
        nome: 'Periquito',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4212601,
        nome: 'Peritiba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2108454,
        nome: 'Peritoró',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4118857,
        nome: 'Perobal',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4118907,
        nome: 'Pérola',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4119004,
        nome: 'Pérola d\'Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5216452,
        nome: 'Perolândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3537602,
        nome: 'Peruíbe',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3150000,
        nome: 'Pescador',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4212650,
        nome: 'Pescaria Brava',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2610905,
        nome: 'Pesqueira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2611002,
        nome: 'Petrolândia',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4212700,
        nome: 'Petrolândia',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2611101,
        nome: 'Petrolina',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5216809,
        nome: 'Petrolina de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3303906,
        nome: 'Petrópolis',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2706802,
        nome: 'Piaçabuçu',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3537701,
        nome: 'Piacatu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2511301,
        nome: 'Piancó',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2924306,
        nome: 'Piatã',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3150109,
        nome: 'Piau',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4314423,
        nome: 'Picada Café',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1505635,
        nome: 'Piçarra',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2208007,
        nome: 'Picos',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2511400,
        nome: 'Picuí',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3537800,
        nome: 'Piedade',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3150158,
        nome: 'Piedade de Caratinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3150208,
        nome: 'Piedade de Ponte Nova',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3150307,
        nome: 'Piedade do Rio Grande',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3150406,
        nome: 'Piedade dos Gerais',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4119103,
        nome: 'Piên',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2924405,
        nome: 'Pilão Arcado',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2511509,
        nome: 'Pilar',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2706901,
        nome: 'Pilar',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5216908,
        nome: 'Pilar de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3537909,
        nome: 'Pilar do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2410009,
        nome: 'Pilões',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2511608,
        nome: 'Pilões',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2511707,
        nome: 'Pilõezinhos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3150505,
        nome: 'Pimenta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1100189,
        nome: 'Pimenta Bueno',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2208106,
        nome: 'Pimenteiras',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1101468,
        nome: 'Pimenteiras do Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2924504,
        nome: 'Pindaí',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3538006,
        nome: 'Pindamonhangaba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2108504,
        nome: 'Pindaré-Mirim',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2707008,
        nome: 'Pindoba',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2924603,
        nome: 'Pindobaçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3538105,
        nome: 'Pindorama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1717008,
        nome: 'Pindorama do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2310852,
        nome: 'Pindoretama',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3150539,
        nome: 'Pingo-d\'Água',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4119152,
        nome: 'Pinhais',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4314456,
        nome: 'Pinhal',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4314464,
        nome: 'Pinhal da Serra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4119251,
        nome: 'Pinhal de São Bento',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4314472,
        nome: 'Pinhal Grande',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4119202,
        nome: 'Pinhalão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3538204,
        nome: 'Pinhalzinho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4212908,
        nome: 'Pinhalzinho',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2805208,
        nome: 'Pinhão',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4119301,
        nome: 'Pinhão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3303955,
        nome: 'Pinheiral',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4314498,
        nome: 'Pinheirinho do Vale',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2108603,
        nome: 'Pinheiro',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4314506,
        nome: 'Pinheiro Machado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4213005,
        nome: 'Pinheiro Preto',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3204104,
        nome: 'Pinheiros',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2924652,
        nome: 'Pintadas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4314548,
        nome: 'Pinto Bandeira',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3150570,
        nome: 'Pintópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2208205,
        nome: 'Pio IX',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2108702,
        nome: 'Pio XII',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3538303,
        nome: 'Piquerobi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2310902,
        nome: 'Piquet Carneiro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3538501,
        nome: 'Piquete',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3538600,
        nome: 'Piracaia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5217104,
        nome: 'Piracanjuba',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3150604,
        nome: 'Piracema',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3538709,
        nome: 'Piracicaba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2208304,
        nome: 'Piracuruca',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3304003,
        nome: 'Piraí',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2924678,
        nome: 'Piraí do Norte',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4119400,
        nome: 'Piraí do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3538808,
        nome: 'Piraju',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3150703,
        nome: 'Pirajuba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3538907,
        nome: 'Pirajuí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2805307,
        nome: 'Pirambu',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3150802,
        nome: 'Piranga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3539004,
        nome: 'Pirangi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3150901,
        nome: 'Piranguçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3151008,
        nome: 'Piranguinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2707107,
        nome: 'Piranhas',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5217203,
        nome: 'Piranhas',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2108801,
        nome: 'Pirapemas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3151107,
        nome: 'Pirapetinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4314555,
        nome: 'Pirapó',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3151206,
        nome: 'Pirapora',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3539103,
        nome: 'Pirapora do Bom Jesus',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3539202,
        nome: 'Pirapozinho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4119509,
        nome: 'Piraquara',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1717206,
        nome: 'Piraquê',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3539301,
        nome: 'Pirassununga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4314605,
        nome: 'Piratini',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3539400,
        nome: 'Piratininga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4213104,
        nome: 'Piratuba',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3151305,
        nome: 'Piraúba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5217302,
        nome: 'Pirenópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5217401,
        nome: 'Pires do Rio',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2310951,
        nome: 'Pires Ferreira',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2924702,
        nome: 'Piripá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2208403,
        nome: 'Piripiri',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2924801,
        nome: 'Piritiba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2511806,
        nome: 'Pirpirituba',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4119608,
        nome: 'Pitanga',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3539509,
        nome: 'Pitangueiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4119657,
        nome: 'Pitangueiras',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3151404,
        nome: 'Pitangui',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2511905,
        nome: 'Pitimbu',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1717503,
        nome: 'Pium',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3204203,
        nome: 'Piúma',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3151503,
        nome: 'Piumhi',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1505650,
        nome: 'Placas',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1200385,
        nome: 'Plácido de Castro',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 5217609,
        nome: 'Planaltina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4119707,
        nome: 'Planaltina do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2924900,
        nome: 'Planaltino',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2925006,
        nome: 'Planalto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4314704,
        nome: 'Planalto',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3539608,
        nome: 'Planalto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4119806,
        nome: 'Planalto',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4213153,
        nome: 'Planalto Alegre',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5106455,
        nome: 'Planalto da Serra',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3151602,
        nome: 'Planura',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3539707,
        nome: 'Platina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3539806,
        nome: 'Poá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2611200,
        nome: 'Poção',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2108900,
        nome: 'Poção de Pedras',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2512002,
        nome: 'Pocinhos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2410108,
        nome: 'Poço Branco',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2512036,
        nome: 'Poço Dantas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4314753,
        nome: 'Poço das Antas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2707206,
        nome: 'Poço das Trincheiras',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2512077,
        nome: 'Poço de José de Moura',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3151701,
        nome: 'Poço Fundo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2805406,
        nome: 'Poço Redondo',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2805505,
        nome: 'Poço Verde',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2925105,
        nome: 'Poções',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5106505,
        nome: 'Poconé',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3151800,
        nome: 'Poços de Caldas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3151909,
        nome: 'Pocrane',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2925204,
        nome: 'Pojuca',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3539905,
        nome: 'Poloni',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2512101,
        nome: 'Pombal',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2611309,
        nome: 'Pombos',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4213203,
        nome: 'Pomerode',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3540002,
        nome: 'Pompéia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3152006,
        nome: 'Pompéu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3540101,
        nome: 'Pongaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1505700,
        nome: 'Ponta de Pedras',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4119905,
        nome: 'Ponta Grossa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5006606,
        nome: 'Ponta Porã',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3540200,
        nome: 'Pontal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5106653,
        nome: 'Pontal do Araguaia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4119954,
        nome: 'Pontal do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5217708,
        nome: 'Pontalina',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3540259,
        nome: 'Pontalinda',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4314779,
        nome: 'Pontão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4213302,
        nome: 'Ponte Alta',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1717800,
        nome: 'Ponte Alta do Bom Jesus',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4213351,
        nome: 'Ponte Alta do Norte',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1717909,
        nome: 'Ponte Alta do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5106703,
        nome: 'Ponte Branca',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3152105,
        nome: 'Ponte Nova',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4314787,
        nome: 'Ponte Preta',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4213401,
        nome: 'Ponte Serrada',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5106752,
        nome: 'Pontes e Lacerda',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3540309,
        nome: 'Pontes Gestal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3204252,
        nome: 'Ponto Belo',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3152131,
        nome: 'Ponto Chique',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3152170,
        nome: 'Ponto dos Volantes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2925253,
        nome: 'Ponto Novo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3540408,
        nome: 'Populina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2311009,
        nome: 'Poranga',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3540507,
        nome: 'Porangaba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5218003,
        nome: 'Porangatu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3304102,
        nome: 'Porciúncula',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4120002,
        nome: 'Porecatu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2410207,
        nome: 'Portalegre',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4314803,
        nome: 'Portão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5218052,
        nome: 'Porteirão',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2311108,
        nome: 'Porteiras',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3152204,
        nome: 'Porteirinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1505809,
        nome: 'Portel',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5218102,
        nome: 'Portelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2208502,
        nome: 'Porto',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1200807,
        nome: 'Porto Acre',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4314902,
        nome: 'Porto Alegre',
        capital: 's',
        sigla_uf: 'RS'
    },
    {
        id: 5106778,
        nome: 'Porto Alegre do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2208551,
        nome: 'Porto Alegre do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1718006,
        nome: 'Porto Alegre do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4120101,
        nome: 'Porto Amazonas',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4120150,
        nome: 'Porto Barreiro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4213500,
        nome: 'Porto Belo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2707305,
        nome: 'Porto Calvo',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2805604,
        nome: 'Porto da Folha',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 1505908,
        nome: 'Porto de Moz',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2707404,
        nome: 'Porto de Pedras',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2410256,
        nome: 'Porto do Mangue',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5106802,
        nome: 'Porto dos Gaúchos',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5106828,
        nome: 'Porto Esperidião',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5106851,
        nome: 'Porto Estrela',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3540606,
        nome: 'Porto Feliz',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3540705,
        nome: 'Porto Ferreira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3152303,
        nome: 'Porto Firme',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2109007,
        nome: 'Porto Franco',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1600535,
        nome: 'Porto Grande',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 4315008,
        nome: 'Porto Lucena',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4315057,
        nome: 'Porto Mauá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5006903,
        nome: 'Porto Murtinho',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 1718204,
        nome: 'Porto Nacional',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3304110,
        nome: 'Porto Real',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2707503,
        nome: 'Porto Real do Colégio',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4120200,
        nome: 'Porto Rico',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2109056,
        nome: 'Porto Rico do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2925303,
        nome: 'Porto Seguro',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4213609,
        nome: 'Porto União',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1100205,
        nome: 'Porto Velho',
        capital: 's',
        sigla_uf: 'RO'
    },
    {
        id: 4315073,
        nome: 'Porto Vera Cruz',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4120309,
        nome: 'Porto Vitória',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1200393,
        nome: 'Porto Walter',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4315107,
        nome: 'Porto Xavier',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5218300,
        nome: 'Posse',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3152402,
        nome: 'Poté',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2311207,
        nome: 'Potengi',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3540754,
        nome: 'Potim',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2925402,
        nome: 'Potiraguá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3540804,
        nome: 'Potirendaba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2311231,
        nome: 'Potiretama',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3152501,
        nome: 'Pouso Alegre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3152600,
        nome: 'Pouso Alto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4315131,
        nome: 'Pouso Novo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4213708,
        nome: 'Pouso Redondo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5107008,
        nome: 'Poxoréu',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3540853,
        nome: 'Pracinha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1600550,
        nome: 'Pracuúba',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 2925501,
        nome: 'Prado',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4120333,
        nome: 'Prado Ferreira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3540903,
        nome: 'Pradópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3152709,
        nome: 'Prados',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3541000,
        nome: 'Praia Grande',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4213807,
        nome: 'Praia Grande',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1718303,
        nome: 'Praia Norte',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1506005,
        nome: 'Prainha',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4120358,
        nome: 'Pranchita',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3152808,
        nome: 'Prata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2512200,
        nome: 'Prata',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2208601,
        nome: 'Prata do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3541059,
        nome: 'Pratânia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3152907,
        nome: 'Pratápolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3153004,
        nome: 'Pratinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3541109,
        nome: 'Presidente Alves',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3541208,
        nome: 'Presidente Bernardes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3153103,
        nome: 'Presidente Bernardes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4213906,
        nome: 'Presidente Castello Branco',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4120408,
        nome: 'Presidente Castelo Branco',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2925600,
        nome: 'Presidente Dutra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2109106,
        nome: 'Presidente Dutra',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3541307,
        nome: 'Presidente Epitácio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1303536,
        nome: 'Presidente Figueiredo',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4214003,
        nome: 'Presidente Getúlio',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2925709,
        nome: 'Presidente Jânio Quadros',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3153202,
        nome: 'Presidente Juscelino',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2109205,
        nome: 'Presidente Juscelino',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1718402,
        nome: 'Presidente Kennedy',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3204302,
        nome: 'Presidente Kennedy',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3153301,
        nome: 'Presidente Kubitschek',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4315149,
        nome: 'Presidente Lucena',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100254,
        nome: 'Presidente Médici',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2109239,
        nome: 'Presidente Médici',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4214102,
        nome: 'Presidente Nereu',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3153400,
        nome: 'Presidente Olegário',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3541406,
        nome: 'Presidente Prudente',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2109270,
        nome: 'Presidente Sarney',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2925758,
        nome: 'Presidente Tancredo Neves',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2109304,
        nome: 'Presidente Vargas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3541505,
        nome: 'Presidente Venceslau',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2611408,
        nome: 'Primavera',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1506104,
        nome: 'Primavera',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1101476,
        nome: 'Primavera de Rondônia',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 5107040,
        nome: 'Primavera do Leste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2109403,
        nome: 'Primeira Cruz',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4120507,
        nome: 'Primeiro de Maio',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4214151,
        nome: 'Princesa',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2512309,
        nome: 'Princesa Isabel',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5218391,
        nome: 'Professor Jamil',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4315156,
        nome: 'Progresso',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3541604,
        nome: 'Promissão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2805703,
        nome: 'Propriá',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4315172,
        nome: 'Protásio Alves',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3153608,
        nome: 'Prudente de Morais',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4120606,
        nome: 'Prudentópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1718451,
        nome: 'Pugmil',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2410405,
        nome: 'Pureza',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4315206,
        nome: 'Putinga',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2512408,
        nome: 'Puxinanã',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3541653,
        nome: 'Quadra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4315305,
        nome: 'Quaraí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3153707,
        nome: 'Quartel Geral',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4120655,
        nome: 'Quarto Centenário',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3541703,
        nome: 'Quatá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4120705,
        nome: 'Quatiguá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1506112,
        nome: 'Quatipuru',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3304128,
        nome: 'Quatis',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4120804,
        nome: 'Quatro Barras',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4315313,
        nome: 'Quatro Irmãos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4120853,
        nome: 'Quatro Pontes',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2707602,
        nome: 'Quebrangulo',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4120903,
        nome: 'Quedas do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2208650,
        nome: 'Queimada Nova',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2512507,
        nome: 'Queimadas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2925808,
        nome: 'Queimadas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3304144,
        nome: 'Queimados',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3541802,
        nome: 'Queiroz',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3541901,
        nome: 'Queluz',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3153806,
        nome: 'Queluzito',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5107065,
        nome: 'Querência',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4121000,
        nome: 'Querência do Norte',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4315321,
        nome: 'Quevedos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2925907,
        nome: 'Quijingue',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4214201,
        nome: 'Quilombo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4121109,
        nome: 'Quinta do Sol',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3542008,
        nome: 'Quintana',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4315354,
        nome: 'Quinze de Novembro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2611507,
        nome: 'Quipapá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5218508,
        nome: 'Quirinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3304151,
        nome: 'Quissamã',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4121208,
        nome: 'Quitandinha',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2311264,
        nome: 'Quiterianópolis',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2512606,
        nome: 'Quixabá',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2611533,
        nome: 'Quixaba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2925931,
        nome: 'Quixabeira',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2311306,
        nome: 'Quixadá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2311355,
        nome: 'Quixelô',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2311405,
        nome: 'Quixeramobim',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2311504,
        nome: 'Quixeré',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2410504,
        nome: 'Rafael Fernandes',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2410603,
        nome: 'Rafael Godeiro',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2925956,
        nome: 'Rafael Jambeiro',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3542107,
        nome: 'Rafard',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4121257,
        nome: 'Ramilândia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3542206,
        nome: 'Rancharia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4121307,
        nome: 'Rancho Alegre',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4121356,
        nome: 'Rancho Alegre D\'Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4214300,
        nome: 'Rancho Queimado',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2109452,
        nome: 'Raposa',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3153905,
        nome: 'Raposos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3154002,
        nome: 'Raul Soares',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4121406,
        nome: 'Realeza',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4121505,
        nome: 'Rebouças',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2611606,
        nome: 'Recife',
        capital: 's',
        sigla_uf: 'PE'
    },
    {
        id: 3154101,
        nome: 'Recreio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1718501,
        nome: 'Recursolândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1506138,
        nome: 'Redenção',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2311603,
        nome: 'Redenção',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3542305,
        nome: 'Redenção da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2208700,
        nome: 'Redenção do Gurguéia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4315404,
        nome: 'Redentora',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3154150,
        nome: 'Reduto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2208809,
        nome: 'Regeneração',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3542404,
        nome: 'Regente Feijó',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3542503,
        nome: 'Reginópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3542602,
        nome: 'Registro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4315453,
        nome: 'Relvado',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2926004,
        nome: 'Remanso',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2512705,
        nome: 'Remígio',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4121604,
        nome: 'Renascença',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2311702,
        nome: 'Reriutaba',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3304201,
        nome: 'Resende',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3154200,
        nome: 'Resende Costa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4121703,
        nome: 'Reserva',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5107156,
        nome: 'Reserva do Cabaçal',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4121752,
        nome: 'Reserva do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3154309,
        nome: 'Resplendor',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3154408,
        nome: 'Ressaquinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3542701,
        nome: 'Restinga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4315503,
        nome: 'Restinga Sêca',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2926103,
        nome: 'Retirolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2512747,
        nome: 'Riachão',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2109502,
        nome: 'Riachão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2926202,
        nome: 'Riachão das Neves',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2512754,
        nome: 'Riachão do Bacamarte',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2805802,
        nome: 'Riachão do Dantas',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2926301,
        nome: 'Riachão do Jacuípe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2512762,
        nome: 'Riachão do Poço',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1718550,
        nome: 'Riachinho',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3154457,
        nome: 'Riachinho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2410702,
        nome: 'Riacho da Cruz',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2611705,
        nome: 'Riacho das Almas',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2410801,
        nome: 'Riacho de Santana',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2926400,
        nome: 'Riacho de Santana',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2512788,
        nome: 'Riacho de Santo Antônio',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2512804,
        nome: 'Riacho dos Cavalos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3154507,
        nome: 'Riacho dos Machados',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2208858,
        nome: 'Riacho Frio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2410900,
        nome: 'Riachuelo',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2805901,
        nome: 'Riachuelo',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 5218607,
        nome: 'Rialma',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5218706,
        nome: 'Rianápolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2109551,
        nome: 'Ribamar Fiquene',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5007109,
        nome: 'Ribas do Rio Pardo',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3542800,
        nome: 'Ribeira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2926509,
        nome: 'Ribeira do Amparo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2208874,
        nome: 'Ribeira do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2926608,
        nome: 'Ribeira do Pombal',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2611804,
        nome: 'Ribeirão',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3542909,
        nome: 'Ribeirão Bonito',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543006,
        nome: 'Ribeirão Branco',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5107180,
        nome: 'Ribeirão Cascalheira',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4121802,
        nome: 'Ribeirão Claro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3543105,
        nome: 'Ribeirão Corrente',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3154606,
        nome: 'Ribeirão das Neves',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2926657,
        nome: 'Ribeirão do Largo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4121901,
        nome: 'Ribeirão do Pinhal',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3543204,
        nome: 'Ribeirão do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543238,
        nome: 'Ribeirão dos Índios',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543253,
        nome: 'Ribeirão Grande',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543303,
        nome: 'Ribeirão Pires',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543402,
        nome: 'Ribeirão Preto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3154705,
        nome: 'Ribeirão Vermelho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5107198,
        nome: 'Ribeirãozinho',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2208908,
        nome: 'Ribeiro Gonçalves',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2806008,
        nome: 'Ribeirópolis',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3543600,
        nome: 'Rifaina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543709,
        nome: 'Rincão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3543808,
        nome: 'Rinópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3154804,
        nome: 'Rio Acima',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4122008,
        nome: 'Rio Azul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3204351,
        nome: 'Rio Bananal',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4122107,
        nome: 'Rio Bom',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3304300,
        nome: 'Rio Bonito',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4122156,
        nome: 'Rio Bonito do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5107206,
        nome: 'Rio Branco',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1200401,
        nome: 'Rio Branco',
        capital: 's',
        sigla_uf: 'AC'
    },
    {
        id: 4122172,
        nome: 'Rio Branco do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4122206,
        nome: 'Rio Branco do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5007208,
        nome: 'Rio Brilhante',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3154903,
        nome: 'Rio Casca',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3304409,
        nome: 'Rio Claro',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3543907,
        nome: 'Rio Claro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1100262,
        nome: 'Rio Crespo',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 1718659,
        nome: 'Rio da Conceição',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4214409,
        nome: 'Rio das Antas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3304508,
        nome: 'Rio das Flores',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3304524,
        nome: 'Rio das Ostras',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3544004,
        nome: 'Rio das Pedras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2926707,
        nome: 'Rio de Contas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3304557,
        nome: 'Rio de Janeiro',
        capital: 's',
        sigla_uf: 'RJ'
    },
    {
        id: 2926806,
        nome: 'Rio do Antônio',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4214508,
        nome: 'Rio do Campo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2408953,
        nome: 'Rio do Fogo',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4214607,
        nome: 'Rio do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2926905,
        nome: 'Rio do Pires',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3155108,
        nome: 'Rio do Prado',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4214805,
        nome: 'Rio do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3155009,
        nome: 'Rio Doce',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1718709,
        nome: 'Rio dos Bois',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4214706,
        nome: 'Rio dos Cedros',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4315552,
        nome: 'Rio dos Índios',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3155207,
        nome: 'Rio Espera',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2611903,
        nome: 'Rio Formoso',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4214904,
        nome: 'Rio Fortuna',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4315602,
        nome: 'Rio Grande',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3544103,
        nome: 'Rio Grande da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2209005,
        nome: 'Rio Grande do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2707701,
        nome: 'Rio Largo',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3155306,
        nome: 'Rio Manso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1506161,
        nome: 'Rio Maria',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4215000,
        nome: 'Rio Negrinho',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5007307,
        nome: 'Rio Negro',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 4122305,
        nome: 'Rio Negro',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3155405,
        nome: 'Rio Novo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3204401,
        nome: 'Rio Novo do Sul',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3155504,
        nome: 'Rio Paranaíba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4315701,
        nome: 'Rio Pardo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3155603,
        nome: 'Rio Pardo de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3155702,
        nome: 'Rio Piracicaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3155801,
        nome: 'Rio Pomba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3155900,
        nome: 'Rio Preto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1303569,
        nome: 'Rio Preto da Eva',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 5218789,
        nome: 'Rio Quente',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2927002,
        nome: 'Rio Real',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4215059,
        nome: 'Rio Rufino',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1718758,
        nome: 'Rio Sono',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2512903,
        nome: 'Rio Tinto',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5218805,
        nome: 'Rio Verde',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5007406,
        nome: 'Rio Verde de Mato Grosso',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3156007,
        nome: 'Rio Vermelho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3544202,
        nome: 'Riolândia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4315750,
        nome: 'Riozinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4215075,
        nome: 'Riqueza',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3156106,
        nome: 'Ritápolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3543501,
        nome: 'Riversul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4315800,
        nome: 'Roca Sales',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5007505,
        nome: 'Rochedo',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3156205,
        nome: 'Rochedo de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4215109,
        nome: 'Rodeio',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4315909,
        nome: 'Rodeio Bonito',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3156304,
        nome: 'Rodeiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2927101,
        nome: 'Rodelas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2411007,
        nome: 'Rodolfo Fernandes',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1200427,
        nome: 'Rodrigues Alves',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4315958,
        nome: 'Rolador',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4122404,
        nome: 'Rolândia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4316006,
        nome: 'Rolante',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1100288,
        nome: 'Rolim de Moura',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3156403,
        nome: 'Romaria',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4215208,
        nome: 'Romelândia',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4122503,
        nome: 'Roncador',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4316105,
        nome: 'Ronda Alta',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4316204,
        nome: 'Rondinha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5107578,
        nome: 'Rondolândia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4122602,
        nome: 'Rondon',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1506187,
        nome: 'Rondon do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5107602,
        nome: 'Rondonópolis',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4316303,
        nome: 'Roque Gonzales',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1400472,
        nome: 'Rorainópolis',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 3544251,
        nome: 'Rosana',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2109601,
        nome: 'Rosário',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3156452,
        nome: 'Rosário da Limeira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2806107,
        nome: 'Rosário do Catete',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4122651,
        nome: 'Rosário do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4316402,
        nome: 'Rosário do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5107701,
        nome: 'Rosário Oeste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3544301,
        nome: 'Roseira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2707800,
        nome: 'Roteiro',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3156502,
        nome: 'Rubelita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3544400,
        nome: 'Rubiácea',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5218904,
        nome: 'Rubiataba',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3156601,
        nome: 'Rubim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3544509,
        nome: 'Rubinéia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1506195,
        nome: 'Rurópolis',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2311801,
        nome: 'Russas',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2411106,
        nome: 'Ruy Barbosa',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2927200,
        nome: 'Ruy Barbosa',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3156700,
        nome: 'Sabará',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4122701,
        nome: 'Sabáudia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3544608,
        nome: 'Sabino',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3156809,
        nome: 'Sabinópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2311900,
        nome: 'Saboeiro',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3156908,
        nome: 'Sacramento',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4316428,
        nome: 'Sagrada Família',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3544707,
        nome: 'Sagres',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2612000,
        nome: 'Sairé',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4316436,
        nome: 'Saldanha Marinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3544806,
        nome: 'Sales',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3544905,
        nome: 'Sales Oliveira',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3545001,
        nome: 'Salesópolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4215307,
        nome: 'Salete',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2513000,
        nome: 'Salgadinho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2612109,
        nome: 'Salgadinho',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2806206,
        nome: 'Salgado',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2513109,
        nome: 'Salgado de São Félix',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4122800,
        nome: 'Salgado Filho',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2612208,
        nome: 'Salgueiro',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3157005,
        nome: 'Salinas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2927309,
        nome: 'Salinas da Margarida',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1506203,
        nome: 'Salinópolis',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2311959,
        nome: 'Salitre',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3545100,
        nome: 'Salmourão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2612307,
        nome: 'Saloá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4215356,
        nome: 'Saltinho',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3545159,
        nome: 'Saltinho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3545209,
        nome: 'Salto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3157104,
        nome: 'Salto da Divisa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3545308,
        nome: 'Salto de Pirapora',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5107750,
        nome: 'Salto do Céu',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4122909,
        nome: 'Salto do Itararé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4316451,
        nome: 'Salto do Jacuí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4123006,
        nome: 'Salto do Lontra',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3545407,
        nome: 'Salto Grande',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4215406,
        nome: 'Salto Veloso',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2927408,
        nome: 'Salvador',
        capital: 's',
        sigla_uf: 'BA'
    },
    {
        id: 4316477,
        nome: 'Salvador das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4316501,
        nome: 'Salvador do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1506302,
        nome: 'Salvaterra',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2109700,
        nome: 'Sambaíba',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1718808,
        nome: 'Sampaio',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4316600,
        nome: 'Sananduva',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5219001,
        nome: 'Sanclerlândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1718840,
        nome: 'Sandolândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3545506,
        nome: 'Sandovalina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4215455,
        nome: 'Sangão',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2612406,
        nome: 'Sanharó',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4317103,
        nome: 'Sant\'Ana do Livramento',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3545605,
        nome: 'Santa Adélia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3545704,
        nome: 'Santa Albertina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4123105,
        nome: 'Santa Amélia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2927507,
        nome: 'Santa Bárbara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3157203,
        nome: 'Santa Bárbara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3545803,
        nome: 'Santa Bárbara d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5219100,
        nome: 'Santa Bárbara de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3157252,
        nome: 'Santa Bárbara do Leste',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3157278,
        nome: 'Santa Bárbara do Monte Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1506351,
        nome: 'Santa Bárbara do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4316709,
        nome: 'Santa Bárbara do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3157302,
        nome: 'Santa Bárbara do Tugúrio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3546009,
        nome: 'Santa Branca',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2927606,
        nome: 'Santa Brígida',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5107248,
        nome: 'Santa Carmem',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4215505,
        nome: 'Santa Cecília',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2513158,
        nome: 'Santa Cecília',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4123204,
        nome: 'Santa Cecília do Pavão',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4316733,
        nome: 'Santa Cecília do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3546108,
        nome: 'Santa Clara d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4316758,
        nome: 'Santa Clara do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2411205,
        nome: 'Santa Cruz',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2513208,
        nome: 'Santa Cruz',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2612455,
        nome: 'Santa Cruz',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2927705,
        nome: 'Santa Cruz Cabrália',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2612471,
        nome: 'Santa Cruz da Baixa Verde',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3546207,
        nome: 'Santa Cruz da Conceição',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3546256,
        nome: 'Santa Cruz da Esperança',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2927804,
        nome: 'Santa Cruz da Vitória',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3546306,
        nome: 'Santa Cruz das Palmeiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5219209,
        nome: 'Santa Cruz de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3157336,
        nome: 'Santa Cruz de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4123303,
        nome: 'Santa Cruz de Monte Castelo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3157377,
        nome: 'Santa Cruz de Salinas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1506401,
        nome: 'Santa Cruz do Arari',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2612505,
        nome: 'Santa Cruz do Capibaribe',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3157401,
        nome: 'Santa Cruz do Escalvado',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2209104,
        nome: 'Santa Cruz do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3546405,
        nome: 'Santa Cruz do Rio Pardo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4316808,
        nome: 'Santa Cruz do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5107743,
        nome: 'Santa Cruz do Xingu',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2209153,
        nome: 'Santa Cruz dos Milagres',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3157500,
        nome: 'Santa Efigênia de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3546504,
        nome: 'Santa Ernestina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4123402,
        nome: 'Santa Fé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5219258,
        nome: 'Santa Fé de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3157609,
        nome: 'Santa Fé de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1718865,
        nome: 'Santa Fé do Araguaia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3546603,
        nome: 'Santa Fé do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2209203,
        nome: 'Santa Filomena',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2612554,
        nome: 'Santa Filomena',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2109759,
        nome: 'Santa Filomena do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3546702,
        nome: 'Santa Gertrudes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4123501,
        nome: 'Santa Helena',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4215554,
        nome: 'Santa Helena',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2109809,
        nome: 'Santa Helena',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2513307,
        nome: 'Santa Helena',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5219308,
        nome: 'Santa Helena de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3157658,
        nome: 'Santa Helena de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2927903,
        nome: 'Santa Inês',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4123600,
        nome: 'Santa Inês',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2513356,
        nome: 'Santa Inês',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2109908,
        nome: 'Santa Inês',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3546801,
        nome: 'Santa Isabel',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5219357,
        nome: 'Santa Isabel',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4123709,
        nome: 'Santa Isabel do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1303601,
        nome: 'Santa Isabel do Rio Negro',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4123808,
        nome: 'Santa Izabel do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1506500,
        nome: 'Santa Izabel do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3157708,
        nome: 'Santa Juliana',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3204500,
        nome: 'Santa Leopoldina',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3546900,
        nome: 'Santa Lúcia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4123824,
        nome: 'Santa Lúcia',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2209302,
        nome: 'Santa Luz',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2110005,
        nome: 'Santa Luzia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2928059,
        nome: 'Santa Luzia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3157807,
        nome: 'Santa Luzia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2513406,
        nome: 'Santa Luzia',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1100296,
        nome: 'Santa Luzia D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2806305,
        nome: 'Santa Luzia do Itanhy',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2707909,
        nome: 'Santa Luzia do Norte',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1506559,
        nome: 'Santa Luzia do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2110039,
        nome: 'Santa Luzia do Paruá',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3157906,
        nome: 'Santa Margarida',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4316972,
        nome: 'Santa Margarida do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4316907,
        nome: 'Santa Maria',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2409332,
        nome: 'Santa Maria',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2612604,
        nome: 'Santa Maria da Boa Vista',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3547007,
        nome: 'Santa Maria da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2928109,
        nome: 'Santa Maria da Vitória',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1506583,
        nome: 'Santa Maria das Barreiras',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3158003,
        nome: 'Santa Maria de Itabira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3204559,
        nome: 'Santa Maria de Jetibá',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2612703,
        nome: 'Santa Maria do Cambucá',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4316956,
        nome: 'Santa Maria do Herval',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4123857,
        nome: 'Santa Maria do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1506609,
        nome: 'Santa Maria do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3158102,
        nome: 'Santa Maria do Salto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3158201,
        nome: 'Santa Maria do Suaçuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1718881,
        nome: 'Santa Maria do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3304607,
        nome: 'Santa Maria Madalena',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4123907,
        nome: 'Santa Mariana',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3547106,
        nome: 'Santa Mercedes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4123956,
        nome: 'Santa Mônica',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2312205,
        nome: 'Santa Quitéria',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2110104,
        nome: 'Santa Quitéria do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2110203,
        nome: 'Santa Rita',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2513703,
        nome: 'Santa Rita',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3547403,
        nome: 'Santa Rita d\'Oeste',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3159209,
        nome: 'Santa Rita de Caldas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2928406,
        nome: 'Santa Rita de Cássia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3159407,
        nome: 'Santa Rita de Ibitipoca',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3159308,
        nome: 'Santa Rita de Jacutinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3159357,
        nome: 'Santa Rita de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5219407,
        nome: 'Santa Rita do Araguaia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3159506,
        nome: 'Santa Rita do Itueto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5219456,
        nome: 'Santa Rita do Novo Destino',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5007554,
        nome: 'Santa Rita do Pardo',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3547502,
        nome: 'Santa Rita do Passa Quatro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3159605,
        nome: 'Santa Rita do Sapucaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1718899,
        nome: 'Santa Rita do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 5107768,
        nome: 'Santa Rita do Trivelato',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4317202,
        nome: 'Santa Rosa',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3159704,
        nome: 'Santa Rosa da Serra',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5219506,
        nome: 'Santa Rosa de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4215604,
        nome: 'Santa Rosa de Lima',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2806503,
        nome: 'Santa Rosa de Lima',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3547601,
        nome: 'Santa Rosa de Viterbo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2209377,
        nome: 'Santa Rosa do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1200435,
        nome: 'Santa Rosa do Purus',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4215653,
        nome: 'Santa Rosa do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1718907,
        nome: 'Santa Rosa do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3547650,
        nome: 'Santa Salete',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3204609,
        nome: 'Santa Teresa',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2928505,
        nome: 'Santa Teresinha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2513802,
        nome: 'Santa Teresinha',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4317251,
        nome: 'Santa Tereza',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5219605,
        nome: 'Santa Tereza de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4124020,
        nome: 'Santa Tereza do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1719004,
        nome: 'Santa Tereza do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4215679,
        nome: 'Santa Terezinha',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5107776,
        nome: 'Santa Terezinha',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2612802,
        nome: 'Santa Terezinha',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5219704,
        nome: 'Santa Terezinha de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4124053,
        nome: 'Santa Terezinha de Itaipu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4215687,
        nome: 'Santa Terezinha do Progresso',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1720002,
        nome: 'Santa Terezinha do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3159803,
        nome: 'Santa Vitória',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4317301,
        nome: 'Santa Vitória do Palmar',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2928000,
        nome: 'Santaluz',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2928208,
        nome: 'Santana',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1600600,
        nome: 'Santana',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 4317004,
        nome: 'Santana da Boa Vista',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3547205,
        nome: 'Santana da Ponte Pensa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3158300,
        nome: 'Santana da Vargem',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3158409,
        nome: 'Santana de Cataguases',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2513505,
        nome: 'Santana de Mangueira',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3547304,
        nome: 'Santana de Parnaíba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3158508,
        nome: 'Santana de Pirapama',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2312007,
        nome: 'Santana do Acaraú',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1506708,
        nome: 'Santana do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2312106,
        nome: 'Santana do Cariri',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3158607,
        nome: 'Santana do Deserto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3158706,
        nome: 'Santana do Garambéu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2708006,
        nome: 'Santana do Ipanema',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4124004,
        nome: 'Santana do Itararé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3158805,
        nome: 'Santana do Jacaré',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3158904,
        nome: 'Santana do Manhuaçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2110237,
        nome: 'Santana do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2411403,
        nome: 'Santana do Matos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2708105,
        nome: 'Santana do Mundaú',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3158953,
        nome: 'Santana do Paraíso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2209351,
        nome: 'Santana do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3159001,
        nome: 'Santana do Riacho',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2806404,
        nome: 'Santana do São Francisco',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2411429,
        nome: 'Santana do Seridó',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2513604,
        nome: 'Santana dos Garrotes',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3159100,
        nome: 'Santana dos Montes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2928307,
        nome: 'Santanópolis',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1506807,
        nome: 'Santarém',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1506906,
        nome: 'Santarém Novo',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4317400,
        nome: 'Santiago',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4215695,
        nome: 'Santiago do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5107263,
        nome: 'Santo Afonso',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2928604,
        nome: 'Santo Amaro',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4215703,
        nome: 'Santo Amaro da Imperatriz',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2806602,
        nome: 'Santo Amaro das Brotas',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2110278,
        nome: 'Santo Amaro do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3547700,
        nome: 'Santo Anastácio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3547809,
        nome: 'Santo André',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2513851,
        nome: 'Santo André',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4317509,
        nome: 'Santo Ângelo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2411502,
        nome: 'Santo Antônio',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3547908,
        nome: 'Santo Antônio da Alegria',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5219712,
        nome: 'Santo Antônio da Barra',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4317608,
        nome: 'Santo Antônio da Patrulha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4124103,
        nome: 'Santo Antônio da Platina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4317707,
        nome: 'Santo Antônio das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5219738,
        nome: 'Santo Antônio de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2928703,
        nome: 'Santo Antônio de Jesus',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2209401,
        nome: 'Santo Antônio de Lisboa',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3304706,
        nome: 'Santo Antônio de Pádua',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3548005,
        nome: 'Santo Antônio de Posse',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3159902,
        nome: 'Santo Antônio do Amparo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3548054,
        nome: 'Santo Antônio do Aracanguá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3160009,
        nome: 'Santo Antônio do Aventureiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4124202,
        nome: 'Santo Antônio do Caiuá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5219753,
        nome: 'Santo Antônio do Descoberto',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3160108,
        nome: 'Santo Antônio do Grama',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1303700,
        nome: 'Santo Antônio do Içá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3160207,
        nome: 'Santo Antônio do Itambé',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3160306,
        nome: 'Santo Antônio do Jacinto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3548104,
        nome: 'Santo Antônio do Jardim',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5107792,
        nome: 'Santo Antônio do Leste',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5107800,
        nome: 'Santo Antônio do Leverger',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3160405,
        nome: 'Santo Antônio do Monte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4317558,
        nome: 'Santo Antônio do Palma',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4124301,
        nome: 'Santo Antônio do Paraíso',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3548203,
        nome: 'Santo Antônio do Pinhal',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4317756,
        nome: 'Santo Antônio do Planalto',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3160454,
        nome: 'Santo Antônio do Retiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3160504,
        nome: 'Santo Antônio do Rio Abaixo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4124400,
        nome: 'Santo Antônio do Sudoeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 1507003,
        nome: 'Santo Antônio do Tauá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2110302,
        nome: 'Santo Antônio dos Lopes',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2209450,
        nome: 'Santo Antônio dos Milagres',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4317806,
        nome: 'Santo Augusto',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4317905,
        nome: 'Santo Cristo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2928802,
        nome: 'Santo Estêvão',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3548302,
        nome: 'Santo Expedito',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4317954,
        nome: 'Santo Expedito do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3160603,
        nome: 'Santo Hipólito',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4124509,
        nome: 'Santo Inácio',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2209500,
        nome: 'Santo Inácio do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3548401,
        nome: 'Santópolis do Aguapeí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3548500,
        nome: 'Santos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3160702,
        nome: 'Santos Dumont',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2312304,
        nome: 'São Benedito',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2110401,
        nome: 'São Benedito do Rio Preto',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2612901,
        nome: 'São Benedito do Sul',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2513927,
        nome: 'São Bentinho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2513901,
        nome: 'São Bento',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2110500,
        nome: 'São Bento',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3160801,
        nome: 'São Bento Abade',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2411601,
        nome: 'São Bento do Norte',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3548609,
        nome: 'São Bento do Sapucaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4215802,
        nome: 'São Bento do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1720101,
        nome: 'São Bento do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2411700,
        nome: 'São Bento do Trairí',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2613008,
        nome: 'São Bento do Una',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4215752,
        nome: 'São Bernardino',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2110609,
        nome: 'São Bernardo',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3548708,
        nome: 'São Bernardo do Campo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4215901,
        nome: 'São Bonifácio',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4318002,
        nome: 'São Borja',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2708204,
        nome: 'São Brás',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3160900,
        nome: 'São Brás do Suaçuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2209559,
        nome: 'São Braz do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2613107,
        nome: 'São Caetano',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1507102,
        nome: 'São Caetano de Odivelas',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3548807,
        nome: 'São Caetano do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3548906,
        nome: 'São Carlos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4216008,
        nome: 'São Carlos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4124608,
        nome: 'São Carlos do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2806701,
        nome: 'São Cristóvão',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4216057,
        nome: 'São Cristovão do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2928901,
        nome: 'São Desidério',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2928950,
        nome: 'São Domingos',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4216107,
        nome: 'São Domingos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2513968,
        nome: 'São Domingos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2806800,
        nome: 'São Domingos',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 5219803,
        nome: 'São Domingos',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3160959,
        nome: 'São Domingos das Dores',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1507151,
        nome: 'São Domingos do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2110658,
        nome: 'São Domingos do Azeitão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1507201,
        nome: 'São Domingos do Capim',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2513943,
        nome: 'São Domingos do Cariri',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2110708,
        nome: 'São Domingos do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3204658,
        nome: 'São Domingos do Norte',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3161007,
        nome: 'São Domingos do Prata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4318051,
        nome: 'São Domingos do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2929107,
        nome: 'São Felipe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1101484,
        nome: 'São Felipe D\'Oeste',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2929008,
        nome: 'São Félix',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2110807,
        nome: 'São Félix de Balsas',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3161056,
        nome: 'São Félix de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5107859,
        nome: 'São Félix do Araguaia',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2929057,
        nome: 'São Félix do Coribe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2209609,
        nome: 'São Félix do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1720150,
        nome: 'São Félix do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1507300,
        nome: 'São Félix do Xingu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2411809,
        nome: 'São Fernando',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3304805,
        nome: 'São Fidélis',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3549003,
        nome: 'São Francisco',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2513984,
        nome: 'São Francisco',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2806909,
        nome: 'São Francisco',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3161106,
        nome: 'São Francisco',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4318101,
        nome: 'São Francisco de Assis',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2209658,
        nome: 'São Francisco de Assis do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5219902,
        nome: 'São Francisco de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3304755,
        nome: 'São Francisco de Itabapoana',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4318200,
        nome: 'São Francisco de Paula',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3161205,
        nome: 'São Francisco de Paula',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3161304,
        nome: 'São Francisco de Sales',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2110856,
        nome: 'São Francisco do Brejão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2929206,
        nome: 'São Francisco do Conde',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3161403,
        nome: 'São Francisco do Glória',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1101492,
        nome: 'São Francisco do Guaporé',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2110906,
        nome: 'São Francisco do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2411908,
        nome: 'São Francisco do Oeste',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1507409,
        nome: 'São Francisco do Pará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2209708,
        nome: 'São Francisco do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4216206,
        nome: 'São Francisco do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4318309,
        nome: 'São Gabriel',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2929255,
        nome: 'São Gabriel',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1303809,
        nome: 'São Gabriel da Cachoeira',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3204708,
        nome: 'São Gabriel da Palha',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5007695,
        nome: 'São Gabriel do Oeste',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3161502,
        nome: 'São Geraldo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3161601,
        nome: 'São Geraldo da Piedade',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1507458,
        nome: 'São Geraldo do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3161650,
        nome: 'São Geraldo do Baixio',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3304904,
        nome: 'São Gonçalo',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3161700,
        nome: 'São Gonçalo do Abaeté',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2412005,
        nome: 'São Gonçalo do Amarante',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2312403,
        nome: 'São Gonçalo do Amarante',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2209757,
        nome: 'São Gonçalo do Gurguéia',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3161809,
        nome: 'São Gonçalo do Pará',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2209807,
        nome: 'São Gonçalo do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3161908,
        nome: 'São Gonçalo do Rio Abaixo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3125507,
        nome: 'São Gonçalo do Rio Preto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3162005,
        nome: 'São Gonçalo do Sapucaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2929305,
        nome: 'São Gonçalo dos Campos',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3162104,
        nome: 'São Gotardo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4318408,
        nome: 'São Jerônimo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4124707,
        nome: 'São Jerônimo da Serra',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4124806,
        nome: 'São João',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2613206,
        nome: 'São João',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2111003,
        nome: 'São João Batista',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4216305,
        nome: 'São João Batista',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3162203,
        nome: 'São João Batista do Glória',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5220009,
        nome: 'São João d\'Aliança',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1400506,
        nome: 'São João da Baliza',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 3305000,
        nome: 'São João da Barra',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3549102,
        nome: 'São João da Boa Vista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2209856,
        nome: 'São João da Canabrava',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2209872,
        nome: 'São João da Fronteira',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3162252,
        nome: 'São João da Lagoa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3162302,
        nome: 'São João da Mata',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5220058,
        nome: 'São João da Paraúna',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1507466,
        nome: 'São João da Ponta',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3162401,
        nome: 'São João da Ponte',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2209906,
        nome: 'São João da Serra',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4318424,
        nome: 'São João da Urtiga',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2209955,
        nome: 'São João da Varjota',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3549201,
        nome: 'São João das Duas Pontes',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3162450,
        nome: 'São João das Missões',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3549250,
        nome: 'São João de Iracema',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3305109,
        nome: 'São João de Meriti',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 1507474,
        nome: 'São João de Pirabas',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3162500,
        nome: 'São João del Rei',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1507508,
        nome: 'São João do Araguaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2209971,
        nome: 'São João do Arraial',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4124905,
        nome: 'São João do Caiuá',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2514008,
        nome: 'São João do Cariri',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2111029,
        nome: 'São João do Carú',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4216354,
        nome: 'São João do Itaperiú',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4125001,
        nome: 'São João do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2312502,
        nome: 'São João do Jaguaribe',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3162559,
        nome: 'São João do Manhuaçu',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3162575,
        nome: 'São João do Manteninha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4216255,
        nome: 'São João do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3162609,
        nome: 'São João do Oriente',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3162658,
        nome: 'São João do Pacuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3162708,
        nome: 'São João do Paraíso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2111052,
        nome: 'São João do Paraíso',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3549300,
        nome: 'São João do Pau d\'Alho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2210003,
        nome: 'São João do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4318432,
        nome: 'São João do Polêsine',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2500700,
        nome: 'São João do Rio do Peixe',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2412104,
        nome: 'São João do Sabugi',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2111078,
        nome: 'São João do Soter',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4216404,
        nome: 'São João do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2514107,
        nome: 'São João do Tigre',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4125100,
        nome: 'São João do Triunfo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2111102,
        nome: 'São João dos Patos',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3162807,
        nome: 'São João Evangelista',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3162906,
        nome: 'São João Nepomuceno',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4216503,
        nome: 'São Joaquim',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3549409,
        nome: 'São Joaquim da Barra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3162922,
        nome: 'São Joaquim de Bicas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2613305,
        nome: 'São Joaquim do Monte',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4318440,
        nome: 'São Jorge',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4125209,
        nome: 'São Jorge d\'Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4125308,
        nome: 'São Jorge do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4125357,
        nome: 'São Jorge do Patrocínio',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4216602,
        nome: 'São José',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3162948,
        nome: 'São José da Barra',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3549508,
        nome: 'São José da Bela Vista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4125407,
        nome: 'São José da Boa Vista',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2613404,
        nome: 'São José da Coroa Grande',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2514206,
        nome: 'São José da Lagoa Tapada',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2708303,
        nome: 'São José da Laje',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3162955,
        nome: 'São José da Lapa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3163003,
        nome: 'São José da Safira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2708402,
        nome: 'São José da Tapera',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3163102,
        nome: 'São José da Varginha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2929354,
        nome: 'São José da Vitória',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4318457,
        nome: 'São José das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4125456,
        nome: 'São José das Palmeiras',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2514305,
        nome: 'São José de Caiana',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2514404,
        nome: 'São José de Espinharas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2412203,
        nome: 'São José de Mipibu',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2514503,
        nome: 'São José de Piranhas',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2514552,
        nome: 'São José de Princesa',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2111201,
        nome: 'São José de Ribamar',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3305133,
        nome: 'São José de Ubá',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3163201,
        nome: 'São José do Alegre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3549607,
        nome: 'São José do Barreiro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2613503,
        nome: 'São José do Belmonte',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2514602,
        nome: 'São José do Bonfim',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2514651,
        nome: 'São José do Brejo do Cruz',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3204807,
        nome: 'São José do Calçado',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2412302,
        nome: 'São José do Campestre',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4216701,
        nome: 'São José do Cedro',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4216800,
        nome: 'São José do Cerrito',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2210052,
        nome: 'São José do Divino',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3163300,
        nome: 'São José do Divino',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2613602,
        nome: 'São José do Egito',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3163409,
        nome: 'São José do Goiabal',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4318465,
        nome: 'São José do Herval',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4318481,
        nome: 'São José do Hortêncio',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4318499,
        nome: 'São José do Inhacorá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2929370,
        nome: 'São José do Jacuípe',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3163508,
        nome: 'São José do Jacuri',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3163607,
        nome: 'São José do Mantimento',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4318507,
        nome: 'São José do Norte',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4318606,
        nome: 'São José do Ouro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2210102,
        nome: 'São José do Peixe',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2210201,
        nome: 'São José do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5107297,
        nome: 'São José do Povo',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5107305,
        nome: 'São José do Rio Claro',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3549706,
        nome: 'São José do Rio Pardo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3549805,
        nome: 'São José do Rio Preto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2514701,
        nome: 'São José do Sabugi',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2412401,
        nome: 'São José do Seridó',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4318614,
        nome: 'São José do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3305158,
        nome: 'São José do Vale do Rio Preto',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5107354,
        nome: 'São José do Xingu',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4318622,
        nome: 'São José dos Ausentes',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2111250,
        nome: 'São José dos Basílios',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3549904,
        nome: 'São José dos Campos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2514800,
        nome: 'São José dos Cordeiros',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4125506,
        nome: 'São José dos Pinhais',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 5107107,
        nome: 'São José dos Quatro Marcos',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2514453,
        nome: 'São José dos Ramos',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2210300,
        nome: 'São Julião',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4318705,
        nome: 'São Leopoldo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3163706,
        nome: 'São Lourenço',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2613701,
        nome: 'São Lourenço da Mata',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3549953,
        nome: 'São Lourenço da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4216909,
        nome: 'São Lourenço do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2210359,
        nome: 'São Lourenço do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4318804,
        nome: 'São Lourenço do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4217006,
        nome: 'São Ludgero',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2111300,
        nome: 'São Luís',
        capital: 's',
        sigla_uf: 'MA'
    },
    {
        id: 5220108,
        nome: 'São Luís de Montes Belos',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2312601,
        nome: 'São Luís do Curu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2210375,
        nome: 'São Luis do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2708501,
        nome: 'São Luís do Quitunde',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2111409,
        nome: 'São Luís Gonzaga do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1400605,
        nome: 'São Luiz',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 5220157,
        nome: 'São Luiz do Norte',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3550001,
        nome: 'São Luiz do Paraitinga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4318903,
        nome: 'São Luiz Gonzaga',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2514909,
        nome: 'São Mamede',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4125555,
        nome: 'São Manoel do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3550100,
        nome: 'São Manuel',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4319000,
        nome: 'São Marcos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4217105,
        nome: 'São Martinho',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4319109,
        nome: 'São Martinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4319125,
        nome: 'São Martinho da Serra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3204906,
        nome: 'São Mateus',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2111508,
        nome: 'São Mateus do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4125605,
        nome: 'São Mateus do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2412500,
        nome: 'São Miguel',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3550209,
        nome: 'São Miguel Arcanjo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2210383,
        nome: 'São Miguel da Baixa Grande',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4217154,
        nome: 'São Miguel da Boa Vista',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2929404,
        nome: 'São Miguel das Matas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4319158,
        nome: 'São Miguel das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2515005,
        nome: 'São Miguel de Taipu',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2807006,
        nome: 'São Miguel do Aleixo',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3163805,
        nome: 'São Miguel do Anta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5220207,
        nome: 'São Miguel do Araguaia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2210391,
        nome: 'São Miguel do Fidalgo',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2412559,
        nome: 'São Miguel do Gostoso',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1507607,
        nome: 'São Miguel do Guamá',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1100320,
        nome: 'São Miguel do Guaporé',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4125704,
        nome: 'São Miguel do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4217204,
        nome: 'São Miguel do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5220264,
        nome: 'São Miguel do Passa Quatro',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2210409,
        nome: 'São Miguel do Tapuio',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1720200,
        nome: 'São Miguel do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2708600,
        nome: 'São Miguel dos Campos',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2708709,
        nome: 'São Miguel dos Milagres',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4319208,
        nome: 'São Nicolau',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5220280,
        nome: 'São Patrício',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3550308,
        nome: 'São Paulo',
        capital: 's',
        sigla_uf: 'SP'
    },
    {
        id: 4319307,
        nome: 'São Paulo das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1303908,
        nome: 'São Paulo de Olivença',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2412609,
        nome: 'São Paulo do Potengi',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2412708,
        nome: 'São Pedro',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3550407,
        nome: 'São Pedro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2111532,
        nome: 'São Pedro da Água Branca',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3305208,
        nome: 'São Pedro da Aldeia',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5107404,
        nome: 'São Pedro da Cipa',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4319356,
        nome: 'São Pedro da Serra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3163904,
        nome: 'São Pedro da União',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4319364,
        nome: 'São Pedro das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4217253,
        nome: 'São Pedro de Alcântara',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4319372,
        nome: 'São Pedro do Butiá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4125753,
        nome: 'São Pedro do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4125803,
        nome: 'São Pedro do Ivaí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4125902,
        nome: 'São Pedro do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2210508,
        nome: 'São Pedro do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3164100,
        nome: 'São Pedro do Suaçuí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4319406,
        nome: 'São Pedro do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3550506,
        nome: 'São Pedro do Turvo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2111573,
        nome: 'São Pedro dos Crentes',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3164001,
        nome: 'São Pedro dos Ferros',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2412807,
        nome: 'São Rafael',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2111607,
        nome: 'São Raimundo das Mangabeiras',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2111631,
        nome: 'São Raimundo do Doca Bezerra',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2210607,
        nome: 'São Raimundo Nonato',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2111672,
        nome: 'São Roberto',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3164209,
        nome: 'São Romão',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3550605,
        nome: 'São Roque',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3164308,
        nome: 'São Roque de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3204955,
        nome: 'São Roque do Canaã',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 1720259,
        nome: 'São Salvador do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3550704,
        nome: 'São Sebastião',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2708808,
        nome: 'São Sebastião',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4126009,
        nome: 'São Sebastião da Amoreira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3164407,
        nome: 'São Sebastião da Bela Vista',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1507706,
        nome: 'São Sebastião da Boa Vista',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3550803,
        nome: 'São Sebastião da Grama',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3164431,
        nome: 'São Sebastião da Vargem Alegre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2515104,
        nome: 'São Sebastião de Lagoa de Roça',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3305307,
        nome: 'São Sebastião do Alto',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3164472,
        nome: 'São Sebastião do Anta',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4319505,
        nome: 'São Sebastião do Caí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3164506,
        nome: 'São Sebastião do Maranhão',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3164605,
        nome: 'São Sebastião do Oeste',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3164704,
        nome: 'São Sebastião do Paraíso',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2929503,
        nome: 'São Sebastião do Passé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3164803,
        nome: 'São Sebastião do Rio Preto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3164902,
        nome: 'São Sebastião do Rio Verde',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1720309,
        nome: 'São Sebastião do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1303957,
        nome: 'São Sebastião do Uatumã',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2515203,
        nome: 'São Sebastião do Umbuzeiro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4319604,
        nome: 'São Sepé',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3550902,
        nome: 'São Simão',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5220405,
        nome: 'São Simão',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3165206,
        nome: 'São Thomé das Letras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3165008,
        nome: 'São Tiago',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3165107,
        nome: 'São Tomás de Aquino',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4126108,
        nome: 'São Tomé',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2412906,
        nome: 'São Tomé',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4319703,
        nome: 'São Valentim',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4319711,
        nome: 'São Valentim do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1720499,
        nome: 'São Valério',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4319737,
        nome: 'São Valério do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4319752,
        nome: 'São Vendelino',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3551009,
        nome: 'São Vicente',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2413003,
        nome: 'São Vicente',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3165305,
        nome: 'São Vicente de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2515401,
        nome: 'São Vicente do Seridó',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4319802,
        nome: 'São Vicente do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2613800,
        nome: 'São Vicente Ferrer',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2111706,
        nome: 'São Vicente Ferrer',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2515302,
        nome: 'Sapé',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2929602,
        nome: 'Sapeaçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5107875,
        nome: 'Sapezal',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4319901,
        nome: 'Sapiranga',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4126207,
        nome: 'Sapopema',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3165404,
        nome: 'Sapucaí-Mirim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1507755,
        nome: 'Sapucaia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 3305406,
        nome: 'Sapucaia',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4320008,
        nome: 'Sapucaia do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3305505,
        nome: 'Saquarema',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4126256,
        nome: 'Sarandi',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4320107,
        nome: 'Sarandi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3551108,
        nome: 'Sarapuí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3165503,
        nome: 'Sardoá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3551207,
        nome: 'Sarutaiá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3165537,
        nome: 'Sarzedo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2929701,
        nome: 'Sátiro Dias',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2708907,
        nome: 'Satuba',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2111722,
        nome: 'Satubinha',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2929750,
        nome: 'Saubara',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4126272,
        nome: 'Saudade do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4217303,
        nome: 'Saudades',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2929800,
        nome: 'Saúde',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4217402,
        nome: 'Schroeder',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2929909,
        nome: 'Seabra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4217501,
        nome: 'Seara',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3551306,
        nome: 'Sebastianópolis do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2210623,
        nome: 'Sebastião Barros',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2930006,
        nome: 'Sebastião Laranjeiras',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2210631,
        nome: 'Sebastião Leal',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4320206,
        nome: 'Seberi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4320230,
        nome: 'Sede Nova',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4320263,
        nome: 'Segredo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4320305,
        nome: 'Selbach',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5007802,
        nome: 'Selvíria',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3165560,
        nome: 'Sem-Peixe',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1200500,
        nome: 'Sena Madureira',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2111748,
        nome: 'Senador Alexandre Costa',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3165578,
        nome: 'Senador Amaral',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5220454,
        nome: 'Senador Canedo',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3165602,
        nome: 'Senador Cortes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2413102,
        nome: 'Senador Elói de Souza',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3165701,
        nome: 'Senador Firmino',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2413201,
        nome: 'Senador Georgino Avelino',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1200450,
        nome: 'Senador Guiomard',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 3165800,
        nome: 'Senador José Bento',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1507805,
        nome: 'Senador José Porfírio',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2111763,
        nome: 'Senador La Rocque',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3165909,
        nome: 'Senador Modestino Gonçalves',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2312700,
        nome: 'Senador Pompeu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2708956,
        nome: 'Senador Rui Palmeira',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2312809,
        nome: 'Senador Sá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4320321,
        nome: 'Senador Salgado Filho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4126306,
        nome: 'Sengés',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2930105,
        nome: 'Senhor do Bonfim',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3166006,
        nome: 'Senhora de Oliveira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3166105,
        nome: 'Senhora do Porto',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3166204,
        nome: 'Senhora dos Remédios',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4320354,
        nome: 'Sentinela do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2930204,
        nome: 'Sento Sé',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4320404,
        nome: 'Serafina Corrêa',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3166303,
        nome: 'Sericita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1101500,
        nome: 'Seringueiras',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4320453,
        nome: 'Sério',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3166402,
        nome: 'Seritinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3305554,
        nome: 'Seropédica',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3205002,
        nome: 'Serra',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 4217550,
        nome: 'Serra Alta',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3551405,
        nome: 'Serra Azul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3166501,
        nome: 'Serra Azul de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2515500,
        nome: 'Serra Branca',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2410306,
        nome: 'Serra Caiada',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2515609,
        nome: 'Serra da Raiz',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3166600,
        nome: 'Serra da Saudade',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2413300,
        nome: 'Serra de São Bento',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2413359,
        nome: 'Serra do Mel',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1600055,
        nome: 'Serra do Navio',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 2930154,
        nome: 'Serra do Ramalho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3166808,
        nome: 'Serra do Salitre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3166709,
        nome: 'Serra dos Aimorés',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2930303,
        nome: 'Serra Dourada',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2515708,
        nome: 'Serra Grande',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3551603,
        nome: 'Serra Negra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2413409,
        nome: 'Serra Negra do Norte',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 5107883,
        nome: 'Serra Nova Dourada',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2930402,
        nome: 'Serra Preta',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2515807,
        nome: 'Serra Redonda',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2613909,
        nome: 'Serra Talhada',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3551504,
        nome: 'Serrana',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3166907,
        nome: 'Serrania',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2111789,
        nome: 'Serrano do Maranhão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5220504,
        nome: 'Serranópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3166956,
        nome: 'Serranópolis de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4126355,
        nome: 'Serranópolis do Iguaçu',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3167004,
        nome: 'Serranos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2515906,
        nome: 'Serraria',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2413508,
        nome: 'Serrinha',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2930501,
        nome: 'Serrinha',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2413557,
        nome: 'Serrinha dos Pintos',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2614006,
        nome: 'Serrita',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3167103,
        nome: 'Serro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2930600,
        nome: 'Serrolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4126405,
        nome: 'Sertaneja',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2614105,
        nome: 'Sertânia',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4126504,
        nome: 'Sertanópolis',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4320503,
        nome: 'Sertão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4320552,
        nome: 'Sertão Santana',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3551702,
        nome: 'Sertãozinho',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2515930,
        nome: 'Sertãozinho',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3551801,
        nome: 'Sete Barras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4320578,
        nome: 'Sete de Setembro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3167202,
        nome: 'Sete Lagoas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5007703,
        nome: 'Sete Quedas',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3165552,
        nome: 'Setubinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4320602,
        nome: 'Severiano de Almeida',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2413607,
        nome: 'Severiano Melo',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3551900,
        nome: 'Severínia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4217600,
        nome: 'Siderópolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5007901,
        nome: 'Sidrolândia',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2210656,
        nome: 'Sigefredo Pacheco',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3305604,
        nome: 'Silva Jardim',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 5220603,
        nome: 'Silvânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1720655,
        nome: 'Silvanópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4320651,
        nome: 'Silveira Martins',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3167301,
        nome: 'Silveirânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3552007,
        nome: 'Silveiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1304005,
        nome: 'Silves',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 3167400,
        nome: 'Silvianópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2807105,
        nome: 'Simão Dias',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 3167509,
        nome: 'Simão Pereira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2210706,
        nome: 'Simões',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2930709,
        nome: 'Simões Filho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 5220686,
        nome: 'Simolândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3167608,
        nome: 'Simonésia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2210805,
        nome: 'Simplício Mendes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4320677,
        nome: 'Sinimbu',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5107909,
        nome: 'Sinop',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4126603,
        nome: 'Siqueira Campos',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2614204,
        nome: 'Sirinhaém',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2807204,
        nome: 'Siriri',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 5220702,
        nome: 'Sítio d\'Abadia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2930758,
        nome: 'Sítio do Mato',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2930766,
        nome: 'Sítio do Quinto',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2111805,
        nome: 'Sítio Novo',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2413706,
        nome: 'Sítio Novo',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 1720804,
        nome: 'Sítio Novo do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2930774,
        nome: 'Sobradinho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4320701,
        nome: 'Sobradinho',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2515971,
        nome: 'Sobrado',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2312908,
        nome: 'Sobral',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3167707,
        nome: 'Sobrália',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3552106,
        nome: 'Socorro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2210904,
        nome: 'Socorro do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2516003,
        nome: 'Solânea',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2516102,
        nome: 'Soledade',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4320800,
        nome: 'Soledade',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3167806,
        nome: 'Soledade de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2614402,
        nome: 'Solidão',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2313005,
        nome: 'Solonópole',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4217709,
        nome: 'Sombrio',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5007935,
        nome: 'Sonora',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3205010,
        nome: 'Sooretama',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3552205,
        nome: 'Sorocaba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5107925,
        nome: 'Sorriso',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2516151,
        nome: 'Sossêgo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1507904,
        nome: 'Soure',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2516201,
        nome: 'Sousa',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2930808,
        nome: 'Souto Soares',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1720853,
        nome: 'Sucupira',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2111904,
        nome: 'Sucupira do Norte',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2111953,
        nome: 'Sucupira do Riachão',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3552304,
        nome: 'Sud Mennucci',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4217758,
        nome: 'Sul Brasil',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4126652,
        nome: 'Sulina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3552403,
        nome: 'Sumaré',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2516300,
        nome: 'Sumé',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3305703,
        nome: 'Sumidouro',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2614501,
        nome: 'Surubim',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2210938,
        nome: 'Sussuapara',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3552551,
        nome: 'Suzanápolis',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3552502,
        nome: 'Suzano',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4320859,
        nome: 'Tabaí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5107941,
        nome: 'Tabaporã',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3552601,
        nome: 'Tabapuã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3552700,
        nome: 'Tabatinga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1304062,
        nome: 'Tabatinga',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2614600,
        nome: 'Tabira',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3552809,
        nome: 'Taboão da Serra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2930907,
        nome: 'Tabocas do Brejo Velho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2413805,
        nome: 'Taboleiro Grande',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3167905,
        nome: 'Tabuleiro',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2313104,
        nome: 'Tabuleiro do Norte',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2614709,
        nome: 'Tacaimbó',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2614808,
        nome: 'Tacaratu',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3552908,
        nome: 'Taciba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2516409,
        nome: 'Tacima',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 5007950,
        nome: 'Tacuru',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3553005,
        nome: 'Taguaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1720903,
        nome: 'Taguatinga',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3553104,
        nome: 'Taiaçu',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1507953,
        nome: 'Tailândia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4217808,
        nome: 'Taió',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3168002,
        nome: 'Taiobeiras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1720937,
        nome: 'Taipas do Tocantins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2413904,
        nome: 'Taipu',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3553203,
        nome: 'Taiúva',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1720978,
        nome: 'Talismã',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2614857,
        nome: 'Tamandaré',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4126678,
        nome: 'Tamarana',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3553302,
        nome: 'Tambaú',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4126702,
        nome: 'Tamboara',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2313203,
        nome: 'Tamboril',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2210953,
        nome: 'Tamboril do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3553401,
        nome: 'Tanabi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2414001,
        nome: 'Tangará',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4217907,
        nome: 'Tangará',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5107958,
        nome: 'Tangará da Serra',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3305752,
        nome: 'Tanguá',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2931004,
        nome: 'Tanhaçu',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2709004,
        nome: 'Tanque d\'Arca',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 2210979,
        nome: 'Tanque do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2931053,
        nome: 'Tanque Novo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2931103,
        nome: 'Tanquinho',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3168051,
        nome: 'Taparuba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1304104,
        nome: 'Tapauá',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4126801,
        nome: 'Tapejara',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4320909,
        nome: 'Tapejara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4321006,
        nome: 'Tapera',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2931202,
        nome: 'Taperoá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2516508,
        nome: 'Taperoá',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4321105,
        nome: 'Tapes',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4126900,
        nome: 'Tapira',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3168101,
        nome: 'Tapira',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3168200,
        nome: 'Tapiraí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3553500,
        nome: 'Tapiraí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2931301,
        nome: 'Tapiramutá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3553609,
        nome: 'Tapiratiba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5108006,
        nome: 'Tapurah',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4321204,
        nome: 'Taquara',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3168309,
        nome: 'Taquaraçu de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3553658,
        nome: 'Taquaral',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5221007,
        nome: 'Taquaral de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2709103,
        nome: 'Taquarana',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 4321303,
        nome: 'Taquari',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3553708,
        nome: 'Taquaritinga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2615003,
        nome: 'Taquaritinga do Norte',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3553807,
        nome: 'Taquarituba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3553856,
        nome: 'Taquarivaí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4321329,
        nome: 'Taquaruçu do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5007976,
        nome: 'Taquarussu',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3553906,
        nome: 'Tarabai',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1200609,
        nome: 'Tarauacá',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 2313252,
        nome: 'Tarrafas',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1600709,
        nome: 'Tartarugalzinho',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 3553955,
        nome: 'Tarumã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3168408,
        nome: 'Tarumirim',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2112001,
        nome: 'Tasso Fragoso',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3554003,
        nome: 'Tatuí',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2313302,
        nome: 'Tauá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3554102,
        nome: 'Taubaté',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4321352,
        nome: 'Tavares',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2516607,
        nome: 'Tavares',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1304203,
        nome: 'Tefé',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2516706,
        nome: 'Teixeira',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2931350,
        nome: 'Teixeira de Freitas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4127007,
        nome: 'Teixeira Soares',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3168507,
        nome: 'Teixeiras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1101559,
        nome: 'Teixeirópolis',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2313351,
        nome: 'Tejuçuoca',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3554201,
        nome: 'Tejupá',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4127106,
        nome: 'Telêmaco Borba',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2807303,
        nome: 'Telha',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2414100,
        nome: 'Tenente Ananias',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2414159,
        nome: 'Tenente Laurentino Cruz',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4321402,
        nome: 'Tenente Portela',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2516755,
        nome: 'Tenório',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2931400,
        nome: 'Teodoro Sampaio',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3554300,
        nome: 'Teodoro Sampaio',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2931509,
        nome: 'Teofilândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3168606,
        nome: 'Teófilo Otoni',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2931608,
        nome: 'Teolândia',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2709152,
        nome: 'Teotônio Vilela',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 5008008,
        nome: 'Terenos',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 2211001,
        nome: 'Teresina',
        capital: 's',
        sigla_uf: 'PI'
    },
    {
        id: 5221080,
        nome: 'Teresina de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3305802,
        nome: 'Teresópolis',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2615102,
        nome: 'Terezinha',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5221197,
        nome: 'Terezópolis de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 1507961,
        nome: 'Terra Alta',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4127205,
        nome: 'Terra Boa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4321436,
        nome: 'Terra de Areia',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2931707,
        nome: 'Terra Nova',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2615201,
        nome: 'Terra Nova',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5108055,
        nome: 'Terra Nova do Norte',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4127304,
        nome: 'Terra Rica',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4127403,
        nome: 'Terra Roxa',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3554409,
        nome: 'Terra Roxa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1507979,
        nome: 'Terra Santa',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5108105,
        nome: 'Tesouro',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4321451,
        nome: 'Teutônia',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1101609,
        nome: 'Theobroma',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 2313401,
        nome: 'Tianguá',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4127502,
        nome: 'Tibagi',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2411056,
        nome: 'Tibau',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2414209,
        nome: 'Tibau do Sul',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3554508,
        nome: 'Tietê',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4217956,
        nome: 'Tigrinhos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4218004,
        nome: 'Tijucas',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4127601,
        nome: 'Tijucas do Sul',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2615300,
        nome: 'Timbaúba',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2414308,
        nome: 'Timbaúba dos Batistas',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4218103,
        nome: 'Timbé do Sul',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2112100,
        nome: 'Timbiras',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4218202,
        nome: 'Timbó',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4218251,
        nome: 'Timbó Grande',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3554607,
        nome: 'Timburi',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2112209,
        nome: 'Timon',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3168705,
        nome: 'Timóteo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4321469,
        nome: 'Tio Hugo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3168804,
        nome: 'Tiradentes',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4321477,
        nome: 'Tiradentes do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3168903,
        nome: 'Tiros',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2807402,
        nome: 'Tobias Barreto',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 1721109,
        nome: 'Tocantínia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1721208,
        nome: 'Tocantinópolis',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 3169000,
        nome: 'Tocantins',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3169059,
        nome: 'Tocos do Moji',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3169109,
        nome: 'Toledo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4127700,
        nome: 'Toledo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2807501,
        nome: 'Tomar do Geru',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 4127809,
        nome: 'Tomazina',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3169208,
        nome: 'Tombos',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1508001,
        nome: 'Tomé-Açu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 1304237,
        nome: 'Tonantins',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2615409,
        nome: 'Toritama',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5108204,
        nome: 'Torixoréu',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 4321493,
        nome: 'Toropi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3554656,
        nome: 'Torre de Pedra',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4321501,
        nome: 'Torres',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3554706,
        nome: 'Torrinha',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2414407,
        nome: 'Touros',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 3554755,
        nome: 'Trabiju',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1508035,
        nome: 'Tracuateua',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2615508,
        nome: 'Tracunhaém',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2709202,
        nome: 'Traipu',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 1508050,
        nome: 'Trairão',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2313500,
        nome: 'Trairi',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3305901,
        nome: 'Trajano de Moraes',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4321600,
        nome: 'Tramandaí',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4321626,
        nome: 'Travesseiro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2931806,
        nome: 'Tremedal',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3554805,
        nome: 'Tremembé',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4321634,
        nome: 'Três Arroios',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4218301,
        nome: 'Três Barras',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4127858,
        nome: 'Três Barras do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4321667,
        nome: 'Três Cachoeiras',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3169307,
        nome: 'Três Corações',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4321709,
        nome: 'Três Coroas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4321808,
        nome: 'Três de Maio',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4321832,
        nome: 'Três Forquilhas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3554904,
        nome: 'Três Fronteiras',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5008305,
        nome: 'Três Lagoas',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 3169356,
        nome: 'Três Marias',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4321857,
        nome: 'Três Palmeiras',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4321907,
        nome: 'Três Passos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3169406,
        nome: 'Três Pontas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5221304,
        nome: 'Três Ranchos',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3306008,
        nome: 'Três Rios',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 4218350,
        nome: 'Treviso',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4218400,
        nome: 'Treze de Maio',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4218509,
        nome: 'Treze Tílias',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5221403,
        nome: 'Trindade',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2615607,
        nome: 'Trindade',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4321956,
        nome: 'Trindade do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322004,
        nome: 'Triunfo',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2516805,
        nome: 'Triunfo',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2615706,
        nome: 'Triunfo',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2414456,
        nome: 'Triunfo Potiguar',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2112233,
        nome: 'Trizidela do Vale',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5221452,
        nome: 'Trombas',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4218608,
        nome: 'Trombudo Central',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4218707,
        nome: 'Tubarão',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2931905,
        nome: 'Tucano',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1508084,
        nome: 'Tucumã',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4322103,
        nome: 'Tucunduva',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 1508100,
        nome: 'Tucuruí',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2112274,
        nome: 'Tufilândia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3554953,
        nome: 'Tuiuti',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3169505,
        nome: 'Tumiritinga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4218756,
        nome: 'Tunápolis',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4322152,
        nome: 'Tunas',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4127882,
        nome: 'Tunas do Paraná',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4127908,
        nome: 'Tuneiras do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2112308,
        nome: 'Tuntum',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3555000,
        nome: 'Tupã',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3169604,
        nome: 'Tupaciguara',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2615805,
        nome: 'Tupanatinga',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4322186,
        nome: 'Tupanci do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322202,
        nome: 'Tupanciretã',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322251,
        nome: 'Tupandi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322301,
        nome: 'Tuparendi',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2615904,
        nome: 'Tuparetama',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4127957,
        nome: 'Tupãssi',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3555109,
        nome: 'Tupi Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 1721257,
        nome: 'Tupirama',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 1721307,
        nome: 'Tupiratins',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2112407,
        nome: 'Turiaçu',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 2112456,
        nome: 'Turilândia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3555208,
        nome: 'Turiúba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3555307,
        nome: 'Turmalina',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3169703,
        nome: 'Turmalina',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4322327,
        nome: 'Turuçu',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2313559,
        nome: 'Tururu',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 5221502,
        nome: 'Turvânia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5221551,
        nome: 'Turvelândia',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4127965,
        nome: 'Turvo',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4218806,
        nome: 'Turvo',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3169802,
        nome: 'Turvolândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2112506,
        nome: 'Tutóia',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1304260,
        nome: 'Uarini',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2932002,
        nome: 'Uauá',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3169901,
        nome: 'Ubá',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3170008,
        nome: 'Ubaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2932101,
        nome: 'Ubaíra',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2932200,
        nome: 'Ubaitaba',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2313609,
        nome: 'Ubajara',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3170057,
        nome: 'Ubaporanga',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3555356,
        nome: 'Ubarana',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2932309,
        nome: 'Ubatã',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3555406,
        nome: 'Ubatuba',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3170107,
        nome: 'Uberaba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3170206,
        nome: 'Uberlândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3555505,
        nome: 'Ubirajara',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4128005,
        nome: 'Ubiratã',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4322343,
        nome: 'Ubiretama',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3555604,
        nome: 'Uchoa',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2932408,
        nome: 'Uibaí',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 1400704,
        nome: 'Uiramutã',
        capital: 'n',
        sigla_uf: 'RR'
    },
    {
        id: 5221577,
        nome: 'Uirapuru',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2516904,
        nome: 'Uiraúna',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1508126,
        nome: 'Ulianópolis',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2313708,
        nome: 'Umari',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2414506,
        nome: 'Umarizal',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2807600,
        nome: 'Umbaúba',
        capital: 'n',
        sigla_uf: 'SE'
    },
    {
        id: 2932457,
        nome: 'Umburanas',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3170305,
        nome: 'Umburatiba',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2517001,
        nome: 'Umbuzeiro',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2313757,
        nome: 'Umirim',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4128104,
        nome: 'Umuarama',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2932507,
        nome: 'Una',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3170404,
        nome: 'Unaí',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2211100,
        nome: 'União',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4322350,
        nome: 'União da Serra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4128203,
        nome: 'União da Vitória',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3170438,
        nome: 'União de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4218855,
        nome: 'União do Oeste',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5108303,
        nome: 'União do Sul',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2709301,
        nome: 'União dos Palmares',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3555703,
        nome: 'União Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4128302,
        nome: 'Uniflor',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4322376,
        nome: 'Unistalda',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2414605,
        nome: 'Upanema',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4128401,
        nome: 'Uraí',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2932606,
        nome: 'Urandi',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3555802,
        nome: 'Urânia',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2112605,
        nome: 'Urbano Santos',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3555901,
        nome: 'Uru',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5221601,
        nome: 'Uruaçu',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5221700,
        nome: 'Uruana',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3170479,
        nome: 'Uruana de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1508159,
        nome: 'Uruará',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4218905,
        nome: 'Urubici',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2313807,
        nome: 'Uruburetama',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3170503,
        nome: 'Urucânia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1304302,
        nome: 'Urucará',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 2932705,
        nome: 'Uruçuca',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2211209,
        nome: 'Uruçuí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3170529,
        nome: 'Urucuia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1304401,
        nome: 'Urucurituba',
        capital: 'n',
        sigla_uf: 'AM'
    },
    {
        id: 4322400,
        nome: 'Uruguaiana',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2313906,
        nome: 'Uruoca',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 1101708,
        nome: 'Urupá',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4218954,
        nome: 'Urupema',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3556008,
        nome: 'Urupês',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4219002,
        nome: 'Urussanga',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 5221809,
        nome: 'Urutaí',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2932804,
        nome: 'Utinga',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4322509,
        nome: 'Vacaria',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5108352,
        nome: 'Vale de São Domingos',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 1101757,
        nome: 'Vale do Anari',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 1101807,
        nome: 'Vale do Paraíso',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 4322533,
        nome: 'Vale do Sol',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322541,
        nome: 'Vale Real',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322525,
        nome: 'Vale Verde',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2932903,
        nome: 'Valença',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3306107,
        nome: 'Valença',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2211308,
        nome: 'Valença do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 2933000,
        nome: 'Valente',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3556107,
        nome: 'Valentim Gentil',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3556206,
        nome: 'Valinhos',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3556305,
        nome: 'Valparaíso',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 5221858,
        nome: 'Valparaíso de Goiás',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 4322558,
        nome: 'Vanini',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4219101,
        nome: 'Vargeão',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4219150,
        nome: 'Vargem',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3556354,
        nome: 'Vargem',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3170578,
        nome: 'Vargem Alegre',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3205036,
        nome: 'Vargem Alta',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3170602,
        nome: 'Vargem Bonita',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4219176,
        nome: 'Vargem Bonita',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2112704,
        nome: 'Vargem Grande',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3170651,
        nome: 'Vargem Grande do Rio Pardo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3556404,
        nome: 'Vargem Grande do Sul',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3556453,
        nome: 'Vargem Grande Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3170701,
        nome: 'Varginha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 5221908,
        nome: 'Varjão',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 3170750,
        nome: 'Varjão de Minas',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2313955,
        nome: 'Varjota',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 3306156,
        nome: 'Varre-Sai',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 2414704,
        nome: 'Várzea',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2517100,
        nome: 'Várzea',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 2314003,
        nome: 'Várzea Alegre',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 2211357,
        nome: 'Várzea Branca',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 3170800,
        nome: 'Várzea da Palma',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2933059,
        nome: 'Várzea da Roça',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2933109,
        nome: 'Várzea do Poço',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2211407,
        nome: 'Várzea Grande',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 5108402,
        nome: 'Várzea Grande',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2933158,
        nome: 'Várzea Nova',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3556503,
        nome: 'Várzea Paulista',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2933174,
        nome: 'Varzedo',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3170909,
        nome: 'Varzelândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3306206,
        nome: 'Vassouras',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3171006,
        nome: 'Vazante',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4322608,
        nome: 'Venâncio Aires',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3205069,
        nome: 'Venda Nova do Imigrante',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2414753,
        nome: 'Venha-Ver',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4128534,
        nome: 'Ventania',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2616001,
        nome: 'Venturosa',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 5108501,
        nome: 'Vera',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 2414803,
        nome: 'Vera Cruz',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2933208,
        nome: 'Vera Cruz',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4322707,
        nome: 'Vera Cruz',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3556602,
        nome: 'Vera Cruz',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4128559,
        nome: 'Vera Cruz do Oeste',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2211506,
        nome: 'Vera Mendes',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4322806,
        nome: 'Veranópolis',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2616100,
        nome: 'Verdejante',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3171030,
        nome: 'Verdelândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4128609,
        nome: 'Verê',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2933257,
        nome: 'Vereda',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3171071,
        nome: 'Veredinha',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3171105,
        nome: 'Veríssimo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3171154,
        nome: 'Vermelho Novo',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2616183,
        nome: 'Vertente do Lério',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 2616209,
        nome: 'Vertentes',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 3171204,
        nome: 'Vespasiano',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4322855,
        nome: 'Vespasiano Corrêa',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4322905,
        nome: 'Viadutos',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4323002,
        nome: 'Viamão',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3205101,
        nome: 'Viana',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 2112803,
        nome: 'Viana',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 5222005,
        nome: 'Vianópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2616308,
        nome: 'Vicência',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 4323101,
        nome: 'Vicente Dutra',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 5008404,
        nome: 'Vicentina',
        capital: 'n',
        sigla_uf: 'MS'
    },
    {
        id: 5222054,
        nome: 'Vicentinópolis',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2414902,
        nome: 'Viçosa',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 2709400,
        nome: 'Viçosa',
        capital: 'n',
        sigla_uf: 'AL'
    },
    {
        id: 3171303,
        nome: 'Viçosa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2314102,
        nome: 'Viçosa do Ceará',
        capital: 'n',
        sigla_uf: 'CE'
    },
    {
        id: 4323200,
        nome: 'Victor Graeff',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4219200,
        nome: 'Vidal Ramos',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4219309,
        nome: 'Videira',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3171402,
        nome: 'Vieiras',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 2517209,
        nome: 'Vieirópolis',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 1508209,
        nome: 'Vigia',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 5105507,
        nome: 'Vila Bela da Santíssima Trindade',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 5222203,
        nome: 'Vila Boa',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 2415008,
        nome: 'Vila Flor',
        capital: 'n',
        sigla_uf: 'RN'
    },
    {
        id: 4323309,
        nome: 'Vila Flores',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4323358,
        nome: 'Vila Lângaro',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4323408,
        nome: 'Vila Maria',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2211605,
        nome: 'Vila Nova do Piauí',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 4323457,
        nome: 'Vila Nova do Sul',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2112852,
        nome: 'Vila Nova dos Martírios',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3205150,
        nome: 'Vila Pavão',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 5222302,
        nome: 'Vila Propício',
        capital: 'n',
        sigla_uf: 'GO'
    },
    {
        id: 5108600,
        nome: 'Vila Rica',
        capital: 'n',
        sigla_uf: 'MT'
    },
    {
        id: 3205176,
        nome: 'Vila Valério',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 3205200,
        nome: 'Vila Velha',
        capital: 'n',
        sigla_uf: 'ES'
    },
    {
        id: 1100304,
        nome: 'Vilhena',
        capital: 'n',
        sigla_uf: 'RO'
    },
    {
        id: 3556701,
        nome: 'Vinhedo',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3556800,
        nome: 'Viradouro',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3171600,
        nome: 'Virgem da Lapa',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3171709,
        nome: 'Virgínia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3171808,
        nome: 'Virginópolis',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3171907,
        nome: 'Virgolândia',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4128658,
        nome: 'Virmond',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 3172004,
        nome: 'Visconde do Rio Branco',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 1508308,
        nome: 'Viseu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4323507,
        nome: 'Vista Alegre',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 3556909,
        nome: 'Vista Alegre do Alto',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 4323606,
        nome: 'Vista Alegre do Prata',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4323705,
        nome: 'Vista Gaúcha',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2505501,
        nome: 'Vista Serrana',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 4219358,
        nome: 'Vitor Meireles',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 3205309,
        nome: 'Vitória',
        capital: 's',
        sigla_uf: 'ES'
    },
    {
        id: 3556958,
        nome: 'Vitória Brasil',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2933307,
        nome: 'Vitória da Conquista',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4323754,
        nome: 'Vitória das Missões',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 2616407,
        nome: 'Vitória de Santo Antão',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1600808,
        nome: 'Vitória do Jari',
        capital: 'n',
        sigla_uf: 'AP'
    },
    {
        id: 2112902,
        nome: 'Vitória do Mearim',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 1508357,
        nome: 'Vitória do Xingu',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 4128708,
        nome: 'Vitorino',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2113009,
        nome: 'Vitorino Freire',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 3172103,
        nome: 'Volta Grande',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 3306305,
        nome: 'Volta Redonda',
        capital: 'n',
        sigla_uf: 'RJ'
    },
    {
        id: 3557006,
        nome: 'Votorantim',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 3557105,
        nome: 'Votuporanga',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2933406,
        nome: 'Wagner',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2211704,
        nome: 'Wall Ferraz',
        capital: 'n',
        sigla_uf: 'PI'
    },
    {
        id: 1722081,
        nome: 'Wanderlândia',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 2933455,
        nome: 'Wanderley',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 3172202,
        nome: 'Wenceslau Braz',
        capital: 'n',
        sigla_uf: 'MG'
    },
    {
        id: 4128500,
        nome: 'Wenceslau Braz',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 2933505,
        nome: 'Wenceslau Guimarães',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 4323770,
        nome: 'Westfália',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4219408,
        nome: 'Witmarsum',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1722107,
        nome: 'Xambioá',
        capital: 'n',
        sigla_uf: 'TO'
    },
    {
        id: 4128807,
        nome: 'Xambrê',
        capital: 'n',
        sigla_uf: 'PR'
    },
    {
        id: 4323804,
        nome: 'Xangri-lá',
        capital: 'n',
        sigla_uf: 'RS'
    },
    {
        id: 4219507,
        nome: 'Xanxerê',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 1200708,
        nome: 'Xapuri',
        capital: 'n',
        sigla_uf: 'AC'
    },
    {
        id: 4219606,
        nome: 'Xavantina',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 4219705,
        nome: 'Xaxim',
        capital: 'n',
        sigla_uf: 'SC'
    },
    {
        id: 2616506,
        nome: 'Xexéu',
        capital: 'n',
        sigla_uf: 'PE'
    },
    {
        id: 1508407,
        nome: 'Xinguara',
        capital: 'n',
        sigla_uf: 'PA'
    },
    {
        id: 2933604,
        nome: 'Xique-Xique',
        capital: 'n',
        sigla_uf: 'BA'
    },
    {
        id: 2517407,
        nome: 'Zabelê',
        capital: 'n',
        sigla_uf: 'PB'
    },
    {
        id: 3557154,
        nome: 'Zacarias',
        capital: 'n',
        sigla_uf: 'SP'
    },
    {
        id: 2114007,
        nome: 'Zé Doca',
        capital: 'n',
        sigla_uf: 'MA'
    },
    {
        id: 4219853,
        nome: 'Zortéa',
        capital: 'n',
        sigla_uf: 'SC'
    }
]
