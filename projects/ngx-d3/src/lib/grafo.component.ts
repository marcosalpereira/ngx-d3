import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { GraphNode } from './data.model';
import { D3graphService } from './d3graph.service';

import { Subscription } from 'rxjs';
import { FilterManager, Filter, FilterType, KeyWord, SearchType } from './filter-manager';

@Component({
  selector: 'd3-grafo',
  templateUrl: './grafo.component.html',
  styleUrls: ['./grafo.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [D3graphService]
})
export class GrafoComponent implements OnChanges, OnInit, OnDestroy {
  @Input() data: any;
  @Output() nodeClick = new EventEmitter<GraphNode>();

  appendMode = true;
  showNodeText = true;
  showEdgeText = false;

  errorMessage: string;

  nodeclickedSubscription: Subscription;

  filterManager: FilterManager;

  keyWords: KeyWord[] = [];

  constructor(public d3s: D3graphService) {
    this.filterManager = d3s.filterManager;
  }

  public get filterTypes(): FilterType[] {
    return this.filterManager.filterTypes;
  }

  public get filters(): Filter[] {
    return this.filterManager.filters;
  }

  ngOnInit(): void {
    this.nodeclickedSubscription = this.d3s.nodeclicked$
      .subscribe(node => this.nodeClick.emit(node));
  }

  ngOnDestroy(): void {
    this.nodeclickedSubscription.unsubscribe();
  }

  onFixAll(fix: boolean) {
    this.d3s.setNodesFixedStatus(fix);
  }

  onSearchTypeChange(searchType: SearchType) {
    this.keyWords = this.filterManager.keyWords.filter(
      k => (
              (searchType === 'edge' && k.fromEdge) ||
              (searchType === 'node' && k.fromNode)
            ));
  }

  onKeyWordChange(keyWord: KeyWord) {

  }

  onChangeShowNodeText(v: boolean) {
    this.showNodeText = v;
    this.d3s.setShowNodeText(v);
  }

  onChangeShowEdgeText(v: boolean) {
    this.showEdgeText = v;
    this.d3s.setShowEdgeText(v);
  }

  onFilterChange(filter: Filter, event) {
    this.filterManager.filterStatusChanged(filter, event.target.checked);
  }

  onRemoveFilterClick(filter: Filter) {
    this.filterManager.removeFilter(filter);
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const dataChange = changes.data;
    if (dataChange) {
      const graph = dataChange.currentValue;
      this.d3s.loadGraph(graph, {
        appendMode: this.appendMode,
        showNodeText: this.showNodeText,
        showEdgeText: this.showEdgeText
      });
    }
  }

  onAddFilterClick(
          searchType: SearchType, filterType: FilterType, keyWord: KeyWord,
          inverse, operation: string, searchedValue: string) {
    this.errorMessage = '';
    const property = keyWord.name;
    const filter = this.filterManager.findFilter(filterType, searchType, property, searchedValue);
    if (filter) {
      this.message('Filtro já existe!');
      return;
    }
    if (!this.filterManager.addFilter(filterType, searchType, property, inverse.checked, operation, searchedValue)) {
      this.message('Nenhum item encontrado');
    }
  }

  message(msg: string) {
    this.errorMessage = msg;
  }

}

