export const log = (o) => {
    console.log(JSON.parse(JSON.stringify(o)));
};

export function getOrCreate<T>(map: Map<string, T>, keyWord: string, notExistsFn: () => any): T {
    if (!map.has(keyWord)) {
        map.set(keyWord, notExistsFn());
    }
    return map.get(keyWord);
}

export function setAddAll(set: Set<any>, items: Set<any> | any[]) {
    items.forEach(item => set.add(item));
}



