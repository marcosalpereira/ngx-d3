
export type GraphNode = INode | any;
export type GraphEdge = IEdge | any;

interface INode {
    id: string;
    name?: string;
    title?: string;
    imageUrl?: string;
    faicon: string;
}

interface IEdge {
    source: number;
    target: number;
    name?: string;
}

export interface GraphItems {
    nodes: GraphNode[];
    edges: GraphEdge[];
}

export interface Graph {
    id: string;
    name: string;
    items: GraphItems;
}

