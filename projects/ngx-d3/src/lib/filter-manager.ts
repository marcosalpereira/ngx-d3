
import { D3graphService, SubGraph } from './d3graph.service';
import { Graph, GraphEdge, GraphNode, GraphItems } from './data.model';
import { setAddAll, getOrCreate } from './util';
import { InvokeMethodExpr } from '@angular/compiler';

const IGNORED_PROPERTIES = new Set([
    'size', 'id', 'icon', 'imageUrl',
    'color', 'x', 'y', 'vy', 'vx', 'fx',
    'fy', 'fixed', 'index', 'collapsed',
    'isLeaf']);

export type SearchType = 'node' | 'edge';
export type FilterType = 'marcar' | 'marcar descendentes' | 'esconder' | 'recolher';

export interface Filter {
    nodesLength: number;
    affectedItems: SubGraph;
    sourceNodes: GraphNode[];
    active: boolean;
    filterType: FilterType;
    searchType: SearchType;
    property: string;
    operation: string;
    inverse: boolean;
    searchedValue: string;
}

export interface KeyWord {
    name: string;
    values: Set<string>;
    fromNode: boolean;
    fromEdge: boolean;
}

export class FilterManager {
    filterTypes: FilterType[] = ['marcar', 'marcar descendentes', 'esconder', 'recolher'];
    filters: Filter[] = [];
    keyWords: KeyWord[];

    private dataKeys: Map<string, KeyWord>;

    public operations = ['=~', '=', '>', '<', '!=', '>=', '<='];

    constructor(
        private d3s: D3graphService) { }

    filterStatusChanged(filter: Filter, active: boolean) {
        filter.active = active;
        this.d3s.applyFilter(filter);
    }

    public removeFilter(filter: Filter) {
        filter.active = false;
        this.d3s.applyFilter(filter);
        this.filters = this.filters.filter(f => f !== filter);
    }

    public findFilter(filterType: FilterType, searchType: SearchType, property: string, searchedValue: string): Filter {
        return this.filters.find(f =>
            f.filterType === filterType
            && f.searchType === searchType
            && f.property === property
            && f.searchedValue === searchedValue);
    }

    public addFilter(
        filterType: FilterType, searchType: SearchType,
        property: string, inverse: boolean, operation: string, searchedValue: string): boolean {
        if (filterType === 'esconder') {
            return this.searchAndHide(searchType, property, inverse, operation, searchedValue);
        } else if (filterType === 'recolher') {
            return this.searchAndCollapse(searchType, property, inverse, operation, searchedValue);
        } else if (filterType === 'marcar') {
            return this.searchAndMark(searchType, property, inverse, operation, searchedValue);
        } else if (filterType === 'marcar descendentes') {
            return this.searchAndMarkDescending(searchType, property, inverse, operation, searchedValue);
        }
    }

    private collectEdgesFromNode(node: GraphNode, collecteds: GraphEdge[]) {

        // edges from node (no cycles)
        const edgesFromNode = this.d3s.edges
            .filter(e => e.source.id === node.id);

        collecteds.push(...edgesFromNode);
        edgesFromNode.forEach(e => {
            if (!collecteds.find(collected => collected.source.id === e.target.id)) {
                this.collectEdgesFromNode(e.target, collecteds);
            }
        });
    }

    public findOrAddFilter(
                filterType: FilterType, searchType: SearchType,
                property: string, inverse: boolean, operation: string, searchedValue: string,
                applyFn?: (created: boolean, f: Filter) => void): Filter {
        let filter = this.findFilter(filterType, searchType, property, searchedValue);
        let created;
        if (filter) {
            created = false;
        } else {
            filter = this._addFilter(filterType, searchType, property, inverse, operation, searchedValue, []);
            created = true;
        }
        if (applyFn) {
            applyFn(created, filter);
        }
        return filter;
    }

    private getCollapseAffectedItems(inverse: boolean, sourceNodes: GraphNode[]): SubGraph {
        const affectedNodes = new Set<GraphNode>();
        const affectedEdges = new Set<GraphEdge>();

        for (const node of sourceNodes) {
            const sg = this.d3s.getSubGraph(node);
            setAddAll(affectedNodes, sg.nodes);
            setAddAll(affectedEdges, sg.edges);
        }

        const ret = {
            nodes: affectedNodes,
            edges: affectedEdges
        };
        if (inverse) {
            return this.invert(ret);
        }
        return ret;
    }

    invert(sg: SubGraph): SubGraph {
        return {
            nodes: new Set(this.d3s.nodes.filter(node => !sg.nodes.has(node))),
            edges: new Set(this.d3s.edges.filter(edge => !sg.edges.has(edge)))
        };
    }

    private getHideAffectedItems(inverse: boolean, sourceNodes: GraphNode[]): SubGraph {
        const affectedNodes = new Set<GraphNode>();
        const affectedEdges = new Set<GraphEdge>();

        for (const node of sourceNodes) {
            const sg = this.d3s.getSubGraph(node);
            setAddAll(affectedNodes, sg.nodes);
            affectedNodes.add(node);

            setAddAll(affectedEdges, sg.edges);
            setAddAll(affectedEdges, this.d3s.getEdgesPointingThisNode(node));
        }

        const ret = {
            nodes: affectedNodes,
            edges: affectedEdges
        };
        if (inverse) {
            return this.invert(ret);
        }
        return ret;

    }

    private getOrCreateKey(dataKeys: Map<string, KeyWord>, name: string, value: string): KeyWord {
        const keyWord: KeyWord = getOrCreate(dataKeys, name, () => {
            return { name, values: new Set() };
        });
        keyWord.values.add(name);
        return keyWord;
    }

    public collectKeys(appendMode) {
        const dataKeys = this.dataKeys || new Map();
        if (!appendMode || !dataKeys) {
            dataKeys.clear();
        }

        this.indexItems(dataKeys, this.d3s.nodes, (keyWord) => keyWord.fromNode = true);
        this.indexItems(dataKeys, this.d3s.edges, (keyWord) => keyWord.fromEdge = true);

        this.dataKeys = dataKeys;
        this.keyWords = [...dataKeys.values()];

    }

    private indexItems(dataKeys: Map<string, KeyWord>, items: any[], callbackfn: (keyWord: KeyWord) => void) {
        items.forEach(item => {
            for (const name in item) {
                if (!IGNORED_PROPERTIES.has(name) && item.hasOwnProperty(name)) {
                    const value = item[name];
                    const keyWord = this.getOrCreateKey(dataKeys, name, value);
                    callbackfn(keyWord);
                }
            }
        });
    }

    private findNodes(nodes: GraphNode[], property: string, operation: string, searched: string | RegExp): GraphNode[] {
        return nodes.filter(node => {
            return this.compliesWith(node, property, operation, searched);
        });
    }

    private compliesWith(obj: any, property: string, operation: string, searched: string | RegExp) {
        let objValue = obj[property];

        // se a propriedade nao foi encontrada, verficar se está pesquisando pelo 'name'
        // como ela é opcional, nesse caso, usar o 'id'
        if (!objValue && property === 'name') {
            objValue = obj.id;
        }

        if (!objValue) { return false; }

        // se for um Node
        if (objValue.id) {
            // pegar o nome dele
            objValue = this.nodeName(objValue);
        }

        let complies: boolean;
        if (Number.isInteger(objValue)) {
            complies = this.numberCompliesWith(operation, objValue, +searched);
        } else {
            complies = this.stringCompliesWith(operation, objValue.toLowerCase(), searched);
        }
        return complies;
    }

    stringCompliesWith(operation: string, objValue: string, searched: string | RegExp): boolean {
        if (operation === '=') { return objValue === searched; }
        if (operation === '>') { return objValue > searched; }
        if (operation === '<') { return objValue < searched; }

        if (operation === '!=') { return objValue !== searched; }
        if (operation === '>=') { return objValue >= searched; }
        if (operation === '<=') { return objValue <= searched; }

        if (operation === '=~') { return objValue.match(searched) != null; }
        return false;
    }

    numberCompliesWith(operation: string, objValue: number, searched: number | RegExp): boolean {
        if (operation === '=') { return objValue === searched; }
        if (operation === '>') { return objValue > searched; }
        if (operation === '<') { return objValue < searched; }

        if (operation === '!=') { return objValue !== searched; }
        if (operation === '>=') { return objValue >= searched; }
        if (operation === '<=') { return objValue <= searched; }
        return false;
    }

    private findNodesByEdges(
            edges: GraphEdge[], property: string,
            operation: string, searched: string | RegExp): GraphNode[] {
        const edgesFiltered = edges.filter(edge => {
            return this.compliesWith(edge, property, operation, searched);
        });
        return edgesFiltered.map(edge => edge.source);
    }

    private nodeName(node: any) {
        return node.name || node.id;
    }

    public collapseNode(node: GraphNode) {
        const filter = this.findOrAddFilter('recolher', 'node', 'name', false, '=', this.nodeName(node),
            (created, f) => {
                if (created) {
                    f.sourceNodes = [node];
                    f.affectedItems = this.getCollapseAffectedItems(false, f.sourceNodes);
                } else {
                    f.active = !node.collapsed;
                }
            });
        this.d3s.applyFilter(filter);
    }

    private searchAndCollapse(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {
        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const filter = this._addFilter('recolher', searchType, property, inverse, operation, searchedValue, sourceNodes);
        filter.affectedItems = this.getCollapseAffectedItems(inverse, sourceNodes);
        this.d3s.applyFilter(filter);
        return true;
    }

    _addFilter(
            filterType: FilterType, searchType: SearchType, property: string, inverse: boolean,
            operation: string, searchedValue: string, sourceNodes: GraphNode[]): Filter {
        const affectedItems: SubGraph = null;
        const filter: Filter = {
            nodesLength: this.d3s.nodes.length,
            affectedItems,
            sourceNodes,
            active: true,
            filterType,
            searchType,
            property,
            inverse,
            operation,
            searchedValue,
        };
        this.filters.push(filter);
        return filter;
    }

    private searchAndHide(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {
        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const filter = this._addFilter('esconder', searchType, property, inverse, operation, searchedValue, sourceNodes);
        filter.affectedItems = this.getHideAffectedItems(inverse, sourceNodes);

        this.d3s.applyFilter(filter);
        return true;
    }

    private searchAndMarkDescending(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {

        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const filter = this._addFilter('marcar descendentes', searchType, property, inverse, operation, searchedValue, sourceNodes);
        // TODO create getSubNodes
        filter.affectedItems = this.getHideAffectedItems(inverse, sourceNodes);
        filter.affectedItems.edges = new Set();

        this.d3s.applyFilter(filter);
        return true;
    }

    private searchAndMark(searchType: SearchType, property: string, inverse: boolean, operation: string, searchedValue: string) {

        const sourceNodes: GraphNode[] = this.searchNodes(searchType, property, operation, searchedValue);
        if (sourceNodes.length === 0) {
            return false;
        }
        const filter = this._addFilter('marcar', searchType, property, inverse, operation, searchedValue, sourceNodes);

        let affectedNodes = new Set(sourceNodes);
        if (inverse) {
            affectedNodes = new Set(this.d3s.nodes.filter(node => !affectedNodes.has(node)));
        }
        filter.affectedItems = {
            edges: new Set(),
            nodes: affectedNodes
        };

        this.d3s.applyFilter(filter);
        return true;
    }

    private searchNodes(
            searchType: SearchType, property: string, operation: string, searchedValue: string): GraphNode[] {
        const searched: string | RegExp =
            operation === '=~'
                ? new RegExp(searchedValue, 'i')
                : searchedValue.toLowerCase();

        let nodes: GraphNode[];
        if (searchType === 'node') {
            nodes = this.findNodes(this.d3s.nodes, property, operation, searched);
        }
        else if (searchType === 'edge') {
            nodes = this.findNodesByEdges(this.d3s.edges, property, operation, searched);
        }
        return nodes;
    }



}
