import { NgModule } from '@angular/core';
import { GrafoComponent } from './grafo.component';

@NgModule({
  declarations: [
    GrafoComponent
  ],
  exports: [GrafoComponent],
  imports: [],
  providers: [],
})
export class NgxD3Module { }
