import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Graph, GraphEdge, GraphNode, GraphItems } from './data.model';
import { FONT_AWESOME } from './fa';
import { Subject } from 'rxjs';
import { Filter, FilterManager } from './filter-manager';
import { log } from './util';

export interface SubGraph {
    nodes: Set<GraphNode>;
    edges: Set<GraphEdge>;
}

const forceProperties = {
    center: {
        x: 0.5,
        y: 0.5
    },
    charge: {
        strength: -30,
        distanceMin: 1,
        distanceMax: 2000
    },
    collide: {
        strength: .7,
        iterations: 1,
        radius: 5
    },
    forceX: {
        enabled: true,
        strength: .1,
        x: .5
    },
    forceY: {
        enabled: true,
        strength: .1,
        y: .5
    },
    link: {
        distance: 30,
        iterations: 1
    }
};

export interface GraphOptions {
    appendMode: boolean;
    showNodeText: boolean;
    showEdgeText: boolean;
}

@Injectable()
export class D3graphService {
    private nodeClickedSubject = new Subject<GraphNode>();
    nodeclicked$ = this.nodeClickedSubject.asObservable();

    public nodes: GraphNode[] = [];
    public edges: GraphEdge[] = [];

    private simulation: any;
    private svg: any;
    private link: any;
    private linkText: any;
    private node: any;

    private width: number;
    private height: number;

    private nodeClicked: GraphNode;

    zoomHandle: any;
    circle: any;

    showEdgeText = false;
    showNodeText = true;

    filterManager: FilterManager = new FilterManager(this);

    constructor() { }

    public setShowNodeText(v) {
        this.showNodeText = v;
        this.draw();
    }

    public setShowEdgeText(v) {
        this.showEdgeText = v;
        this.draw();
    }

    public setNodesFixedStatus(fix: boolean) {
        if (fix) {
            this.nodes.forEach(node => this.fixNode(node));
        } else {
            this.nodes.forEach(node => this.unfixNode(node));
        }
    }

    private onNodeClicked(node: GraphNode) {
        d3.event.stopPropagation();
        if (node.collapsed || !this.isLeaf(node)) {
            this.filterManager.collapseNode(node);
        } else {
            this.nodeClicked = node;
            this.nodeClickedSubject.next(node);
        }
    }

    public loadGraph(graph: Graph, options: GraphOptions) {
        this.showNodeText = options.showNodeText;
        this.showEdgeText = options.showEdgeText;

        graph.items.edges.forEach(e => e.id = e.source + e.target);

        if (this.nodeClicked && options.appendMode) {
            const nodes = this.nodes.slice();
            const edges = this.edges.slice();

            nodes.push(...graph.items.nodes.filter(n => n.id !== this.nodeClicked.id));
            edges.push(...graph.items.edges);

            this.nodes = nodes;
            this.edges = edges;

            this.nodeClicked.isLeaf = nodes.length > 0;
        } else {
            this.nodes = graph.items.nodes;
            this.edges = graph.items.edges;
        }
        this.filterManager.filters = [];
        this.filterManager.collectKeys(options.appendMode);
        this.draw();

    }

    public applyFilter(filter: Filter) {
        const affectedNodes = filter.affectedItems.nodes;
        const affectedEdges = filter.affectedItems.edges;

        if (filter.filterType === 'recolher' || filter.filterType === 'esconder') {
            if (filter.active) {
                this.nodes = this.nodes.filter(node => !affectedNodes.has(node));
                this.edges = this.edges.filter(edge => !affectedEdges.has(edge));
            } else {
                const nodes = this.nodes.slice();
                const edges = this.edges.slice();
                nodes.push(...affectedNodes);
                edges.push(...affectedEdges);
                this.nodes = nodes;
                this.edges = edges;
            }

            if (filter.filterType === 'recolher') {
                filter.sourceNodes.forEach(node => node.collapsed = filter.active);
            }

        } else if (filter.filterType === 'marcar' || filter.filterType === 'marcar descendentes') {
            affectedNodes.forEach(node => node.marked = filter.active);
        }

        this.draw();
    }

    private fixNode(node: GraphNode): GraphNode {
        if (node.x || node.cx) {
            node.fx = node.x;
            node.fy = node.y;
            node.fixed = true;
        }
        return node;
    }


    private unfixNode(node: GraphNode): GraphNode {
        node.vx = null;
        node.vy = null;
        node.fixed = false;
        return node;
    }

    private isKnownLeaf(node: GraphNode): boolean {
        // if (node.hasOwnProperty('isLeaf')) {
        //     return node.isLeaf;
        // }
        return false;
    }
    private setKnownLeaf(node, isLeaf) {
        // node.isLeaf = isLeaf;
    }

    public isLeaf(node: GraphNode): boolean {
        // if (node.hasOwnProperty('isLeaf')) {
        //     return node.isLeaf;
        // }
        for (const edge of this.edges) {
            if (edge.source === node.id || edge.source.id === node.id) {
                return false;
            }
        }
        return true;
    }

    private getNode(d): GraphNode {
        // se já tem id assume que é o node desejado (d3 já transformou em node)
        if (d.id) { return d; }
        return this.nodes.find(node => node.id === d);
    }

    private getLeavingEdges(node: GraphNode): GraphEdge[] {
        if (this.isKnownLeaf(node)) {
            return [];
        }
        const edges: GraphEdge[] = this.edges
            .filter(e => e.source.id === node.id);
        this.setKnownLeaf(node, edges.length === 0);
        return edges;
    }

    public getEdgesPointingThisNode(node: GraphNode): GraphEdge[] {
        return this.edges.filter(
            edge => edge.target.id === node.id);
    }

    public getSubGraph(root: GraphNode): SubGraph {
        const nodes: Set<GraphNode> = new Set();
        const edges: Set<GraphEdge> = new Set();
        this._getSubGraph(root, nodes, edges);
        const subGraph: SubGraph = { edges, nodes };
        return subGraph;
    }

    private _getSubGraph(root: GraphNode, collectedNodes: Set<GraphNode>, collectedEdges: Set<GraphEdge>) {
        const leavingEdges = this.getLeavingEdges(root);
        for (const edge of leavingEdges) {
            collectedEdges.add(edge);
            const nodeTarget = edge.target;
            if (!collectedNodes.has(nodeTarget)) {
                collectedNodes.add(nodeTarget);
                this._getSubGraph(nodeTarget, collectedNodes, collectedEdges);
            }
        }
    }

    private nodeSize = (d) => {
        const node = this.getNode(d);
        return node.size ? node.size : 5;
    }

    onWindowResize = () => {
        this.simulation.force('center', d3.forceCenter());
        this.setNodesFixedStatus(false);
        this.updateWorkingArea();
        this.setForces();
    }

    private svgClear = () => this.svg.selectAll('*').remove();

    private draw() {
        this.svg = d3.select('svg');

        this.svgClear();

        this.svgDefinitions();
        this.updateWorkingArea();

        this.simulation = d3.forceSimulation();

        d3.select(window).on('resize', this.onWindowResize);

        this.createGraph();
        this.initializeSimulation();

        this.svg.transition().call(this.zoomHandle.scaleBy, 1);
    }

    private distinctSizes = () => {
        return [... new Set(this.nodes.map(n => this.nodeSize(n)))];
    }

    private svgDefinitions = () => {

        const defs = this.svg
            .append('defs')
            .selectAll('marker')
            .data(this.distinctSizes())
            .enter()
            .append('marker')
            .attr('id', size => `triangle${size}`)
            .attr('viewBox', '0 0 10 10')
            .attr('refX', size => size + 11 + (size * 0.33))
            .attr('refY', 5)
            .attr('orient', 'auto')
            .attr('markerWidth', 4)
            .attr('markerHeight', 3)
            .attr('xoverflow', 'visible')
            .append('svg:path')
            .attr('d', 'M 0 0 L 10 5 L 0 10 z')
            .attr('fill', '#444')
            .style('stroke', 'none')
            ;
    }

    private initializeSimulation = () => {
        this.simulation.nodes(this.nodes);
        this.initializeForces();
        this.simulation.on('tick', this.onTick);
    }

    private initializeForces = () => {
        this.simulation
            .force('link', d3.forceLink())
            .force('charge', d3.forceManyBody())
            .force('collide', d3.forceCollide())
            .force('forceX', d3.forceX())
            .force('forceY', d3.forceY())
            ;
        this.setForces();
    }

    private setForces = () => {

        this.simulation.force('charge')
            .strength(forceProperties.charge.strength)
            .distanceMin(forceProperties.charge.distanceMin)
            .distanceMax(forceProperties.charge.distanceMax);

        this.simulation.force('collide')
            .strength(forceProperties.collide.strength)
            .radius(forceProperties.collide.radius)
            .iterations(forceProperties.collide.iterations);

        this.simulation.force('forceX')
            .strength(forceProperties.forceX.strength)
            .x(this.width * forceProperties.forceX.x);

        this.simulation.force('forceY')
            .strength(forceProperties.forceY.strength)
            .y(this.height * forceProperties.forceY.y);


        this.simulation.force('link')
            .id(d => d.id)
            .distance(forceProperties.link.distance)
            .iterations(forceProperties.link.iterations)
            .links(this.edges);

        // updates ignored until this is run
        // restarts the this.simulation (important if this.simulation has already slowed down)
        this.simulation.alpha(1).restart();
    }

    private dragstarted = (d) => {
        if (!d3.event.active) {
            this.simulation.alphaTarget(0.3).restart();
        }
        d.fx = d.x;
        d.fy = d.y;
    }

    private dragged = (d) => {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    private dragended = (d) => {
        if (!d3.event.active) {
            this.simulation.alphaTarget(0.0001);
        }
        this.radialLayout(d);
        // d.fx = null;
        // d.fy = null;
    }

    private radialLayout(rootNode: GraphNode) {
        const radius = 30;
        const nodes = this.getLeavingEdges(rootNode).map(e => e.target);
        const step = 360.0 / nodes.length;
        let angle = 0;
        for (const node of nodes) {
            if (!node.layouted) {
                node.x = rootNode.x + Math.cos(angle * Math.PI / 180.0) * radius;
                node.y = rootNode.y + Math.sin(angle * Math.PI / 180.0) * radius;
            }
            this.fixNode(node);
            angle += step;
        }
        rootNode.layouted = true;
    }

    private nodeName(node: any) {
        return node.name || node.id;
    }

    private nodeToolTip = (d) => {
        return d.title ? d.title : this.nodeName(d);
    }

    private nodeText = (d) => {
        let txt = '';
        if (this.showNodeText) {
            txt = this.nodeName(d);
        }
        return d.collapsed ? '+' + txt : txt;
    }

    private createGraph = () => {

        const gZoom = this.svg.append('g');

        this.createGraphEdges(gZoom);

        this.createGraphNodes(gZoom);

        this.zoomHandle = d3.zoom()
            .on('zoom', () => gZoom.attr('transform', d3.event.transform));

        this.zoomHandle(this.svg);

    }

    // update the display positions after each simulation tick
    private onTick = () => {

        const baseX = this.nodeClicked ? this.nodeClicked.x : this.width / 2;
        const baseY = this.nodeClicked ? this.nodeClicked.y : this.height / 2;

        const tx = (x: number) => x ? x : baseX;
        const ty = (y: number) => y ? y : baseY;

        this.node
            .attr('transform', d => `translate(${tx(d.x)},${ty(d.y)})`);

        this.link
            .attr('x1', d => tx(d.source.x))
            .attr('y1', d => ty(d.source.y))
            .attr('x2', d => tx(d.target.x))
            .attr('y2', d => ty(d.target.y))
            ;

        this.linkText
            .attr('x', d => (d.source.x + d.target.x) / 2)
            .attr('y', d => (d.source.y + d.target.y) / 2)
            ;

    }

    private updateWorkingArea = () => {
        const svgNode: any = this.svg.node();
        this.width = +svgNode.getBoundingClientRect().width;
        this.height = +svgNode.getBoundingClientRect().height;
    }

    private createGraphNodes(gZoom: any) {
        this.node = gZoom
            .selectAll('.gn')
            .data(this.nodes)
            .enter()
            .append('g')
            .attr('class', 'gn')
            .call(d3.drag()
                .on('start', this.dragstarted)
                .on('drag', this.dragged)
                .on('end', this.dragended));

        this.node
            .append('circle')
            .attr('fill', d => d.icon ? 'none' : this.nodeColor(d))
            .attr('stroke', d => d.collapsed ? '#b80f58' : d.icon ? 'none' : 'gray')
            .attr('r', d => this.nodeSize(d))
            .append('title').text(d => this.nodeToolTip(d));

        this.node.append('text')
            .attr('x', d => -(this.nodeSize(d) * 2 + 1))
            .attr('y', d => this.nodeSize(d))
            .attr('fill', d => this.nodeColor(d))
            .attr('class', 'fa')
            .text(d => FONT_AWESOME[d.icon])
            .append('title').text(d => this.nodeToolTip(d));

        this.node.append('image')
            .attr('xlink:href', d => d.imageUrl)
            .attr('class', 'image')
            .attr('x', -10)
            .attr('y', -10)
            .attr('width', 20)
            .attr('height', 20)
            .append('title').text(d => this.nodeToolTip(d));

        if (this.showNodeText) {
            this.node.append('text')
                .attr('class', 'node-name')
                .text(d => this.nodeText(d))
                .attr('y', d => this.nodeSize(d) + 1 + 8)
                .attr('x', d => -this.nodeSize(d) * this.nodeName(d).length / 2)
                .attr('fill', d => d.color ? d.color : '#444')
                .append('title').text(d => d.title ? d.title : '');
        }

        this.svg.selectAll('.gn *')
            .on('dblclick', n => this.onNodeClicked(n));

        this.svg.selectAll('.gn *')
            .on('click', n => {
                if (d3.event.ctrlKey) {
                    this.radialLayout(n);
                }
            });


    }

    private createGraphEdges(gZoom: any) {
        this.link = gZoom
            .selectAll('.ge')
            .data(this.edges)
            .enter()
            .append('g')
            .attr('class', 'ge')
            .append('line')
            .attr('class', 'edge')
            .attr('stroke', d => d.color ? d.color : '#555')
            .attr('marker-end', d => `url(#triangle${this.nodeSize(d.target)})`);

        this.linkText = gZoom.selectAll('.ge')
            .append('text')
            .attr('class', 'edge-name')
            .attr('fill', d => d.color ? d.color : '#555')
            .text(d => this.showEdgeText ? d.name : '');
    }

    private nodeColor(d: GraphNode) {
        if (d.marked) {
            return 'red';
        }
        return d.color ? d.color : 'lightgray';
    }
}

